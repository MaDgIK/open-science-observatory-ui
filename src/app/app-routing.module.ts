import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { CountriesMapOverviewComponent } from './pages/home/countries-map-overview.component';
import { CountryComponent } from './pages/country/country.component';
import { MethodologyPageComponent } from './pages/methodology/methodology.component';
import { ContinentComponent } from './pages/continent/continent.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'overview-map-embed',
    component: CountriesMapOverviewComponent
  },
  {
    path: 'continent',
    loadChildren: () => import('./pages/continent/continent.module').then(m => m.ContinentModule),
  },
  {
    path: 'country',
    loadChildren: () => import('./pages/country/country.module').then(m => m.CountryModule),
  },
  // {
  //   path: 'overview/:continentName',
  //   component: ContinentComponent
  // },
  // {
  //   path: 'country/:countryCode',
  //   component: CountryComponent
  // },
  {
    path: 'methodology',
    component: MethodologyPageComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    // preloadingStrategy: PreloadAllModules,
    scrollPositionRestoration: 'disabled',
    // onSameUrlNavigation: 'reload',
    // relativeLinkResolution: 'corrected'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }

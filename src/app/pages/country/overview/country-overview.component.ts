import { environment } from '../../../../environments/environment';
import { Component, OnInit } from '@angular/core';
import {
  datasetPalette,
  otherResearchProductsPalette,
  publicationPalette,
  softwarePalette,
  resultsPalette,
  datasetColor, publicationColor, otherResearchProductsColor, softwareColor, gradientStartColor
} from '../../../chart-palettes';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { TreemapHighchartsData } from '../../../domain/treemap-highcharts-data';
import { CountryPageOverviewData } from '../../../domain/overview-map-data';
import { DataHandlerService } from '../../../services/data-handler.service';
import { DataService } from '../../../services/data.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-country-overview',
  templateUrl: './country-overview.component.html',
})

export class CountryOverviewComponent implements OnInit {

  private chartsURL = environment.API_ENDPOINT + 'chart?json=';
  private profileName = environment.profileName;

  private publicationPalette = publicationPalette;
  private datasetPalette = datasetPalette;
  private softwarePalette = softwarePalette;
  private otherResearchProductsPalette = otherResearchProductsPalette;
  private resultsPalette = resultsPalette;

  publicationColor = publicationColor;
  datasetColor = datasetColor;
  softwareColor = softwareColor;
  otherResearchProductsColor = otherResearchProductsColor;
  gradientStartColor = gradientStartColor;

  countryCode: string;

  countryPageOAData: CountryPageOverviewData;

  resultsByTypeOANonOAChartURL: SafeResourceUrl;

  oaNonOAPublicationsTimelineChartURL: SafeResourceUrl;
  oaNonOADatasetsTimelineChartURL: SafeResourceUrl;
  oaNonOASoftwareTimelineChartURL: SafeResourceUrl;
  oaNonOAOtherTimelineChartURL: SafeResourceUrl;

  publicationsByFunderData: TreemapHighchartsData[];
  datasetsByFunderData: TreemapHighchartsData[];
  softwareByFunderData: TreemapHighchartsData[];
  otherByFunderData: TreemapHighchartsData[];

  publicationGroupByView = 'datasource';
  publicationsByDatasourceChartURL: SafeResourceUrl;
  publicationsByOrganizationChartURL: SafeResourceUrl;
  publicationsByCountryChartURLMobile: SafeResourceUrl;
  publicationsByDatasourceChartURLMobile: SafeResourceUrl;
  publicationsByOrganizationChartURLMobile: SafeResourceUrl;

  datasetsGroupByView = 'datasource';
  datasetsByDatasourceChartURL: SafeResourceUrl;
  datasetsByOrganizationChartURL: SafeResourceUrl;
  datasetsByCountryChartURLMobile: SafeResourceUrl;
  datasetsByDatasourceChartURLMobile: SafeResourceUrl;
  datasetsByOrganizationChartURLMobile: SafeResourceUrl;

  softwareGroupByView = 'datasource';
  softwareByDatasourceChartURL: SafeResourceUrl;
  softwareByOrganizationChartURL: SafeResourceUrl;
  softwareByCountryChartURLMobile: SafeResourceUrl;
  softwareByDatasourceChartURLMobile: SafeResourceUrl;
  softwareByOrganizationChartURLMobile: SafeResourceUrl;

  orpGroupByView = 'datasource';
  otherByDatasourceChartURL: SafeResourceUrl;
  otherByOrganizationChartURL: SafeResourceUrl;
  otherByCountryChartURLMobile: SafeResourceUrl;
  otherByDatasourceChartURLMobile: SafeResourceUrl;
  otherByOrganizationChartURLMobile: SafeResourceUrl;

  constructor(private dataService: DataService,
              private dataHandlerService: DataHandlerService,
              private route: ActivatedRoute,
              private sanitizer: DomSanitizer) { }

  ngOnInit(): void {

    this.route.parent.params.subscribe(params => {

      this.countryCode = params['countryCode'];
      console.log('this.countryCode', this.countryCode);

      this.dataService.getCountryOAData(this.countryCode).subscribe(
        rawData => {
          this.countryPageOAData = this.dataHandlerService.convertRawDataToCountryPageOAData(rawData);
        }, error => {
          console.log(error);
        }
      );

      this.oaNonOAPublicationsTimelineChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"OA","type":"column","query":{"name":"new.oso.publications_timeline.peer_reviewed.oa.affiliated.country","parameters":[1,"${this.countryCode}"],"profile":"${this.profileName}"}},{"name":"Non-OA","type":"column","query":{"name":"new.oso.publications_timeline.peer_reviewed.oa.affiliated.country","parameters":[0,"${this.countryCode}"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Publications","align":"left","margin":50},"subtitle":{"text":"over time","align":"left","margin":50},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.publicationPalette.join('","')}\"]}}`));
      this.oaNonOADatasetsTimelineChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"OA","type":"column","query":{"name":"new.oso.results_timeline.oa.affiliated.country","parameters":[1,"dataset","${this.countryCode}"],"profile":"${this.profileName}"}},{"name":"Non-OA","type":"column","query":{"name":"new.oso.results_timeline.oa.affiliated.country","parameters":[0,"dataset","${this.countryCode}"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Datasets","align":"left","margin":50},"subtitle":{"text":"over time","align":"left","margin":50},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetPalette.join('","')}\"]}}`));
      this.oaNonOASoftwareTimelineChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"OA","type":"column","query":{"name":"new.oso.results_timeline.oa.affiliated.country","parameters":[1,"software","${this.countryCode}"],"profile":"${this.profileName}"}},{"name":"Non-OA","type":"column","query":{"name":"new.oso.results_timeline.oa.affiliated.country","parameters":[0,"software","${this.countryCode}"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Software","align":"left","margin":50},"subtitle":{"text":"over time","align":"left","margin":50},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.softwarePalette.join('","')}\"]}}`));
      this.oaNonOAOtherTimelineChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"OA","type":"column","query":{"name":"new.oso.results_timeline.oa.affiliated.country","parameters":[1,"other","${this.countryCode}"],"profile":"${this.profileName}"}},{"name":"Non-OA","type":"column","query":{"name":"new.oso.results_timeline.oa.affiliated.country","parameters":[0,"other","${this.countryCode}"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Other research products","align":"left","margin":50},"subtitle":{"text":"over time","align":"left","margin":50},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherResearchProductsPalette.join('","')}\"]}}`));

      this.publicationsByDatasourceChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"publications","type":"column","query":{"name":"new.oso.publications.peer_reviewed.affiliated.bydatasource.country","parameters":["${this.countryCode}"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access publications","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""}},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":false},"credits":{"href":null,"enabled":false},"colors":[\"${this.publicationPalette.join('","')}\"]}}`));
      this.publicationsByDatasourceChartURLMobile = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"publications","type":"bar","query":{"name":"new.oso.publications.peer_reviewed.affiliated.bydatasource.country","parameters":["${this.countryCode}"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access publications","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""}},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":false},"credits":{"href":null,"enabled":false},"colors":[\"${this.publicationPalette.join('","')}\"]}}`));

      this.datasetsByDatasourceChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"datasets","type":"column","query":{"name":"oso.results.affiliated.bydatasource.country","parameters":["${this.countryCode}","dataset"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""}},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":false},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetPalette.join('","')}\"]}}`));
      this.datasetsByDatasourceChartURLMobile = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"datasets","type":"bar","query":{"name":"oso.results.affiliated.bydatasource.country","parameters":["${this.countryCode}","dataset"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""}},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":false},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetPalette.join('","')}\"]}}`));

      this.softwareByDatasourceChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"software","type":"column","query":{"name":"oso.results.affiliated.bydatasource.country","parameters":["${this.countryCode}","software"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Source software","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""}},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":false},"credits":{"href":null,"enabled":false},"colors":[\"${this.softwarePalette.join('","')}\"]}}`));
      this.softwareByDatasourceChartURLMobile = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"software","type":"bar","query":{"name":"oso.results.affiliated.bydatasource.country","parameters":["${this.countryCode}","software"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Source software","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""}},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":false},"credits":{"href":null,"enabled":false},"colors":[\"${this.softwarePalette.join('","')}\"]}}`));

      this.otherByDatasourceChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"other research products","type":"column","query":{"name":"oso.results.affiliated.bydatasource.country","parameters":["${this.countryCode}","other"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access other research products","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""}},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":false},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherResearchProductsPalette.join('","')}\"]}}`));
      this.otherByDatasourceChartURLMobile = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"other research products","type":"bar","query":{"name":"oso.results.affiliated.bydatasource.country","parameters":["${this.countryCode}","other"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access other research products","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""}},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":false},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherResearchProductsPalette.join('","')}\"]}}`));


      // this.resultsByTypeOANonOAChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"OA","type":"column","query":{"select":[{"field":"result","aggregate":"count"},{"field":"result.type","aggregate":null}],"filters":[{"groupFilters":[{"field":"result.access mode","type":"=","values":["Open Access"]},{"field":"result.access mode","type":"=","values":["Open Source"]}],"op":"OR"},{"groupFilters":[{"field":"result.organization.country.code","type":"=","values":["${this.countryCode}"]}],"op":"AND"}],"entity":"result","profile":"${this.profileName}","limit":"30"}},{"name":"Non-OA","type":"column","query":{"select":[{"field":"result","aggregate":"count"},{"field":"result.type","aggregate":null}],"filters":[{"groupFilters":[{"field":"result.access mode","type":"!=","values":["Open Access"]},{"field":"result.access mode","type":"!=","values":["Open Source"]}],"op":"AND"},{"groupFilters":[{"field":"result.organization.country.code","type":"=","values":["${this.countryCode}"]}],"op":"AND"}],"entity":"result","profile":"${this.profileName}","limit":"30"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Research results by type","align":"left","margin":50},"subtitle":{},"yAxis":{"title":{"text":""}},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":true,"align":"center","verticalAlign":"bottom","layout":"horizontal"},"credits":{"href":null,"enabled":false},"colors":[\"${this.resultsPalette.join('","')}\"]}}`));

      this.dataService.getFundersResultsByPublicationsForCountry(this.countryCode).subscribe(
        rawData => {
          this.publicationsByFunderData = this.dataHandlerService.convertRawDataToTreemapHighchartsData(rawData);
        }, error => {
          console.log(error);
        }
      );

      this.dataService.getFundersResultsByTypeForCountry('dataset', this.countryCode).subscribe(
        rawData => {
          this.datasetsByFunderData = this.dataHandlerService.convertRawDataToTreemapHighchartsData(rawData);
        }, error => {
          console.log(error);
        }
      );

      this.dataService.getFundersResultsByTypeForCountry('software', this.countryCode).subscribe(
        rawData => {
          this.softwareByFunderData = this.dataHandlerService.convertRawDataToTreemapHighchartsData(rawData);
        }, error => {
          console.log(error);
        }
      );

      this.dataService.getFundersResultsByTypeForCountry('other', this.countryCode).subscribe(
        rawData => {
          this.otherByFunderData = this.dataHandlerService.convertRawDataToTreemapHighchartsData(rawData);
        }, error => {
          console.log(error);
        }
      );
      }
    );
  }

  getPublicationsGroupBy(contentSelection: string): void {
    this.publicationGroupByView = contentSelection;
    if (contentSelection === 'datasource' && !this.publicationsByDatasourceChartURL) {
      this.publicationsByDatasourceChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"publications","type":"column","query":{"name":"new.oso.publications.peer_reviewed.affiliated.bydatasource.country","parameters":["${this.countryCode}"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access publications","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""}},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":false},"credits":{"href":null,"enabled":false},"colors":[\"${this.publicationPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'organization' && !this.publicationsByOrganizationChartURL) {
      this.publicationsByOrganizationChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"publications","type":"column","query":{"name":"new.oso.publications.peer_reviewed.affiliated.byorganization.country","parameters":["${this.countryCode}"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access publications","align":"left","margin":50},"subtitle":{"text":"by organization (top organizations)","align":"left"},"yAxis":{"title":{"text":""}},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":false},"credits":{"href":null,"enabled":false},"colors":[\"${this.publicationPalette.join('","')}\"]}}`));
    }
  }

  getPublicationsGroupByMobile(contentSelection: string): void {
    this.publicationGroupByView = contentSelection;
    if (contentSelection === 'datasource' && !this.publicationsByDatasourceChartURLMobile) {
      this.publicationsByDatasourceChartURLMobile = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"publications","type":"bar","query":{"name":"new.oso.publications.peer_reviewed.affiliated.bydatasource.country","parameters":["${this.countryCode}"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access publications","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""}},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":false},"credits":{"href":null,"enabled":false},"colors":[\"${this.publicationPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'organization' && !this.publicationsByOrganizationChartURLMobile) {
      this.publicationsByOrganizationChartURLMobile = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"publications","type":"bar","query":{"name":"new.oso.publications.peer_reviewed.affiliated.byorganization.country","parameters":["${this.countryCode}"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access publications","align":"left","margin":50},"subtitle":{"text":"by organization (top organizations)","align":"left"},"yAxis":{"title":{"text":""}},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":false},"credits":{"href":null,"enabled":false},"colors":[\"${this.publicationPalette.join('","')}\"]}}`));
    }
  }

  getDatasetsGroupBy(contentSelection: string): void {
    this.datasetsGroupByView = contentSelection;
    if (contentSelection === 'datasource' && !this.datasetsByDatasourceChartURL) {
      this.datasetsByDatasourceChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"datasets","type":"column","query":{"name":"oso.results.affiliated.bydatasource.country","parameters":["${this.countryCode}","dataset"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""}},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":false},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'organization' && !this.datasetsByOrganizationChartURL) {
      this.datasetsByOrganizationChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"datasets","type":"column","query":{"name":"oso.results.affiliated.byorganization.country","parameters":["${this.countryCode}","dataset"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets","align":"left","margin":50},"subtitle":{"text":"by organization (top organizations)","align":"left"},"yAxis":{"title":{"text":""}},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":false},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetPalette.join('","')}\"]}}`));
    }
  }

  getDatasetsGroupByMobile(contentSelection: string): void {
    this.datasetsGroupByView = contentSelection;
    if (contentSelection === 'datasource' && !this.datasetsByDatasourceChartURLMobile) {
      this.datasetsByDatasourceChartURLMobile = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"datasets","type":"bar","query":{"name":"oso.results.affiliated.bydatasource.country","parameters":["${this.countryCode}","dataset"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""}},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":false},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'organization' && !this.datasetsByOrganizationChartURLMobile) {
      this.datasetsByOrganizationChartURLMobile = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"datasets","type":"bar","query":{"name":"oso.results.affiliated.byorganization.country","parameters":["${this.countryCode}","dataset"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets","align":"left","margin":50},"subtitle":{"text":"by organization (top organizations)","align":"left"},"yAxis":{"title":{"text":""}},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":false},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetPalette.join('","')}\"]}}`));
    }
  }

  getSoftwareGroupBy(contentSelection: string): void {
    this.softwareGroupByView = contentSelection;
    if (contentSelection === 'datasource' && !this.softwareByDatasourceChartURL) {
      this.softwareByDatasourceChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"software","type":"column","query":{"name":"oso.results.affiliated.bydatasource.country","parameters":["${this.countryCode}","software"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Source software","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""}},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":false},"credits":{"href":null,"enabled":false},"colors":[\"${this.softwarePalette.join('","')}\"]}}`));
    } else if (contentSelection === 'organization' && !this.softwareByOrganizationChartURL) {
      this.softwareByOrganizationChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"software","type":"column","query":{"name":"oso.results.affiliated.byorganization.country","parameters":["${this.countryCode}","software"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Source software","align":"left","margin":50},"subtitle":{"text":"by organization (top organizations)","align":"left"},"yAxis":{"title":{"text":""}},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":false},"credits":{"href":null,"enabled":false},"colors":[\"${this.softwarePalette.join('","')}\"]}}`));
    }
  }

  getSoftwareGroupByMobile(contentSelection: string): void {
    this.softwareGroupByView = contentSelection;
    if (contentSelection === 'datasource' && !this.softwareByDatasourceChartURLMobile) {
      this.softwareByDatasourceChartURLMobile = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"software","type":"bar","query":{"name":"oso.results.affiliated.bydatasource.country","parameters":["${this.countryCode}","software"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Source software","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""}},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":false},"credits":{"href":null,"enabled":false},"colors":[\"${this.softwarePalette.join('","')}\"]}}`));
    } else if (contentSelection === 'organization' && !this.softwareByOrganizationChartURLMobile) {
      this.softwareByOrganizationChartURLMobile = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"software","type":"bar","query":{"name":"oso.results.affiliated.byorganization.country","parameters":["${this.countryCode}","software"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Source software","align":"left","margin":50},"subtitle":{"text":"by organization (top organizations)","align":"left"},"yAxis":{"title":{"text":""}},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":false},"credits":{"href":null,"enabled":false},"colors":[\"${this.softwarePalette.join('","')}\"]}}`));
    }
  }

  getORPGroupBy(contentSelection: string): void {
    this.orpGroupByView = contentSelection;
    if (contentSelection === 'datasource' && !this.otherByDatasourceChartURL) {
      this.otherByDatasourceChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"other research products","type":"column","query":{"name":"oso.results.affiliated.bydatasource.country","parameters":["${this.countryCode}","other"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access other research products","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""}},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":false},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherResearchProductsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'organization' && !this.otherByOrganizationChartURL) {
      this.otherByOrganizationChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"other research products","type":"column","query":{"name":"oso.results.affiliated.byorganization.country","parameters":["${this.countryCode}","other"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access other research products","align":"left","margin":50},"subtitle":{"text":"by organization (top organizations)","align":"left"},"yAxis":{"title":{"text":""}},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":false},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherResearchProductsPalette.join('","')}\"]}}`));
    }
  }

  getORPGroupByMobile(contentSelection: string): void {
    this.orpGroupByView = contentSelection;
    if (contentSelection === 'datasource' && !this.otherByDatasourceChartURLMobile) {
      this.otherByDatasourceChartURLMobile = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"other research products","type":"bar","query":{"name":"oso.results.affiliated.bydatasource.country","parameters":["${this.countryCode}","other"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access other research products","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""}},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":false},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherResearchProductsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'organization' && !this.otherByOrganizationChartURLMobile) {
      this.otherByOrganizationChartURLMobile = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"other research products","type":"bar","query":{"name":"oso.results.affiliated.byorganization.country","parameters":["${this.countryCode}","other"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access other research products","align":"left","margin":50},"subtitle":{"text":"by organization (top organizations)","align":"left"},"yAxis":{"title":{"text":""}},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":false},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherResearchProductsPalette.join('","')}\"]}}`));
    }
  }
}

import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { CountryComponent } from './country.component';
import { CountryOverviewComponent } from './overview/country-overview.component';

const countryRoutes: Routes = [
  {
    path: ':countryCode',
    component: CountryComponent,
    children: [
      {
        path: '',
        redirectTo: 'overview',
        // pathMatch: 'full'
      },
      {
        path: 'overview',
        component: CountryOverviewComponent
      },
      {
        path: 'open-science',
        loadChildren: () => import('./open-science/country-open-science.module').then(m => m.CountryOpenScienceModule),
      },
      {
        path: 'collaboration',
        loadChildren: () => import('./collaboration/country-collaboration.module').then(m => m.CountryCollaborationModule),
      }
    ]
  }
];

@NgModule ({
  imports: [RouterModule.forChild(countryRoutes)],
  exports: [RouterModule]
})

export class CountryRoutingModule {}

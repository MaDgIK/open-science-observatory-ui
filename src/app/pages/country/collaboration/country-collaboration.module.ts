import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReusableComponentsModule } from '../../../shared/reusablecomponents/reusable-components.module';
import { DataService } from '../../../services/data.service';
import { DataHandlerService } from '../../../services/data-handler.service';
import { CountryCollaborationComponent } from './country-collaboration.component';
import { CountryCollaborationRoutingModule } from './country-collaboration-routing.module';

@NgModule ({
  imports: [
    CommonModule,
    CountryCollaborationRoutingModule,
    ReusableComponentsModule,
  ],
  declarations: [
    CountryCollaborationComponent,
  ],
  providers: [
    DataService,
    DataHandlerService
  ],
})

export class CountryCollaborationModule {}

import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { CountryCollaborationSoftwareComponent } from './country-collaboration-software.component';
import { CountryCollaborationSoftwareIndicatorsComponent } from './country-collaboration-software-indicators.component';


const countryCollaborationSoftwareRoutes: Routes = [
  {
    path: '',
    component: CountryCollaborationSoftwareComponent,
    children : [
      {
        path: '',
        redirectTo: 'co-funded',
        pathMatch: 'full',
      },
      {
        path: ':indicator',
        component: CountryCollaborationSoftwareIndicatorsComponent,
      }
    ]
  },
];

@NgModule ({
  imports: [RouterModule.forChild(countryCollaborationSoftwareRoutes)],
  exports: [RouterModule]
})

export class CountryCollaborationSoftwareRoutingModule {}

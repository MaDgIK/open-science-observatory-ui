import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReusableComponentsModule } from '../../../../shared/reusablecomponents/reusable-components.module';
import { DataService } from '../../../../services/data.service';
import { DataHandlerService } from '../../../../services/data-handler.service';
import { CountryCollaborationSoftwareRoutingModule } from './country-collaboration-software-routing.module';
import { CountryCollaborationSoftwareComponent } from './country-collaboration-software.component';
import { CountryCollaborationSoftwareIndicatorsComponent } from './country-collaboration-software-indicators.component';


@NgModule ({
  imports: [
    CommonModule,
    CountryCollaborationSoftwareRoutingModule,
    ReusableComponentsModule,
  ],
  declarations: [
    CountryCollaborationSoftwareComponent,
    CountryCollaborationSoftwareIndicatorsComponent,
  ],
  providers: [
    DataService,
    DataHandlerService
  ],
})

export class CountryCollaborationSoftwareModule {}

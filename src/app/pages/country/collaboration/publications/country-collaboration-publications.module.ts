import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReusableComponentsModule } from '../../../../shared/reusablecomponents/reusable-components.module';
import { DataService } from '../../../../services/data.service';
import { DataHandlerService } from '../../../../services/data-handler.service';
import { CountryCollaborationPublicationsRoutingModule } from './country-collaboration-publications-routing.module';
import { CountryCollaborationPublicationsComponent } from './country-collaboration-publications.component';
import { CountryCollaborationPublicationsIndicatorsComponent } from './country-collaboration-publications-indicators.component';


@NgModule ({
  imports: [
    CommonModule,
    CountryCollaborationPublicationsRoutingModule,
    ReusableComponentsModule,
  ],
  declarations: [
    CountryCollaborationPublicationsComponent,
    CountryCollaborationPublicationsIndicatorsComponent,
  ],
  providers: [
    DataService,
    DataHandlerService
  ],
})

export class CountryCollaborationPublicationsModule {}

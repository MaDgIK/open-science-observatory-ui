import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { CountryCollaborationPublicationsComponent } from './country-collaboration-publications.component';
import { CountryCollaborationPublicationsIndicatorsComponent } from './country-collaboration-publications-indicators.component';


const countryCollaborationPublicationsRoutes: Routes = [
  {
    path: '',
    component: CountryCollaborationPublicationsComponent,
    children : [
      {
        path: '',
        redirectTo: 'co-funded',
        pathMatch: 'full',
      },
      {
        path: ':indicator',
        component: CountryCollaborationPublicationsIndicatorsComponent,
      }
    ]
  },
];

@NgModule ({
  imports: [RouterModule.forChild(countryCollaborationPublicationsRoutes)],
  exports: [RouterModule]
})

export class CountryCollaborationPublicationsRoutingModule {}

import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { CountryCollaborationComponent } from './country-collaboration.component';


const countryCollaborationRoutes: Routes = [
  {
    path: '',
    component: CountryCollaborationComponent,
    children : [
      {
        path: '',
        redirectTo: 'publications',
        pathMatch: 'full',
      },
      {
        path: 'publications',
        loadChildren: () => import('./publications/country-collaboration-publications.module').then(m => m.CountryCollaborationPublicationsModule),
      },
      {
        path: 'datasets',
        loadChildren: () => import('./datasets/country-collaboration-datasets.module').then(m => m.CountryCollaborationDatasetsModule),
      },
      {
        path: 'software',
        loadChildren: () => import('./software/country-collaboration-software.module').then(m => m.CountryCollaborationSoftwareModule),
      },
      {
        path: 'other-research-products',
        loadChildren: () => import('./other-research-products/country-collaboration-orp.module').then(m => m.CountryCollaborationORPModule),
      },
    ]
  },
];

@NgModule ({
  imports: [RouterModule.forChild(countryCollaborationRoutes)],
  exports: [RouterModule]
})

export class CountryCollaborationRoutingModule {}

import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { CountryCollaborationDatasetsIndicatorsComponent } from './country-collaboration-datasets-indicators.component';
import { CountryCollaborationDatasetsComponent } from './country-collaboration-datasets.component';

const countryCollaborationDatasetsRoutes: Routes = [
  {
    path: '',
    component: CountryCollaborationDatasetsComponent,
    children : [
      {
        path: '',
        redirectTo: 'co-funded',
        pathMatch: 'full',
      },
      {
        path: ':indicator',
        component: CountryCollaborationDatasetsIndicatorsComponent,
      }
    ]
  },
];

@NgModule ({
  imports: [RouterModule.forChild(countryCollaborationDatasetsRoutes)],
  exports: [RouterModule]
})

export class CountryCollaborationDatasetsRoutingModule {}

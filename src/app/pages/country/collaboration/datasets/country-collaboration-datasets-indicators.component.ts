import { Component, OnInit } from '@angular/core';
import {
  gradientStartColor,
  datasetColor,
  datasetPalette, datasetIndicatorsPalette, goldGreenIndicatorsPalette
} from '../../../../chart-palettes';
import {environment} from '../../../../../environments/environment';
import {CountryTableData} from '../../../../domain/overview-map-data';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';
import {TreemapHighchartsData} from '../../../../domain/treemap-highcharts-data';
import {DataHandlerService} from '../../../../services/data-handler.service';
import {DataService} from '../../../../services/data.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-country-collaboration-datasets-indicators',
  templateUrl: './country-collaboration-datasets-indicators.component.html',
})

export class CountryCollaborationDatasetsIndicatorsComponent implements OnInit {

  private chartsURL = environment.API_ENDPOINT + 'chart?json=';
  private profileName = environment.profileName;

  indicator: string;

  constructor(private dataService: DataService,
              private dataHandlerService: DataHandlerService,
              private route: ActivatedRoute,
              private sanitizer: DomSanitizer) { }

  ngOnInit(): void {

    this.route.params.subscribe(params => {

      this.indicator = params['indicator'];
    });
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReusableComponentsModule } from '../../../../shared/reusablecomponents/reusable-components.module';
import { DataService } from '../../../../services/data.service';
import { DataHandlerService } from '../../../../services/data-handler.service';
import { CountryCollaborationDatasetsRoutingModule } from './country-collaboration-datasets-routing.module';
import { CountryCollaborationDatasetsComponent } from './country-collaboration-datasets.component';
import { CountryCollaborationDatasetsIndicatorsComponent } from './country-collaboration-datasets-indicators.component';


@NgModule ({
  imports: [
    CommonModule,
    CountryCollaborationDatasetsRoutingModule,
    ReusableComponentsModule,
  ],
  declarations: [
    CountryCollaborationDatasetsComponent,
    CountryCollaborationDatasetsIndicatorsComponent,
  ],
  providers: [
    DataService,
    DataHandlerService
  ],
})

export class CountryCollaborationDatasetsModule {}

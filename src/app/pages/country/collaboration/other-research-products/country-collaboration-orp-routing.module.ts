import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { CountryCollaborationORPComponent } from './country-collaboration-orp.component';
import { CountryCollaborationORPIndicatorsComponent } from './country-collaboration-orp-indicators.component';


const countryCollaborationORPRoutes: Routes = [
  {
    path: '',
    component: CountryCollaborationORPComponent,
    children : [
      {
        path: '',
        redirectTo: 'co-funded',
        pathMatch: 'full',
      },
      {
        path: ':indicator',
        component: CountryCollaborationORPIndicatorsComponent,
      }
    ]
  },
];

@NgModule ({
  imports: [RouterModule.forChild(countryCollaborationORPRoutes)],
  exports: [RouterModule]
})

export class CountryCollaborationORPRoutingModule {}

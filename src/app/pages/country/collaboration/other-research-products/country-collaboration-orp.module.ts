import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReusableComponentsModule } from '../../../../shared/reusablecomponents/reusable-components.module';
import { DataService } from '../../../../services/data.service';
import { DataHandlerService } from '../../../../services/data-handler.service';
import { CountryCollaborationORPRoutingModule } from './country-collaboration-orp-routing.module';
import { CountryCollaborationORPComponent } from './country-collaboration-orp.component';
import { CountryCollaborationORPIndicatorsComponent } from './country-collaboration-orp-indicators.component';


@NgModule ({
  imports: [
    CommonModule,
    CountryCollaborationORPRoutingModule,
    ReusableComponentsModule,
  ],
  declarations: [
    CountryCollaborationORPComponent,
    CountryCollaborationORPIndicatorsComponent,
  ],
  providers: [
    DataService,
    DataHandlerService
  ],
})

export class CountryCollaborationORPModule {}

import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { ActivatedRoute } from '@angular/router';
import { DataHandlerService } from '../../services/data-handler.service';
import { CountryPageOverviewData } from '../../domain/overview-map-data';

@Component({
  selector: 'app-country-page',
  templateUrl: './country.component.html',
})

export class CountryComponent implements OnInit {

  lastUpdateDate: string;

  countryCode: string;

  linkToCountryInOpenAIRE: string;

  countryPageOverviewData: CountryPageOverviewData;

  constructor(private dataService: DataService,
              private dataHandlerService: DataHandlerService,
              private route: ActivatedRoute) { }

  ngOnInit(): void {

    window.scroll(0, 0);

    this.dataService.getLastUpdateDate().subscribe(
      rawData => {
        this.lastUpdateDate = this.dataHandlerService.convertRawDataToLastUpdateDate(rawData);
      }, error => {
        console.log(error);
      }
    );

    this.route.params.subscribe(params => {

      this.countryCode = params['countryCode'];

      this.dataService.getCountryPageOverviewData(this.countryCode).subscribe(
        rawData => {
          this.countryPageOverviewData = this.dataHandlerService.convertRawDataToCountryPageOverviewData(rawData);
          if (this.countryPageOverviewData && this.countryPageOverviewData.name) {
            this.linkToCountryInOpenAIRE = 'https://www.openaire.eu/item/' + this.countryPageOverviewData.name.replace(' ', '-');
            // this.createChartURLs();
          }
        }, error => {
          console.log(error);
        }
      );
    });

  }
}

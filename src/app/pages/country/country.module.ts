import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReusableComponentsModule } from '../../shared/reusablecomponents/reusable-components.module';
import { DataHandlerService } from '../../services/data-handler.service';
import { DataService } from '../../services/data.service';
import { CountryRoutingModule } from './country-routing.module';
import { CountryComponent } from './country.component';
import { CountryOverviewComponent } from './overview/country-overview.component';


@NgModule ({
  imports: [
    CommonModule,
    CountryRoutingModule,
    ReusableComponentsModule,
  ],
  declarations: [
    CountryComponent,
    CountryOverviewComponent,
  ],
  providers: [
    DataService,
    DataHandlerService
  ],
})

export class CountryModule {}

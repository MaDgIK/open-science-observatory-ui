import { MapCountryData } from '../../domain/map-country-data';

declare var require: any;

import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';

import * as Highcharts from 'highcharts';
import MapModule from 'highcharts/modules/map';
import {CountryOverview} from '../../domain/overview-data';

const mapWorld = require('@highcharts/map-collection/custom/world.geo.json');
const mapEurope = require('@highcharts/map-collection/custom/europe.geo.json');

MapModule(Highcharts);

@Component({
  selector: 'app-country-map',
  templateUrl: './country-map.component.html',
  encapsulation: ViewEncapsulation.None
})

export class CountryMapComponent implements OnInit {

  @Input() country: string;

  Highcharts: typeof Highcharts = Highcharts;

  chartMapOptions: Highcharts.Options;

  mapCountryData: MapCountryData[] = [];
  seriesColor: string = '#ff9800';
  seriesName: string = 'OA publications';

  europe: string[] = [];
  countryNames: string[] = [];

  constructor() {}

  ngOnInit(): void {
    this.countryNames.push(this.country);
    console.log('this.country', this.country);
    console.log('this.countryNames', this.countryNames);
    this.mapCountryData.push(this.overviewToMapData());
    this.loadMap(this.mapCountryData, this.seriesColor, this.seriesName, this.country);
  }

  overviewToMapData(): MapCountryData {
    this.seriesColor = '#A26C0A';
    this.seriesName = 'OA policies';
    return {
      id: this.country,
      country: this.country,
      z: 5
    };
  }

  loadMap(mapCountryData: MapCountryData[], seriesColor: string, seriesName: string, countryName: string) {
    this.chartMapOptions = {
      chart: {
        map: mapWorld,
        events: {
          load: function() {
            this.series[0].data = this.series[0].data.map((el) => {
              if ((el.name === countryName)) {
                el.color = 'rgba(139,151,167,0.6)';
                return el;
              }
              return el;
            });
            this.update({
              series: [{
                data: this.series[0].data as Highcharts.SeriesMapbubbleDataOptions,
              } as Highcharts.SeriesMapbubbleOptions]
            });
            let x, y;
            for (let i = 0; i < mapCountryData.length; i++) {
              if (mapCountryData[i].hasOwnProperty('name')) {
                if (mapCountryData[i]['name'] === countryName) {
                  console.log(mapCountryData[i]);
                  if (mapCountryData[i].hasOwnProperty('_midX')) {
                    x = mapCountryData[i]['_midX'];
                  }
                  if (mapCountryData[i].hasOwnProperty('_midY')) {
                    y = mapCountryData[i]['_midY'];
                  }
                  break;
                }
              }
            }
            this.mapZoom(0.1, x, y);
          }
        }
      },
      title: {
        text: ''
      },
      legend: {
        enabled: false
      },
      colorAxis: {
        min: 0
      },
      series: [{
        // name: 'Countries',
        // borderColor: '#fff',
        // negativeColor: 'rgba(139,151,167,0.2)',
        // enableMouseTracking: false,
        // type: 'map'
        // }, {
        name: 'Random data',
        type: 'map',
        // joinBy: ['name', 'country'],
        data : mapCountryData,
        // data: [
        //   ['is', 1],
        //   ['no', 1],
        //   ['se', 1],
        //   ['dk', 1],
        //   ['fi', 1]
        // ],
        // states: {
        //   hover: {
        //     color: '#BADA55'
        //   }
        // },
        // dataLabels: {
        //   enabled: true,
        //   format: '{point.name}'
        // },
        // allAreas: false,
        // joinBy: ['name', 'country'],
        // data : mapCountryData
      } as Highcharts.SeriesMapOptions]
    };

  }
}

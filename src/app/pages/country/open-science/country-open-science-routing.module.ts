import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { CountryOpenScienceComponent } from './country-open-science.component';


const continentOpenScienceRoutes: Routes = [
  {
    path: '',
    component: CountryOpenScienceComponent,
    children : [
      {
        path: '',
        redirectTo: 'publications',
        pathMatch: 'full',
      },
      {
        path: 'publications',
        loadChildren: () => import('./publications/country-os-publications.module').then(m => m.CountryOpenSciencePublicationsModule),
      },
      {
        path: 'datasets',
        loadChildren: () => import('./datasets/country-os-datasets.module').then(m => m.CountryOpenScienceDatasetsModule),
      },
      {
        path: 'software',
        loadChildren: () => import('./software/country-os-software.module').then(m => m.CountryOpenScienceSoftwareModule),
      },
      {
        path: 'other-research-products',
        loadChildren: () => import('./other-research-products/country-os-orp.module').then(m => m.CountryOpenScienceORPModule),
      },
    ]
  },
];

@NgModule ({
  imports: [RouterModule.forChild(continentOpenScienceRoutes)],
  exports: [RouterModule]
})

export class CountryOpenScienceRoutingModule {}

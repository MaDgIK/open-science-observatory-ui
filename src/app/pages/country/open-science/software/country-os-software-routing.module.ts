import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { CountryOSSoftwareComponent } from './country-os-software.component';
import { CountryOSSoftwareIndicatorsComponent } from './country-os-software-indicators.component';

const countryOpenScienceSoftwareRoutes: Routes = [
  {
    path: '',
    component: CountryOSSoftwareComponent,
    children : [
      {
        path: '',
        redirectTo: 'licence',
        pathMatch: 'full',
      },
      {
        path: ':indicator',
        component: CountryOSSoftwareIndicatorsComponent,
      }
    ]
  },
];

@NgModule ({
  imports: [RouterModule.forChild(countryOpenScienceSoftwareRoutes)],
  exports: [RouterModule]
})

export class CountryOpenScienceSoftwareRoutingModule {}

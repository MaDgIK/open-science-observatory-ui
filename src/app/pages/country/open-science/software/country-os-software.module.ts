import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CountryOpenScienceSoftwareRoutingModule } from './country-os-software-routing.module';
import { ReusableComponentsModule } from '../../../../shared/reusablecomponents/reusable-components.module';
import { CountryOSSoftwareComponent } from './country-os-software.component';
import { CountryOSSoftwareIndicatorsComponent } from './country-os-software-indicators.component';
import { DataService } from '../../../../services/data.service';
import { DataHandlerService } from '../../../../services/data-handler.service';


@NgModule ({
  imports: [
    CommonModule,
    CountryOpenScienceSoftwareRoutingModule,
    ReusableComponentsModule,
  ],
  declarations: [
    CountryOSSoftwareComponent,
    CountryOSSoftwareIndicatorsComponent,
  ],
  providers: [
    DataService,
    DataHandlerService
  ],
})

export class CountryOpenScienceSoftwareModule {}

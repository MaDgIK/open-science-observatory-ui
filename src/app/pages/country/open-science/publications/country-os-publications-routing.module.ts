import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { CountryOSPublicationsComponent } from './country-os-publications.component';
import { CountryOSPublicationsIndicatorsComponent } from './country-os-publications-indicators.component';

const countryOpenSciencePublicationsRoutes: Routes = [
  {
    path: '',
    component: CountryOSPublicationsComponent,
    children : [
      {
        path: '',
        redirectTo: 'gold-green',
        pathMatch: 'full',
      },
      {
        path: ':indicator',
        component: CountryOSPublicationsIndicatorsComponent,
      }
    ]
  },
];

@NgModule ({
  imports: [RouterModule.forChild(countryOpenSciencePublicationsRoutes)],
  exports: [RouterModule]
})

export class CountryOpenSciencePublicationsRoutingModule {}

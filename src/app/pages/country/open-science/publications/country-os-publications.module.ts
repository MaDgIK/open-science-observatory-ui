import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CountryOpenSciencePublicationsRoutingModule } from './country-os-publications-routing.module';
import { ReusableComponentsModule } from '../../../../shared/reusablecomponents/reusable-components.module';
import { CountryOSPublicationsComponent } from './country-os-publications.component';
import { CountryOSPublicationsIndicatorsComponent } from './country-os-publications-indicators.component';
import { DataService } from '../../../../services/data.service';
import { DataHandlerService } from '../../../../services/data-handler.service';


@NgModule ({
  imports: [
    CommonModule,
    CountryOpenSciencePublicationsRoutingModule,
    ReusableComponentsModule,
  ],
  declarations: [
    CountryOSPublicationsComponent,
    CountryOSPublicationsIndicatorsComponent,
  ],
  providers: [
    DataService,
    DataHandlerService
  ],
})

export class CountryOpenSciencePublicationsModule {}

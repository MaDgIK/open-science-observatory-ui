import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CountryOpenScienceORPRoutingModule } from './country-os-orp-routing.module';
import { ReusableComponentsModule } from '../../../../shared/reusablecomponents/reusable-components.module';
import { CountryOSORPComponent } from './country-os-orp.component';
import { CountryOSORPIndicatorsComponent } from './country-os-orp-indicators.component';
import { DataService } from '../../../../services/data.service';
import { DataHandlerService } from '../../../../services/data-handler.service';


@NgModule ({
  imports: [
    CommonModule,
    CountryOpenScienceORPRoutingModule,
    ReusableComponentsModule,
  ],
  declarations: [
    CountryOSORPComponent,
    CountryOSORPIndicatorsComponent,
  ],
  providers: [
    DataService,
    DataHandlerService
  ],
})

export class CountryOpenScienceORPModule {}

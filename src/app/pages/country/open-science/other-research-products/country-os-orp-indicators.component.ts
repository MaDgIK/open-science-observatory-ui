import {
  gradientStartColor,
  otherResearchProductsColor, otherResearchProductsPalette, otherIndicatorsPalette,
  resultsPalette
} from '../../../../chart-palettes';
import { Component, OnInit } from '@angular/core';
import { environment } from '../../../../../environments/environment';
import { DataHandlerService } from '../../../../services/data-handler.service';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { DataService } from '../../../../services/data.service';
import { ActivatedRoute } from '@angular/router';
import { TreemapHighchartsData } from '../../../../domain/treemap-highcharts-data';

@Component({
  selector: 'app-country-os-orp-indicators',
  templateUrl: './country-os-orp-indicators.component.html',
})

export class CountryOSORPIndicatorsComponent implements OnInit {

  private chartsURL = environment.API_ENDPOINT + 'chart?json=';
  private profileName = environment.profileName;

  indicator: string;

  private otherResearchProductsPalette = otherResearchProductsPalette;
  private otherIndicatorsPalette = otherIndicatorsPalette;

  otherResearchProductsColor = otherResearchProductsColor;
  gradientStartColor = gradientStartColor;

  countryCode: string;

  otherWithLicenceTimeline: SafeResourceUrl;
  otherWithCCLicenceTimeline: SafeResourceUrl;
  otherWithPIDTimeline: SafeResourceUrl;

  otherWithPIDGroupByView = 'datasource';
  otherWithPIDByDatasourceChartURL: SafeResourceUrl;
  otherWithPIDByOrganizationChartURL: SafeResourceUrl;

  otherWithLicenceGroupByView = 'datasource';
  otherWithLicenceByDatasourceChartURL: SafeResourceUrl;
  otherWithLicenceByOrganizationChartURL: SafeResourceUrl;

  otherWithCCLicenceGroupByView = 'datasource';
  otherWithCCLicenceByDatasourceChartURL: SafeResourceUrl;
  otherWithCCLicenceByOrganizationChartURL: SafeResourceUrl;

  otherWithPIDByFunderData: TreemapHighchartsData[];
  otherWithLicenceByFunderData: TreemapHighchartsData[];
  otherWithCCLicenceByFunderData: TreemapHighchartsData[];

  constructor(private dataService: DataService,
              private dataHandlerService: DataHandlerService,
              private route: ActivatedRoute,
              private sanitizer: DomSanitizer) { }

  ngOnInit(): void {

    this.route.params.subscribe(params => {

      this.indicator = params['indicator'];
    });

    this.route.parent.parent.parent.parent.parent.params.subscribe(params => {

      this.countryCode = params['countryCode'];

      this.otherWithPIDTimeline = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with PID","type":"column","query":{"name":"new.oso.results.pid_timeline.affiliated.country","parameters":["other","${this.countryCode}",1],"profile":"${this.profileName}"}},{"name":"without PID","type":"column","query":{"name":"new.oso.results.pid_timeline.affiliated.country","parameters":["other","${this.countryCode}",0],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access ORPs with PID","align":"left","margin":50},"subtitle":{"text":"over time","align":"left","margin":50},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherIndicatorsPalette.join('","')}\"]}}`));
      this.otherWithLicenceTimeline = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with licence","type":"column","query":{"name":"new.oso.results.licence_timeline.affiliated.country","parameters":["other","${this.countryCode}",1],"profile":"${this.profileName}"}},{"name":"without licence","type":"column","query":{"name":"new.oso.results.licence_timeline.affiliated.country","parameters":["other","${this.countryCode}",0],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access ORPs with licence","align":"left","margin":50},"subtitle":{"text":"over time","align":"left","margin":50},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherIndicatorsPalette.join('","')}\"]}}`));
      this.otherWithCCLicenceTimeline = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with CC licence","type":"column","query":{"name":"new.oso.results.cc_licence_timeline.affiliated.country","parameters":["other","${this.countryCode}",1],"profile":"${this.profileName}"}},{"name":"without CC licence","type":"column","query":{"name":"new.oso.results.cc_licence_timeline.affiliated.country","parameters":["other","${this.countryCode}",0],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access ORPs with CC licence","align":"left","margin":50},"subtitle":{"text":"over time","align":"left","margin":50},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherIndicatorsPalette.join('","')}\"]}}`));

      this.otherWithPIDByDatasourceChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with PID","type":"bar","query":{"name":"new.oso.results.pid.affiliated.bydatasource.country","parameters":[1,"other","${this.countryCode}"],"profile":"${this.profileName}"}},{"name":"without PID","type":"bar","query":{"name":"new.oso.results.pid.affiliated.bydatasource.country","parameters":[0,"other","${this.countryCode}"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access ORPs with PID","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherIndicatorsPalette.join('","')}\"]}}`));
      this.otherWithLicenceByDatasourceChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with licence","type":"bar","query":{"name":"new.oso.results.licence.affiliated.bydatasource.country","parameters":[1,"other","${this.countryCode}"],"profile":"${this.profileName}"}},{"name":"without licence","type":"bar","query":{"name":"new.oso.results.licence.affiliated.bydatasource.country","parameters":[0,"other","${this.countryCode}"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access ORPs with licence","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherIndicatorsPalette.join('","')}\"]}}`));
      this.otherWithCCLicenceByDatasourceChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with CC licence","type":"bar","query":{"name":"new.oso.results.cc_licence.affiliated.bydatasource.country","parameters":[1,"other","${this.countryCode}"],"profile":"${this.profileName}"}},{"name":"without CC licence","type":"bar","query":{"name":"new.oso.results.cc_licence.affiliated.bydatasource.country","parameters":[0,"other","${this.countryCode}"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access ORPs with CC licence","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherIndicatorsPalette.join('","')}\"]}}`));


      this.dataService.getFundersResultsByTypeForIndicatorForCountry('other', 'pid', this.countryCode).subscribe(
        rawData => {
          this.otherWithPIDByFunderData = this.dataHandlerService.convertRawDataToTreemapHighchartsData(rawData);
        }, error => {
          console.log(error);
        }
      );

      this.dataService.getFundersResultsByTypeForIndicatorForCountry('other', 'licence', this.countryCode).subscribe(
        rawData => {
          this.otherWithLicenceByFunderData = this.dataHandlerService.convertRawDataToTreemapHighchartsData(rawData);
        }, error => {
          console.log(error);
        }
      );

      this.dataService.getFundersResultsByTypeForIndicatorForCountry('other', 'cc_licence', this.countryCode).subscribe(
        rawData => {
          this.otherWithCCLicenceByFunderData = this.dataHandlerService.convertRawDataToTreemapHighchartsData(rawData);
        }, error => {
          console.log(error);
        }
      );
    });
  }

  getOtherWithPIDGroupBy(contentSelection: string): void {
    this.otherWithPIDGroupByView = contentSelection;
    if (contentSelection === 'datasource' && !this.otherWithPIDByDatasourceChartURL) {
      this.otherWithPIDByDatasourceChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with PID","type":"bar","query":{"name":"new.oso.results.pid.affiliated.bydatasource.country","parameters":[1,"other","${this.countryCode}"],"profile":"${this.profileName}"}},{"name":"without PID","type":"bar","query":{"name":"new.oso.results.pid.affiliated.bydatasource.country","parameters":[0,"other","${this.countryCode}"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access ORPs with PID","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'organization' && !this.otherWithPIDByOrganizationChartURL) {
      this.otherWithPIDByOrganizationChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with PID","type":"bar","query":{"name":"new.oso.results.pid.affiliated.byorganization.country","parameters":[1,"other","${this.countryCode}"],"profile":"${this.profileName}"}},{"name":"without PID","type":"bar","query":{"name":"new.oso.results.pid.affiliated.byorganization.country","parameters":[0,"other","${this.countryCode}"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access ORPs with PID","align":"left","margin":50},"subtitle":{"text":"by organization (top organizations)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherIndicatorsPalette.join('","')}\"]}}`));
    }
  }

  getOtherWithPIDGroupByMobile(contentSelection: string): void {
    this.otherWithPIDGroupByView = contentSelection;
    if (contentSelection === 'datasource' && !this.otherWithPIDByDatasourceChartURL) {
      this.otherWithPIDByDatasourceChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with PID","type":"bar","query":{"name":"new.oso.results.pid.affiliated.bydatasource.country","parameters":[1,"other","${this.countryCode}"],"profile":"${this.profileName}"}},{"name":"without PID","type":"bar","query":{"name":"new.oso.results.pid.affiliated.bydatasource.country","parameters":[0,"other","${this.countryCode}"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access ORPs with PID","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'organization' && !this.otherWithPIDByOrganizationChartURL) {
      this.otherWithPIDByOrganizationChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with PID","type":"bar","query":{"name":"new.oso.results.pid.affiliated.byorganization.country","parameters":[1,"other","${this.countryCode}"],"profile":"${this.profileName}"}},{"name":"without PID","type":"bar","query":{"name":"new.oso.results.pid.affiliated.byorganization.country","parameters":[0,"other","${this.countryCode}"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access ORPs with PID","align":"left","margin":50},"subtitle":{"text":"by organization (top organizations)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherIndicatorsPalette.join('","')}\"]}}`));
    }
  }

  getOtherWithLicenceGroupBy(contentSelection: string): void {
    this.otherWithLicenceGroupByView = contentSelection;
    if (contentSelection === 'datasource' && !this.otherWithLicenceByDatasourceChartURL) {
      this.otherWithLicenceByDatasourceChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with licence","type":"bar","query":{"name":"new.oso.results.licence.affiliated.bydatasource.country","parameters":[1,"other","${this.countryCode}"],"profile":"${this.profileName}"}},{"name":"without licence","type":"bar","query":{"name":"new.oso.results.licence.affiliated.bydatasource.country","parameters":[0,"other","${this.countryCode}"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access ORPs with licence","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'organization' && !this.otherWithLicenceByOrganizationChartURL) {
      this.otherWithLicenceByOrganizationChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with licence","type":"bar","query":{"name":"new.oso.results.licence.affiliated.byorganization.country","parameters":[1,"other","${this.countryCode}"],"profile":"${this.profileName}"}},{"name":"without licence","type":"bar","query":{"name":"new.oso.results.licence.affiliated.byorganization.country","parameters":[0,"other","${this.countryCode}"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access ORPs with licence","align":"left","margin":50},"subtitle":{"text":"by organization (top organizations)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherIndicatorsPalette.join('","')}\"]}}`));
    }
  }

  getOtherWithLicenceGroupByMobile(contentSelection: string): void {
    this.otherWithLicenceGroupByView = contentSelection;
    if (contentSelection === 'datasource' && !this.otherWithLicenceByDatasourceChartURL) {
      this.otherWithLicenceByDatasourceChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with licence","type":"bar","query":{"name":"new.oso.results.licence.affiliated.bydatasource.country","parameters":[1,"other","${this.countryCode}"],"profile":"${this.profileName}"}},{"name":"without licence","type":"bar","query":{"name":"new.oso.results.licence.affiliated.bydatasource.country","parameters":[0,"other","${this.countryCode}"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access ORPs with licence","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'organization' && !this.otherWithLicenceByOrganizationChartURL) {
      this.otherWithLicenceByOrganizationChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with licence","type":"bar","query":{"name":"new.oso.results.licence.affiliated.byorganization.country","parameters":[1,"other","${this.countryCode}"],"profile":"${this.profileName}"}},{"name":"without licence","type":"bar","query":{"name":"new.oso.results.licence.affiliated.byorganization.country","parameters":[0,"other","${this.countryCode}"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access ORPs with licence","align":"left","margin":50},"subtitle":{"text":"by organization (top organizations)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherIndicatorsPalette.join('","')}\"]}}`));
    }
  }

  getOtherWithCCLicenceGroupBy(contentSelection: string): void {
    this.otherWithCCLicenceGroupByView = contentSelection;
    if (contentSelection === 'datasource' && !this.otherWithCCLicenceByDatasourceChartURL) {
      this.otherWithCCLicenceByDatasourceChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with CC licence","type":"bar","query":{"name":"new.oso.results.cc_licence.affiliated.bydatasource.country","parameters":[1,"other","${this.countryCode}"],"profile":"${this.profileName}"}},{"name":"without CC licence","type":"bar","query":{"name":"new.oso.results.cc_licence.affiliated.bydatasource.country","parameters":[0,"other","${this.countryCode}"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access ORPs with CC licence","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'organization' && !this.otherWithCCLicenceByOrganizationChartURL) {
      this.otherWithCCLicenceByOrganizationChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with CC licence","type":"bar","query":{"name":"new.oso.results.cc_licence.affiliated.byorganization.country","parameters":[1,"other","${this.countryCode}"],"profile":"${this.profileName}"}},{"name":"without CC licence","type":"bar","query":{"name":"new.oso.results.cc_licence.affiliated.byorganization.country","parameters":[0,"other","${this.countryCode}"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access ORPs with CC licence","align":"left","margin":50},"subtitle":{"text":"by organization (top organizations)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherIndicatorsPalette.join('","')}\"]}}`));
    }
  }

  getOtherWithCCLicenceGroupByMobile(contentSelection: string): void {
    this.otherWithCCLicenceGroupByView = contentSelection;
    if (contentSelection === 'datasource' && !this.otherWithCCLicenceByDatasourceChartURL) {
      this.otherWithCCLicenceByDatasourceChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with CC licence","type":"bar","query":{"name":"new.oso.results.cc_licence.affiliated.bydatasource.country","parameters":[1,"other","${this.countryCode}"],"profile":"${this.profileName}"}},{"name":"without CC licence","type":"bar","query":{"name":"new.oso.results.cc_licence.affiliated.bydatasource.country","parameters":[0,"other","${this.countryCode}"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access ORPs with CC licence","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'organization' && !this.otherWithCCLicenceByOrganizationChartURL) {
      this.otherWithCCLicenceByOrganizationChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with CC licence","type":"bar","query":{"name":"new.oso.results.cc_licence.affiliated.byorganization.country","parameters":[1,"other","${this.countryCode}"],"profile":"${this.profileName}"}},{"name":"without CC licence","type":"bar","query":{"name":"new.oso.results.cc_licence.affiliated.byorganization.country","parameters":[0,"other","${this.countryCode}"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access ORPs with CC licence","align":"left","margin":50},"subtitle":{"text":"by organization (top organizations)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherIndicatorsPalette.join('","')}\"]}}`));
    }
  }

}

import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { CountryOSORPComponent } from './country-os-orp.component';
import { CountryOSORPIndicatorsComponent } from './country-os-orp-indicators.component';

const countryOpenScienceORPRoutes: Routes = [
  {
    path: '',
    component: CountryOSORPComponent,
    children : [
      {
        path: '',
        redirectTo: 'licence',
        pathMatch: 'full',
      },
      {
        path: ':indicator',
        component: CountryOSORPIndicatorsComponent,
      }
    ]
  },
];

@NgModule ({
  imports: [RouterModule.forChild(countryOpenScienceORPRoutes)],
  exports: [RouterModule]
})

export class CountryOpenScienceORPRoutingModule {}

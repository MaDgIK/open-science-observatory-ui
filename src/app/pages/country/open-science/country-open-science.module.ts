import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReusableComponentsModule } from '../../../shared/reusablecomponents/reusable-components.module';
import { DataService } from '../../../services/data.service';
import { DataHandlerService } from '../../../services/data-handler.service';
import { CountryOpenScienceRoutingModule } from './country-open-science-routing.module';
import { CountryOpenScienceComponent } from './country-open-science.component';

@NgModule ({
  imports: [
    CommonModule,
    CountryOpenScienceRoutingModule,
    ReusableComponentsModule,
  ],
  declarations: [
    CountryOpenScienceComponent
  ],
  providers: [
    DataService,
    DataHandlerService
  ],
})

export class CountryOpenScienceModule {}

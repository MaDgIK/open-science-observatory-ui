import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { CountryOSDatasetsComponent } from './country-os-datasets.component';
import { CountryOSDatasetsIndicatorsComponent } from './country-os-datasets-indicators.component';

const countryOpenScienceDatasetsRoutes: Routes = [
  {
    path: '',
    component: CountryOSDatasetsComponent,
    children : [
      {
        path: '',
        redirectTo: 'licence',
        pathMatch: 'full',
      },
      {
        path: ':indicator',
        component: CountryOSDatasetsIndicatorsComponent,
      }
    ]
  },
];

@NgModule ({
  imports: [RouterModule.forChild(countryOpenScienceDatasetsRoutes)],
  exports: [RouterModule]
})

export class CountryOpenScienceDatasetsRoutingModule {}

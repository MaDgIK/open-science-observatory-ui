import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CountryOpenScienceDatasetsRoutingModule } from './country-os-datasets-routing.module';
import { ReusableComponentsModule } from '../../../../shared/reusablecomponents/reusable-components.module';
import { CountryOSDatasetsComponent } from './country-os-datasets.component';
import { CountryOSDatasetsIndicatorsComponent } from './country-os-datasets-indicators.component';
import { DataService } from '../../../../services/data.service';
import { DataHandlerService } from '../../../../services/data-handler.service';


@NgModule ({
  imports: [
    CommonModule,
    CountryOpenScienceDatasetsRoutingModule,
    ReusableComponentsModule,
  ],
  declarations: [
    CountryOSDatasetsComponent,
    CountryOSDatasetsIndicatorsComponent,
  ],
  providers: [
    DataService,
    DataHandlerService
  ],
})

export class CountryOpenScienceDatasetsModule {}

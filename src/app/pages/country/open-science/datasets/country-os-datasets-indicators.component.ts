import {
  datasetColor,
  datasetPalette, datasetIndicatorsPalette,
  gradientStartColor
} from '../../../../chart-palettes';
import { Component, OnInit } from '@angular/core';
import { environment } from '../../../../../environments/environment';
import { DataHandlerService } from '../../../../services/data-handler.service';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { DataService } from '../../../../services/data.service';
import { ActivatedRoute } from '@angular/router';
import { TreemapHighchartsData } from '../../../../domain/treemap-highcharts-data';

@Component({
  selector: 'app-country-os-datasets-indicators',
  templateUrl: './country-os-datasets-indicators.component.html',
})

export class CountryOSDatasetsIndicatorsComponent implements OnInit {

  private chartsURL = environment.API_ENDPOINT + 'chart?json=';
  private profileName = environment.profileName;

  indicator: string;

  private datasetPalette = datasetPalette;
  private datasetIndicatorsPalette = datasetIndicatorsPalette;

  datasetColor = datasetColor;
  gradientStartColor = gradientStartColor;

  countryCode: string;

  datasetsWithLicenceTimeline: SafeResourceUrl;
  datasetsWithCCLicenceTimeline: SafeResourceUrl;
  datasetsWithPIDTimeline: SafeResourceUrl;

  datasetsWithPIDGroupByView = 'datasource';
  datasetsWithPIDByDatasourceChartURL: SafeResourceUrl;
  datasetsWithPIDByOrganizationChartURL: SafeResourceUrl;

  datasetsWithLicenceGroupByView = 'datasource';
  datasetsWithLicenceByDatasourceChartURL: SafeResourceUrl;
  datasetsWithLicenceByOrganizationChartURL: SafeResourceUrl;

  datasetsWithCCLicenceGroupByView = 'datasource';
  datasetsWithCCLicenceByDatasourceChartURL: SafeResourceUrl;
  datasetsWithCCLicenceByOrganizationChartURL: SafeResourceUrl;

  datasetsWithPIDByFunderData: TreemapHighchartsData[];
  datasetsWithLicenceByFunderData: TreemapHighchartsData[];
  datasetsWithCCLicenceByFunderData: TreemapHighchartsData[];

  constructor(private dataService: DataService,
              private dataHandlerService: DataHandlerService,
              private route: ActivatedRoute,
              private sanitizer: DomSanitizer) { }

  ngOnInit(): void {

    this.route.params.subscribe(params => {

      this.indicator = params['indicator'];
    });

    this.route.parent.parent.parent.parent.parent.params.subscribe(params => {

      this.countryCode = params['countryCode'];

      this.datasetsWithPIDTimeline = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with PID","type":"column","query":{"name":"new.oso.results.pid_timeline.affiliated.country","parameters":["dataset","${this.countryCode}",1],"profile":"${this.profileName}"}},{"name":"without PID","type":"column","query":{"name":"new.oso.results.pid_timeline.affiliated.country","parameters":["dataset","${this.countryCode}",0],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets with PID","align":"left","margin":50},"subtitle":{"text":"over time","align":"left","margin":50},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetIndicatorsPalette.join('","')}\"]}}`));
      this.datasetsWithLicenceTimeline = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with licence","type":"column","query":{"name":"new.oso.results.licence_timeline.affiliated.country","parameters":["dataset","${this.countryCode}",1],"profile":"${this.profileName}"}},{"name":"without licence","type":"column","query":{"name":"new.oso.results.licence_timeline.affiliated.country","parameters":["dataset","${this.countryCode}",0],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets with licence","align":"left","margin":50},"subtitle":{"text":"over time","align":"left","margin":50},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetIndicatorsPalette.join('","')}\"]}}`));
      this.datasetsWithCCLicenceTimeline = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with CC licence","type":"column","query":{"name":"new.oso.results.cc_licence_timeline.affiliated.country","parameters":["dataset","${this.countryCode}",1],"profile":"${this.profileName}"}},{"name":"without CC licence","type":"column","query":{"name":"new.oso.results.cc_licence_timeline.affiliated.country","parameters":["dataset","${this.countryCode}",0],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets with CC licence","align":"left","margin":50},"subtitle":{"text":"over time","align":"left","margin":50},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetIndicatorsPalette.join('","')}\"]}}`));

      this.datasetsWithPIDByDatasourceChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with PID","type":"bar","query":{"name":"new.oso.results.pid.affiliated.bydatasource.country","parameters":[1,"dataset","${this.countryCode}"],"profile":"${this.profileName}"}},{"name":"without PID","type":"bar","query":{"name":"new.oso.results.pid.affiliated.bydatasource.country","parameters":[0,"dataset","${this.countryCode}"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets with PID","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetIndicatorsPalette.join('","')}\"]}}`));
      this.datasetsWithLicenceByDatasourceChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with licence","type":"bar","query":{"name":"new.oso.results.licence.affiliated.bydatasource.country","parameters":[1,"dataset","${this.countryCode}"],"profile":"${this.profileName}"}},{"name":"without licence","type":"bar","query":{"name":"new.oso.results.licence.affiliated.bydatasource.country","parameters":[0,"dataset","${this.countryCode}"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets with licence","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetIndicatorsPalette.join('","')}\"]}}`));
      this.datasetsWithCCLicenceByDatasourceChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with CC licence","type":"bar","query":{"name":"new.oso.results.cc_licence.affiliated.bydatasource.country","parameters":[1,"dataset","${this.countryCode}"],"profile":"${this.profileName}"}},{"name":"without CC licence","type":"bar","query":{"name":"new.oso.results.cc_licence.affiliated.bydatasource.country","parameters":[0,"dataset","${this.countryCode}"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets with CC licence","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetIndicatorsPalette.join('","')}\"]}}`));

      this.dataService.getFundersResultsByTypeForIndicatorForCountry('dataset', 'pid', this.countryCode).subscribe(
        rawData => {
          this.datasetsWithPIDByFunderData = this.dataHandlerService.convertRawDataToTreemapHighchartsData(rawData);
        }, error => {
          console.log(error);
        }
      );

      this.dataService.getFundersResultsByTypeForIndicatorForCountry('dataset', 'licence', this.countryCode).subscribe(
        rawData => {
          this.datasetsWithLicenceByFunderData = this.dataHandlerService.convertRawDataToTreemapHighchartsData(rawData);
        }, error => {
          console.log(error);
        }
      );

      this.dataService.getFundersResultsByTypeForIndicatorForCountry('dataset', 'cc_licence', this.countryCode).subscribe(
        rawData => {
          this.datasetsWithCCLicenceByFunderData = this.dataHandlerService.convertRawDataToTreemapHighchartsData(rawData);
        }, error => {
          console.log(error);
        }
      );
    });
  }

  getDatasetsWithPIDGroupBy(contentSelection: string): void {
    this.datasetsWithPIDGroupByView = contentSelection;
    if (contentSelection === 'datasource' && !this.datasetsWithPIDByDatasourceChartURL) {
      this.datasetsWithPIDByDatasourceChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with PID","type":"bar","query":{"name":"new.oso.results.pid.affiliated.bydatasource.country","parameters":[1,"dataset","${this.countryCode}"],"profile":"${this.profileName}"}},{"name":"without PID","type":"bar","query":{"name":"new.oso.results.pid.affiliated.bydatasource.country","parameters":[0,"dataset","${this.countryCode}"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets with PID","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'organization' && !this.datasetsWithPIDByOrganizationChartURL) {
      this.datasetsWithPIDByOrganizationChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with PID","type":"bar","query":{"name":"new.oso.results.pid.affiliated.byorganization.country","parameters":[1,"dataset","${this.countryCode}"],"profile":"${this.profileName}"}},{"name":"without PID","type":"bar","query":{"name":"new.oso.results.pid.affiliated.byorganization.country","parameters":[0,"dataset","${this.countryCode}"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets with PID","align":"left","margin":50},"subtitle":{"text":"by organization (top organizations)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetIndicatorsPalette.join('","')}\"]}}`));
    }
  }

  getDatasetsWithPIDGroupByMobile(contentSelection: string): void {
    this.datasetsWithPIDGroupByView = contentSelection;
    if (contentSelection === 'datasource' && !this.datasetsWithPIDByDatasourceChartURL) {
      this.datasetsWithPIDByDatasourceChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with PID","type":"bar","query":{"name":"new.oso.results.pid.affiliated.bydatasource.country","parameters":[1,"dataset","${this.countryCode}"],"profile":"${this.profileName}"}},{"name":"without PID","type":"bar","query":{"name":"new.oso.results.pid.affiliated.bydatasource.country","parameters":[0,"dataset","${this.countryCode}"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets with PID","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'organization' && !this.datasetsWithPIDByOrganizationChartURL) {
      this.datasetsWithPIDByOrganizationChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with PID","type":"bar","query":{"name":"new.oso.results.pid.affiliated.byorganization.country","parameters":[1,"dataset","${this.countryCode}"],"profile":"${this.profileName}"}},{"name":"without PID","type":"bar","query":{"name":"new.oso.results.pid.affiliated.byorganization.country","parameters":[0,"dataset","${this.countryCode}"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets with PID","align":"left","margin":50},"subtitle":{"text":"by organization (top organizations)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetIndicatorsPalette.join('","')}\"]}}`));
    }
  }

  getDatasetsWithLicenceGroupBy(contentSelection: string): void {
    this.datasetsWithLicenceGroupByView = contentSelection;
    if (contentSelection === 'datasource' && !this.datasetsWithLicenceByDatasourceChartURL) {
      this.datasetsWithLicenceByDatasourceChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with licence","type":"bar","query":{"name":"new.oso.results.licence.affiliated.bydatasource.country","parameters":[1,"dataset","${this.countryCode}"],"profile":"${this.profileName}"}},{"name":"without licence","type":"bar","query":{"name":"new.oso.results.licence.affiliated.bydatasource.country","parameters":[0,"dataset","${this.countryCode}"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets with licence","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'organization' && !this.datasetsWithLicenceByOrganizationChartURL) {
      this.datasetsWithLicenceByOrganizationChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with licence","type":"bar","query":{"name":"new.oso.results.licence.affiliated.byorganization.country","parameters":[1,"dataset","${this.countryCode}"],"profile":"${this.profileName}"}},{"name":"without licence","type":"bar","query":{"name":"new.oso.results.licence.affiliated.byorganization.country","parameters":[0,"dataset","${this.countryCode}"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets with licence","align":"left","margin":50},"subtitle":{"text":"by organization (top organizations)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetIndicatorsPalette.join('","')}\"]}}`));
    }
  }

  getDatasetsWithLicenceGroupByMobile(contentSelection: string): void {
    this.datasetsWithLicenceGroupByView = contentSelection;
    if (contentSelection === 'datasource' && !this.datasetsWithLicenceByDatasourceChartURL) {
      this.datasetsWithLicenceByDatasourceChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with licence","type":"bar","query":{"name":"new.oso.results.licence.affiliated.bydatasource.country","parameters":[1,"dataset","${this.countryCode}"],"profile":"${this.profileName}"}},{"name":"without licence","type":"bar","query":{"name":"new.oso.results.licence.affiliated.bydatasource.country","parameters":[0,"dataset","${this.countryCode}"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets with licence","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'organization' && !this.datasetsWithLicenceByOrganizationChartURL) {
      this.datasetsWithLicenceByOrganizationChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with licence","type":"bar","query":{"name":"new.oso.results.licence.affiliated.byorganization.country","parameters":[1,"dataset","${this.countryCode}"],"profile":"${this.profileName}"}},{"name":"without licence","type":"bar","query":{"name":"new.oso.results.licence.affiliated.byorganization.country","parameters":[0,"dataset","${this.countryCode}"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets with licence","align":"left","margin":50},"subtitle":{"text":"by organization (top organizations)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetIndicatorsPalette.join('","')}\"]}}`));
    }
  }

  getDatasetsWithCCLicenceGroupBy(contentSelection: string): void {
    this.datasetsWithCCLicenceGroupByView = contentSelection;
    if (contentSelection === 'datasource' && !this.datasetsWithCCLicenceByDatasourceChartURL) {
      this.datasetsWithCCLicenceByDatasourceChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with CC licence","type":"bar","query":{"name":"new.oso.results.cc_licence.affiliated.bydatasource.country","parameters":[1,"dataset","${this.countryCode}"],"profile":"${this.profileName}"}},{"name":"without CC licence","type":"bar","query":{"name":"new.oso.results.cc_licence.affiliated.bydatasource.country","parameters":[0,"dataset","${this.countryCode}"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets with CC licence","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'organization' && !this.datasetsWithCCLicenceByOrganizationChartURL) {
      this.datasetsWithCCLicenceByOrganizationChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with CC licence","type":"bar","query":{"name":"new.oso.results.cc_licence.affiliated.byorganization.country","parameters":[1,"dataset","${this.countryCode}"],"profile":"${this.profileName}"}},{"name":"without CC licence","type":"bar","query":{"name":"new.oso.results.cc_licence.affiliated.byorganization.country","parameters":[0,"dataset","${this.countryCode}"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets with CC licence","align":"left","margin":50},"subtitle":{"text":"by organization (top organizations)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetIndicatorsPalette.join('","')}\"]}}`));
    }
  }

  getDatasetsWithCCLicenceGroupByMobile(contentSelection: string): void {
    this.datasetsWithCCLicenceGroupByView = contentSelection;
    if (contentSelection === 'datasource' && !this.datasetsWithCCLicenceByDatasourceChartURL) {
      this.datasetsWithCCLicenceByDatasourceChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with CC licence","type":"bar","query":{"name":"new.oso.results.cc_licence.affiliated.bydatasource.country","parameters":[1,"dataset","${this.countryCode}"],"profile":"${this.profileName}"}},{"name":"without CC licence","type":"bar","query":{"name":"new.oso.results.cc_licence.affiliated.bydatasource.country","parameters":[0,"dataset","${this.countryCode}"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets with CC licence","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'organization' && !this.datasetsWithCCLicenceByOrganizationChartURL) {
      this.datasetsWithCCLicenceByOrganizationChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with CC licence","type":"bar","query":{"name":"new.oso.results.cc_licence.affiliated.byorganization.country","parameters":[1,"dataset","${this.countryCode}"],"profile":"${this.profileName}"}},{"name":"without CC licence","type":"bar","query":{"name":"new.oso.results.cc_licence.affiliated.byorganization.country","parameters":[0,"dataset","${this.countryCode}"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets with CC licence","align":"left","margin":50},"subtitle":{"text":"by organization (top organizations)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetIndicatorsPalette.join('","')}\"]}}`));
    }
  }

}

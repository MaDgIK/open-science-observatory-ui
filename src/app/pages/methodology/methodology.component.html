<section class="section greySection terminologySection">

  <div id="how" class="uk-container uk-container-large uk-container-center uk-scrollspy-init-inview uk-scrollspy-inview uk-animation-scale-up" data-uk-scrollspy="{cls:'uk-animation-scale-up uk-invisible',delay:300,topoffset:-100}">
    <h2 class="uk-text-center">
      Approach
    </h2>

    <div class="uk-margin-large-top uk-card uk-card-default uk-card-body">
      <p class="uk-margin-top">Our methodological approach is based on the following operational quality criteria:</p>
      <ul>
        <li><span class="uk-text-bold">Openness and transparency:</span> Methodological assumptions are openly and clearly presented.</li>
        <li><span class="uk-text-bold">Coverage and accuracy:</span> As detailed in <a href="https://graph.openaire.eu/" target="_blank">graph.openaire.eu</a>
          multiple data sources are ingested in the OpenAIRE research graph for coverage to the fullest extent possible, in order to provide meaningful indicators.</li>
        <li><span class="uk-text-bold">Clarity and replicability:</span> We describe our construction methodology in detail, so that
          it can be verified and used by the scholarly communication community to create ongoing updates to our proposed statistics and indicators.</li>
        <li><span class="uk-text-bold">Readiness and timeliness:</span> The methodology is built around well-established open databases
          and already tested knowledge extraction technologies - natural language processing (NLP)/machine-learning (ML) - using operational
          workflows in OpenAIRE to warrant timely results.</li>
        <li><span class="uk-text-bold">Trust and robustness:</span> Our methodology also strives to be reliable, robust, and aligned
          to other assessment methods so that it can be operationalized, used and reused, in conjunction with other assessment methods.</li>
      </ul>
      <div class="uk-text-small uk-text-italic uk-text-right">The text above is modified from <a href="https://op.europa.eu/en/publication-detail/-/publication/56cc104f-0ebb-11ec-b771-01aa75ed71a1"
                                                                                          target="_blank">this report</a> (DOI: 10.2777/268348).</div>
    </div>

    <div class="uk-margin-large-top">

      <h3>Step-by-step</h3>

      <div class="how uk-h6">
        <div class="uk-flex uk-flex-wrap">
          <div class="uk-width-1-3@s first uk-text-muted">
            <div>
              <img src="../../../assets/img/methodology/1.png">
            </div>
            <div class="uk-text-center">
              <span class="uk-text-bold uk-text-uppercase">Starting</span> from existing<br>research-related data sources
            </div>
          </div>
          <div class="uk-width-1-3@s second uk-text-muted">
            <div>
              <img src="../../../assets/img/methodology/2.png">
            </div>
          </div>
          <div class="uk-width-1-3@s third uk-text-muted">
            <div>
              <img src="../../../assets/img/methodology/3.png">
            </div>
            <div class="uk-text-center">
              build an open, global<br>and trusted Research graph
            </div>
          </div>
        </div>
        <div class="uk-flex uk-flex-wrap">
          <div class="uk-width-1-3@s fourth uk-flex-last@s uk-text-muted">
            <div>
              <img src="../../../assets/img/methodology/4.png">
            </div>
            <div class="uk-text-center">
              we perform <span class="uk-text-bold uk-text-uppercase">Statistical Analysis</span> and produce
              <span class="portal-color">Open Science Indicators</span>
            </div>
          </div>
          <div class="uk-width-1-3@s fifth uk-text-muted">
            <div>
              <img src="../../../assets/img/methodology/5.png">
            </div>
            <div class="uk-text-center">
              furthermore <span class="uk-text-bold uk-text-uppercase">Network<br>Analysis</span> producing
              <span class="portal-color">Collaboration Indicators</span>
            </div>
          </div>
          <div class="uk-width-1-3@s sixth uk-flex-first@s uk-text-muted">
            <div>
              <img src="../../../assets/img/methodology/6.png">
            </div>
            <div class="uk-text-center">
              Often combine with external data
              (patents, social, company) and
              perform <span class="uk-text-bold uk-text-uppercase">Impact Analysis</span> to
              produce <span class="portal-color">Innovation Indicators</span>
            </div>
          </div>
        </div>
        <div class="final uk-text-muted">
          <div>
            <img src="../../../assets/img/methodology/Asset%201.png">
          </div>
        </div>
      </div>
    </div>
  </div>

</section>

<section class="section greySection terminologySection" id="sect-tabs">

  <div id="terminology" class="uk-container uk-container-large uk-container-center uk-scrollspy-init-inview uk-scrollspy-inview uk-animation-scale-up" data-uk-scrollspy="{cls:'uk-animation-scale-up uk-invisible',delay:300,topoffset:-100}">
    <h2 class="uk-text-center">
      Terminology and construction
    </h2>
    <div class="uk-margin-large-top">

      <!--LAPTOP & PAD LANDSCAPE-->
      <ul class="uk-visible@m uk-tab uk-tab-large uk-flex-center" data-uk-tab="{connect:'#terminology_tabbed',animation: 'slide-bottom'}">
        <li class="uk-active" aria-expanded="true"><a>Entities</a></li>
        <li aria-expanded="false"><a>Inherited and Inferred Attributes</a></li>
        <li aria-expanded="false"><a>Constructed Attributes</a></li>
      </ul>

      <!--MOBILE & PAD PORTRAIT-->
      <ul class="uk-hidden@m uk-tab" data-uk-tab="{connect:'#terminology_tabbed',animation: 'slide-bottom'}">
        <li class="uk-active" aria-expanded="true"><a>Entities</a></li>
        <li aria-expanded="false"><a>Inherited and Inferred Attributes</a></li>
        <li aria-expanded="false"><a>Constructed Attributes</a></li>
      </ul>

      <ul id="terminology_tabbed" class="uk-switcher">

        <!--Entities tab-->
        <li aria-hidden="false" class="uk-active" style="animation-duration: 200ms;">
          <table>
            <tr>
              <th class="important">Research Outcomes</th>
              <td>
                <div>There are currently four different types of research outcomes in the OpenAIRE Research
                  Graph:
                </div>
                <ul class="portal-circle">
                  <li>publications</li>
                  <li>datasets</li>
                  <li>software</li>
                  <li>other research products</li>
                </ul>
                <div class="uk-margin-small-top">
                  OpenAIRE deduplicates (merges) different records of research outcomes and keeps the metadata of all
                  instances.
                </div>
              </td>
            </tr>
            <tr>
              <th>publication</th>
              <td>
                <p>Research outcome intended for human reading (published articles, pre-prints, conference papers,
                  presentations, technical reports, etc.)</p>
                <p>
                  Only peer-reviewed publications are considered in the OS Observatory, unless explicitly stated otherwise
                  (see tab Constructed Attributes for definition of peer-review).
                </p>
              </td>
            </tr>
            <tr>
              <th>dataset</th>
              <td>
                <p>Research data</p>
                <p>Granularity is not defined by OpenAIRE, it reflects the granularity supported by
                  the sources from which the description of the dataset has been collected.</p>
              </td>
            </tr>
            <tr>
              <th>software</th>
              <td>Source code or software package developed and/or used in a research context</td>
            </tr>
            <tr>
              <th>other research product</th>
              <td>Anything that does not fall in the previous categories (e.g. workflow, methods, protocols)</td>
            </tr>
          </table>
          <div class="uk-text-small uk-margin-small-top">
            <img src="../../../assets/img/Open_Research_Graph.svg" style="opacity: 0.4">
            <span class="uk-margin-small-left uk-text-baseline uk-text-muted">More information at <a
              href="https://graph.openaire.eu" class="graph-color">OpenAIRE Research Graph</a>.</span>
          </div>
        </li>

        <!--Inherited and Inferred Attributes tab-->
        <li aria-hidden="true" style="animation-duration: 200ms;">
          <div>
            <p class="uk-text-center">
              The attributes of entities listed below are either inherited via entries in the harvested metadata records
              or automatically generated by our inference (text and data mining) algorithms.
            </p>
          </div>
          <table>
            <!--<tr>-->
              <!--<th>Organization & Country</th>-->
              <!--<td>-->
                <!--<p><span class="uk-text-bold">For research outcomes:</span> the affiliated organizations of its-->
                  <!--authors (and their country)</p>-->
                <!--<p><span class="uk-text-bold">For projects:</span> the organizations participating in the project-->
                  <!--(i.e. beneficiaries of the grant) and-->
                  <!--their countries-->
                <!--</p>-->
                <!--<p>-->
                  <!--<span class="uk-text-bold">Country code mapping: </span>-->
                  <!--<a href="https://api.openaire.eu/vocabularies/dnet:countries" target="_blank">-->
                    <!--https://api.openaire.eu/vocabularies/dnet:countries</a>-->
                <!--</p>-->
              <!--</td>-->
            <!--</tr>-->
            <tr>
              <th>Organization</th>
              <td>
                <p>For research outcomes, this refers to the affiliated organizations of its authors</p>
                <p>For projects: the organizations participating in the project (i.e. beneficiaries of the grant)</p>
                <p>Country code mapping: <a href="https://api.openaire.eu/vocabularies/dnet:countries"
                                            target="_blank">https://api.openaire.eu/vocabularies/dnet:countries</a>
                </p>
                <p>The OpenAIRE research graph is in the process of improving the organization database with the newly developed
                  <a href="https://beta.orgs.openaire.eu/" target="_blank">OpenOrgs</a> tool.</p>
              </td>
            </tr>
            <tr>
              <th>Country</th>
              <td>
                <p>Unless stated otherwise this refers to the country of the affiliated organization</p>
                <p>The OS Observatory relies on the coverage and quality of this attribute (as we present results at the country-level),
                  therefore we are continuously working on improving it, via ingesting additional affiliation links and cleaning the data.</p>
              </td>
            </tr>
            <tr>
              <th>Share of GDP for R&D</th>
              <td>
                <p>The share of the Gross Domestic Product of a country that has gone into research and development. We
                  are currently using 2018 data as presented <a href="https://ec.europa.eu/eurostat/web/products-datasets/-/tsc00001"
                                                                target="_blank">here</a>.</p>
              </td>
            </tr>
            <tr>
              <th>Funder</th>
              <td>
                <p>Funders in OpenAIRE; that is funders for which there is a linked project or research outcome in the OpenAIRE
                  Research Graph. Please check the Funder filter on the sidebar, <a href="https://explore.openaire.eu/search/find"
                                                                                                        target="_blank">here</a>, for a list.</p>
              </td>
            </tr>
            <tr>
              <th>Funding organization</th>
              <td>
                <p>As listed in the registry of grant-giving organizations: <a href="https://www.crossref.org/services/funder-registry/"
                                                                               target="_blank">https://www.crossref.org/services/funder-registry/</a></p>
              </td>
            </tr>
            <tr>
              <th>Type</th>
              <td>
                <p>The sub-type of a research outcome (e.g., a publication can be a pre-print, conference proceeding,
                  article,
                  etc.)</p>
                <p><span class="uk-text-bold">Resource type mapping: </span>
                  <a href="https://api.openaire.eu/vocabularies/dnet:result_typologies" target="_blank">https://api.openaire.eu/vocabularies/dnet:result_typologies</a>
                  (click on the code to see the specific types for each result type)
                </p>
              </td>
            </tr>
            <tr>
              <th>Access mode</th>
              <td>
                <p>The best available (across all instances) access rights of a research outcome</p>
                <p>Types: open access (or open source for software), restricted, closed, embargo (= closed for a specific period of time, then open)</p>
                <p><span class="uk-text-bold">Note:</span> definition of <span class="uk-text-bold">restricted</span>
                  may vary by data source, it may refer to access rights being given to registered users, potentially behind a paywall.</p>
              </td>
            </tr>
            <tr>
              <th>CC licence</th>
              <td>
                <p>A Creative Commons copyright license (<a href="https://creativecommons.org/" target="_blank">https://creativecommons.org/</a>)</p>
              </td>
            </tr>
            <tr>
              <th>PID (persistent identifier)</th>
              <td>
                <p>A long-lasting reference to a resource</p>
                <p><span class="uk-text-bold">Types: </span> <a
                  href="http://api.openaire.eu/vocabularies/dnet:pid_types" target="_blank">http://api.openaire.eu/vocabularies/dnet:pid_types</a>
                </p>
              </td>
            </tr>
            <tr>
              <th>Context</th>
              <td>Related research community, initiative or infrastructure</td>
            </tr>
            <tr>
              <th>Journal</th>
              <td>The scientific journal an article is published in.</td>
            </tr>
            <tr>
              <th>Publisher</th>
              <td>The publisher of the venue (journal, book, etc.) of a research outcome</td>
            </tr>
            <tr>
              <th class="important">Datasources (content providers)</th>
              <td>
                <p>The different datasources ingested in the OpenAIRE Research Graph.</p>
                <div class="uk-text-bold">Content Provider Types:</div>
                <ul class="portal-circle">
                  <li>Repositories</li>
                  <li>Open Access Publishers & Journals</li>
                  <li>Aggregators</li>
                  <li>Entity Registries</li>
                  <li>Journal Aggregators</li>
                  <li>CRIS (Current Research Information System)</li>
                </ul>
              </td>
            </tr>
            <tr>
              <th>Repositories</th>
              <td>Information systems where scientists upload the bibliographic metadata and payloads of their
                research outcomes (e.g. PDFs of their scientific articles, CSVs of their data, archive with their
                software), due to obligations from their organizations, their funders, or due to community practices
                (e.g. ArXiv, Europe PMC, Zenodo).
              </td>
            </tr>
            <tr>
              <th>Open Access Publishers & Journals</th>
              <td>Information systems of open access publishers or relative journals, which offer bibliographic
                metadata and PDFs of their published articles.
              </td>
            </tr>
            <tr>
              <th>Aggregators</th>
              <td>Information systems that collect descriptive metadata about research products from multiple sources
                in order to enable cross-data source discovery of given research products (e,g, DataCite,
                BASE, DOAJ).
              </td>
            </tr>
            <tr>
              <th>Entity Registries</th>
              <td>Information systems created with the intent of maintaining authoritative registries of given
                entities in the scholarly communication, such as OpenDOAR for the institutional repositories, re3data
                for the data repositories, CORDA and other funder databases for projects and funding information.
              </td>
            </tr>
            <tr>
              <th>CRIS (Current Research Information System)</th>
              <td>Information systems adopted by research and academic organizations to keep track of their research
                administration records and relative results; examples of CRIS content are articles or datasets funded
                by projects, their principal investigators, facilities acquired thanks to funding, etc.
              </td>
            </tr>
          </table>
          <div class="uk-text-small uk-margin-small-top">
            <img src="../../../assets/img/Open_Research_Graph.svg" style="opacity: 0.4">
            <span class="uk-margin-small-left uk-text-baseline uk-text-muted">More information at <a
              href="https://graph.openaire.eu" class="graph-color">OpenAIRE Research Graph</a>.</span>
          </div>
        </li>

        <!--Constructed Attributes tab-->
        <li aria-hidden="true" style="animation-duration: 200ms;">

          <div>
            <p class="uk-text-center">The attributes of entities under this tab are constructed following the methodology described below.</p>
          </div>

          <table>
            <thead>
              <tr>
                <th>Attribute</th>
                <td class="uk-text-bold">Definition</td>
                <td class="uk-text-bold">Construction</td>
              </tr>
            </thead>

            <tbody>
              <tr>
                <th>
                  <p><span class="uk-text-bold uk-text-primary">Peer-review</span></p>
                </th>
                <td>
                  <p>“A process by which something proposed (as for research or publication) is evaluated by a group of
                    experts in the appropriate field.” (Merriam-Webster)</p>
                </td>
                <td>
                  <p>Tentatively, a publication is considered peer-reviewed if it satisfies the following two criteria:</p>
                  <ul>
                    <li>It has a DOI that can be resolved through Crossref.</li>
                    <li>It is not only found as grey literature (pre-prints, reviews, etc).</li>
                  </ul>
                  <p>We are in the process of updating this construction using the actual peer-review status of the venue of publication.</p>
                </td>
              </tr>
              <tr>
                <th>
                  <p><span class="uk-text-bold uk-text-primary">Gold open access</span></p>
                </th>
                <td>
                  <p>A scientific publication with open access provided by the publisher</p>
                </td>
                <td>
                  <p>A publication is Gold OA if it is published in a journal listed in the DOAJ.</p>
                  <p>We are in the process of incorporating the ISSN-Gold-4.0 list of Gold OA journals in our construction
                    (<a href="https://pub.uni-bielefeld.de/record/2944717" target="_blank">https://pub.uni-bielefeld.de/record/2944717</a>).</p>
                </td>
              </tr>
              <tr>
                <th>
                  <p><span class="uk-text-bold uk-text-primary">Green open access</span></p>
                </th>
                <td>
                  <p>An open access scientific publication deposited in a repository.</p>
                </td>
                <td>
                  <p>As in definition.</p>
                </td>
              </tr>
              <tr>
                <th>
                  <p><span class="uk-text-bold uk-text-primary">Validated datasource</span></p>
                </th>
                <td>
                  <p>A datasource of research outcomes that upholds metadata standards</p>
                </td>
                <td>
                  <p>Datasources that have on average a score above 50 in the OpenAIRE Validator service, which checks
                    metadata quality against the most recent OpenAIRE Guidelines.</p>
                  <p>Validator service: <a href="https://www.openaire.eu/validator-registration-guide" target="_blank">https://www.openaire.eu/validator-registration-guide</a></p>
                </td>
              </tr>

            </tbody>

          </table>
          <div class="uk-text-italic uk-margin-small-top">
            <span class="uk-margin-small-left uk-text-baseline uk-text-muted">Stay tuned for updates coming soon!</span>
          </div>
        </li>

      </ul>

    </div>
  </div>

</section>

import { MapCountryData } from '../../domain/map-country-data';;

import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { CountryOverview } from '../../domain/overview-data';

import { DataService } from '../../services/data.service';
import { Router } from '@angular/router';
import {CountryMapData, JoinedMapData, OverviewMapData, SelectedCountry} from '../../domain/overview-map-data';
import { latlong } from '../../domain/countries-lat-lon';
import { DataHandlerService } from '../../services/data-handler.service';

import * as echarts from 'echarts';
import {
  datasetColor,
  datasetTooltipColor, journalsColor, journalsTooltipColor, journalsBackgroundColor, policiesColor, policiesTooltipColor,
  publicationColor,
  publicationTooltipColor,
  repositoriesColor,
  repositoriesTooltipColor,
  gradientStartColor
} from '../../chart-palettes';

declare var require: any;

const mapWorld = require('echarts/map/json/world.json');

@Component({
  selector: 'app-europe-map-overview',
  templateUrl: './europe-map-overview.component.html',
})

export class EuropeMapOverviewComponent implements OnInit {

  private gradientStartColor = gradientStartColor;

  countries: CountryOverview[];

  @Output() emitSelectedCountry: EventEmitter<any> = new EventEmitter();

  activeView: string = 'publications';
  seriesColor: string = publicationColor;
  seriesName: string = 'OA publications';

  // tooltipBackgroundColor: string = '#EC4386';
  tooltipBackgroundColor: string = publicationTooltipColor;
  tooltipBorderColor: string = '#000';

  options = {};

  overviewMapData: OverviewMapData;

  countriesLatLong = latlong;

  joinedPublicationsMap: Map<string, JoinedMapData>;
  joinedDatasetsMap: Map<string, JoinedMapData>;
  joinedRepositoriesMap: Map<string, JoinedMapData>;
  joinedJournalsMap: Map<string, JoinedMapData>;

  constructor(private dataService: DataService,
              private dataHandlerService: DataHandlerService,
              private router: Router) {}

  ngOnInit(): void {

    echarts.registerMap('world', mapWorld);

    if (this.isEmbedRoute()) {
      const body = document.getElementsByTagName('body')[0];
      body.classList.remove('header_sticky');
    }

    this.dataService.getOverviewMapData().subscribe(
      rawData => {
        // console.log('==== Map RawData ====', rawData);
        this.overviewMapData = this.dataHandlerService.convertRawMapDataToMapData(rawData);
        this.joinedPublicationsMap = this.dataHandlerService.createJoinedPublicationsCountryMap(rawData);
        this.joinedDatasetsMap = this.dataHandlerService.createJoinedDatasetsCountryMap(rawData);
        this.joinedRepositoriesMap = this.dataHandlerService.createJoinedRepositoriesCountryMap(rawData);
        this.joinedJournalsMap = this.dataHandlerService.createJoinedJournalsCountryMap(rawData);

        // console.log('Country map data', this.overviewMapData[this.activeView]);
        this.loadMap(this.overviewMapData[this.activeView], this.seriesColor, this.seriesColor);
      },
      error => {
        console.log(error);
      }
    );
  }

  changeView(view: string) {
    this.activeView = view;

    if (this.activeView === 'publications') {
      this.seriesColor = publicationColor;
      this.seriesName = 'OA publications';
      this.tooltipBackgroundColor = publicationTooltipColor;
      this.tooltipBorderColor = '#000';
    } else if (this.activeView === 'datasets') {
      this.seriesColor = datasetColor;
      this.seriesName = 'OA datasets';
      this.tooltipBackgroundColor = datasetTooltipColor;
      this.tooltipBorderColor = '#000';
    } else if (this.activeView === 'repositories') {
      this.seriesColor = repositoriesColor;
      this.seriesName = 'OA repositories';
      this.tooltipBackgroundColor = repositoriesTooltipColor;
      this.tooltipBorderColor = '#000';
    } else if (this.activeView === 'journals') {
      this.seriesColor = journalsColor;
      this.seriesName = 'OA journals';
      this.tooltipBackgroundColor = journalsTooltipColor;
      this.tooltipBorderColor = '#000';
    } else {
      this.seriesColor = policiesColor;
      this.seriesName = 'OA policies';
      this.tooltipBackgroundColor = policiesTooltipColor;
      this.tooltipBorderColor = '#000';
    }
    this.loadMap(this.overviewMapData[view], this.seriesColor, this.seriesName);
  }

  loadMap(countryMapData: CountryMapData[], seriesColor: string, seriesName: string) {

    this.options = {
      title : {
        // text: 'World Population (2011)',
        // subtext: 'From Gapminder',
        left: 'center',
        top: 'top',
        textStyle: {
          color:  '#fff'
        }
      },
      responsive: true,
      tooltip : {
        trigger: 'item',
        // position: 'top',
        // formatter: '<strong>{b}</strong><br> {c2} {a}',
        // backgroundColor: '#f6c4d8',
        // borderColor: '#bf1d5e',
        // borderWidth: 0.2
      },
      visualMap: {
        show: false,
        min: 0,
        // max: max,
        max: 80000,
        inRange: {
          // symbolSize: [6, 60]
          // symbolSize: [0, 6]
        }
      },
      geo: {
        // name:  'World Population (2010)',
        type: 'map',
        map: 'world',
        // center: [15.2551, 54.5260],
        center: [14, 51],
        // zoom: 4.7,
        zoom: 4.3,
        // zoom: 6,
        // roam: true,
        label: {
          emphasis: {
            show: false
          }
        },
        itemStyle: {
          normal: {
            // color: 'rgba(139,151,167,0.4)',
            // borderColor: '#000',
            borderColor: '#fff',
            borderWidth: 0.5,
            areaColor: 'rgba(139,151,167,0.6)',
            // areaStyle: {
            //   color: 'rgba(139,151,167,0.2)'
            // }
          },
          emphasis: {
            // color: 'rgba(139,151,167,0.4)',
            // borderColor: '#000',
            borderColor: '#fff',
            borderWidth: 0.5,
            areaColor: 'rgba(139,151,167,0.8)',
            // areaStyle: {
            //   color: 'rgba(139,151,167,0.2)'
            // }
          }
        }
        // itemStyle: {
        //   // areacolor: '#ffffff',
        //   // bordercolor: '#111',
        //   // borderWidth: 1,
        //   // normal: {
        //   //   // areacolor: '#323c48',
        //   //   areacolor: '#ffffff',
        //   //   bordercolor: '#111'
        //   // },
        //   // emphasis: {
        //   //   areacolor: '#2a333d'
        //   // }
        // }
      },
      series : [
        {
          type: 'scatter',
          coordinateSystem: 'geo',
          name: seriesName,
          label: {
            show: true,
            position: 'inside',
            color: '#fff',
            opacity: 1,
            // formatter: '{@[2]}',
            formatter: (params => {
              return params.data.value[2].toLocaleString();
            }),
            textBorderColor: '#000',
            textBorderWidth: '2',
            fontSize: 15
            // formatter: '{b}: {@score}'
          },
          // activeOpacity: 0.6,
          data: countryMapData.map((itemOpt) => {
            // console.log('itemOpt.code', itemOpt.code);
            return {
              name: itemOpt.name,
              value: [
                this.countriesLatLong.get(itemOpt.code).longitude,
                this.countriesLatLong.get(itemOpt.code).latitude,
                itemOpt.value
              ],
              // label: {
              //   emphasis: {
              //     position: 'right',
              //     show: true
              //   }
              // },
              itemStyle: {
                normal: {
                  color:  seriesColor,
                  // color:  {
                  //   type: 'linear',
                  //   x: 0,
                  //   y: 0,
                  //   x2: 0,
                  //   y2: 1,
                  //   colorStops: [{
                  //     offset: 0, color: gradientStartColor // color at 0% position
                  //   }, {
                  //     offset: 1, color: seriesColor // color at 100% position
                  //   }],
                  //   global: false // false by default
                  // },
                  // color:  '#f8d5e3',
                  // opacity: 0.5,
                  borderColor: seriesColor,
                  borderWidth: 2
                }
              }
            };
          }),
          symbolSize: value => {
            // console.log('Math.sqrt(value[2]) / 1000: ' + (Math.sqrt(value[2]) / 1000) );
            // return Math.sqrt(value[2]) / 10;
            return Math.sqrt(value[2]) / 10 * this.resizeFactor(countryMapData);
            // return Math.sqrt(value[2]) / 10;
          },
          tooltip : {
            trigger: 'item',
            position: 'top',
            formatter: (params => {
              // console.log('params: ', params);
              // let bla = '<strong>' + params.data.name + '</strong><br>';
              // bla += params.data.value[2].toLocaleString() + ' ' + params.seriesName;
              // return bla;
              return this.createTooltip(params);
            }),
            backgroundColor: this.tooltipBackgroundColor,
            borderColor: this.tooltipBorderColor,
            borderWidth: 0.2
          },
        }
      ]
    };
  }

  onChartClick(event: any, type: string) {
    // console.log('chart event:', type, event);
    // console.log('country:', event.name);

    const selectedCountry: SelectedCountry = new SelectedCountry();
    selectedCountry.name = event.name;
    selectedCountry.code = this.joinedPublicationsMap.get(event.name).countryCode;

    // console.log('Selected country', selectedCountry);
    this.emitSelectedCountry.emit(selectedCountry);
  }

  // selectedCountry(countryName: string) {
  //   this.emitSelectedCountry.emit(countryName);
  // }

  isEmbedRoute() {
    return (this.router.url === '/overview-map-embed');
  }

  resizeFactor(countryMapData: CountryMapData[]) {

    let max = 0;
    for (const countryData of countryMapData) {
      if (countryData.value > max) {
        max = countryData.value;
      }
    }
    // console.log('resizeFactor', 100 / (Math.sqrt(max) / 10));
    return 100 / (Math.sqrt(max) / 10);
  }

  createTooltip(params: any) {

    // console.log('params: ', params);
    // console.log('params.name: ', params.name);

    let tooltip = '<div style="padding:10px">';

    tooltip += '<div class="uk-margin-small" style="font-weight: 600;">' + params.name + '</div>';

    if (this.activeView === 'publications') {

      tooltip += '<div class="numbers">';
      tooltip += '<div class="indicator">' +
        '<span class="number">' + params.data.value[2].toLocaleString() + '</span>' +
        '<span><i>OA publications</i> affiliated <br>to an organization in the country</span></div>';

      tooltip += '<div class="indicator uk-margin-small-top">' +
        '<span class="number">' + this.joinedPublicationsMap.get(params.name).deposited.toLocaleString() + '</span>' +
        '<span><i>OA publications</i> in the <br>country\'s institutional repositories</span></div>';

      tooltip += '</div>';

    } else if (this.activeView === 'datasets') {

      tooltip += '<div class="numbers">';
      tooltip += '<div class="indicator">' +
        '<span class="number">' + params.data.value[2].toLocaleString() + '</span>' +
        '<span><i>OA datasets</i> affiliated <br>to an organization in the country</span></div>';

      tooltip += '<div class="indicator uk-margin-small-top">' +
        '<span class="number">' + this.joinedDatasetsMap.get(params.name).deposited.toLocaleString() + '</span>' +
        '<span><i>OA datasets</i> in the <br>country\'s repositories</span></div>';

      tooltip += '</div>';

    } else if (this.activeView === 'repositories') {

      tooltip += '<div class="numbers">';
      tooltip += '<div class="indicator">' +
        '<span class="number">' + params.data.value[2].toLocaleString() + '</span>' +
        '<span><i>repositories</i> in <br>openDOAR & re3data</span></div>';

      // tooltip += '<div class="indicator uk-margin-small-top">' +
      //   '<span class="number">' + this.joinedRepositoriesMap.get(params.name).simple.toLocaleString() + '</span>' +
      //   '<span><i>repositories</i></span></div>';

      tooltip += '</div>';

    } else if (this.activeView === 'journals') {

      tooltip += '<div class="numbers">';
      tooltip += '<div class="indicator">' +
        '<span class="number">' + params.data.value[2].toLocaleString() + '</span>' +
        '<span><i>journals</i> in DOAJ</span></div>';

      // tooltip += '<div class="indicator uk-margin-small-top">' +
      //   '<span class="number">' + this.joinedJournalsMap.get(params.name).simple.toLocaleString() + '</span>' +
      //   '<span><i>journals</i></span></div>';

      tooltip += '</div>';

    } else {

      tooltip += '<div class="numbers">';
      tooltip += '<div class="indicator">' +
        '<span class="number">' + params.data.value[2].toLocaleString() + '</span>' +
        '<span>organizations with <br><i>OA policies</i></span></div>';

      tooltip += '</div>';
    }

    tooltip += '</div>';
    return tooltip;
  }
}

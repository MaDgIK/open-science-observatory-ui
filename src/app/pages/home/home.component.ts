import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { CountryOverviewData, EuropeData, SelectedCountry } from '../../domain/overview-map-data';
import { DataHandlerService } from '../../services/data-handler.service';
import { countries } from '../../domain/countries';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})

export class HomeComponent implements OnInit {

  lastUpdateDate: string;

  europeOverviewData: EuropeData;

  selectedCountry: SelectedCountry = null;
  selectedCountryData: CountryOverviewData = null;

  leadingOpenScienceData: CountryOverviewData[];

  countriesCollection = countries;
  keyword = 'name';

  constructor(private dataService: DataService,
              private dataHandlerService: DataHandlerService,
              private router: Router) { }

  ngOnInit(): void {

    window.scroll(0, 0);

    // const searchIcon = document.getElementById('search-icon');
    // const inputContainer = document.getElementsByClassName('input-container')[0];
    // inputContainer.insertAdjacentElement('afterbegin', searchIcon);

    this.dataService.getLastUpdateDate().subscribe(
      rawData => {
        this.lastUpdateDate = this.dataHandlerService.convertRawDataToLastUpdateDate(rawData);
      }, error => {
        console.log(error);
      }
    );

    this.dataService.getEuropeOverviewData().subscribe(
      rawData => {
        this.europeOverviewData = this.dataHandlerService.convertRawDataToEuropeOverviewData(rawData);
      }, error => {
        console.log(error);
      }
    );

    this.dataService.getLeadingOpenScienceMobileData().subscribe(
      rawData => {
        this.leadingOpenScienceData = this.dataHandlerService.convertRawDataToLeadingOpenScienceData(rawData);
      }, error => {
        console.log(error);
      }
    );
  }

  countrySelected(selectedCountry: SelectedCountry) {
    this.selectedCountry = selectedCountry;
    this.selectedCountryData = null;

    this.dataService.getCountryOverviewData(this.selectedCountry.code).subscribe(
      rawData => {
        this.selectedCountryData = this.dataHandlerService.convertRawDataToCountryOverviewData(rawData);
      }, error => {
        console.log(error);
      }
    );
  }

  deselectCountry() {
    this.selectedCountry = null;
    this.selectedCountryData = null;
  }


  selectCountryFromAutocompleteEvent(item) {
    // do something with selected item
    // console.log('country selected: ', item);
    this.router.navigate([`/country/${item.id}`]);
  }

  onChangeSearch(search: string) {
    // fetch remote data from here
    // And reassign the 'data' which is binded to 'data' property.
  }

  onFocused(e) {
    // do something
  }
}

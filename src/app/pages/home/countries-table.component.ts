import { Component, Input, OnChanges } from '@angular/core';
import { CountryTableData } from '../../domain/overview-map-data';

@Component({
  selector: 'app-countries-table',
  templateUrl: './countries-table.component.html',
})

export class CountriesTableComponent implements OnChanges {

  @Input() isPercentage: boolean;
  @Input() countries: CountryTableData[];

  @Input() view: string;
  @Input() entity: string;

  isSortedBy: string;
  isDescending: boolean = true;

  constructor() {}

  ngOnChanges() {
    this.countries.sort((a, b) => (a['name'] > b['name']) ? 1 : -1);
    console.log('countries ->', this.countries);
  }

  sortBy(field: string) {

    if (field === this.isSortedBy) {
      this.isDescending = !this.isDescending;
    } else {
      this.isDescending = true;
    }

    this.isSortedBy = field;

    if (field === 'pid' || field === 'licence' || field === 'gold' || field === 'green' || field === 'cc_licence' || field === 'abstract'
    || field === 'funders_collab' || field === 'projects_collab' || field === 'authors_collab') {
      if (this.isDescending) {
        if (this.isPercentage) {
          this.countries.sort((a, b) => b[field].percentage - a[field].percentage);
        } else {
          this.countries.sort((a, b) => b[field].oa - a[field].oa);
        }
      } else {
        if (this.isPercentage) {
          this.countries.sort((a, b) => a[field].percentage - b[field].percentage);
        } else {
          this.countries.sort((a, b) => a[field].oa - b[field].oa);
        }
      }
    } else if (field !== 'country') {
      if (this.isDescending) {
        this.countries.sort((a, b) => b[field] - a[field]);
      } else {
        this.countries.sort((a, b) => a[field] - b[field]);
      }
    } else {
      if (this.isDescending) {
        this.countries.sort((a, b) => (a['name'] < b['name']) ? 1 : -1);
      } else {
        this.countries.sort((a, b) => (a['name'] > b['name']) ? 1 : -1);
      }
    }

  }
}

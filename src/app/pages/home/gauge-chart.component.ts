import {Component, Input, OnChanges, OnInit, SimpleChanges, ViewEncapsulation} from '@angular/core';

import * as Highcharts from 'highcharts';
import HighchartsMore from 'highcharts/highcharts-more.src.js';
import solidGauge from 'highcharts/modules/solid-gauge.js';

HighchartsMore(Highcharts);
solidGauge(Highcharts);

@Component({
  selector: 'app-gauge-chart',
  templateUrl: './gauge-chart.component.html',
  // styleUrls: ['./top-menu.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class GaugeChartComponent implements OnInit, OnChanges {

  @Input() chartData: number;
  @Input() color: string;

  public chartValue = 25;

  public update = false;

  Highcharts: typeof Highcharts = Highcharts;

  // gaugeChartOptions: Highcharts.Options;

  public gaugeChartOptions = {
    chart: {
      type: 'solidgauge'
    },

    title: {
      text: ''
    },

    pane: {
      center: ['50%', '85%'],
      size: '140%',
      startAngle: -90,
      endAngle: 90,
      background: {
        backgroundColor: '#fff',
        innerRadius: '60%',
        outerRadius: '100%',
        shape: 'arc'
      }
    },

    tooltip: {
      enabled: true
    },

    credits: {
      enabled: false
    },

    yAxis: {
      stops: [[0.1, '#DF5353'], [0.5, '#DDDF0D'], [0.9, '#55BF3B']],
      min: 0,
      max: 100,
      lineWidth: 0,
      // tickAmount: 6,
      // labels: {
      //   y: 16
      // }
    },

    plotOptions: {
      solidgauge: {
        dataLabels: {
          y: 5,
          borderWidth: 0,
          useHTML: true
        }
      }
    },

    series: [
      {
        name: 'Speed',
        data: [this.chartValue],
        dataLabels: {
          allowOverlap: true,
          format:
          '<div style="text-align:center"><span style="font-size:20px;color:black' +
          '">{y:.0f}%</span><br/>' +
          '<span style="font-size:12px;color:silver"> </span></div>'
        }
      }
    ]
  };

  constructor() {}

  private updateChart(value) {
    this.chartValue = value;

    // first try - working without transition
    // this.gaugeChartOptions.title.text = value;
    this.gaugeChartOptions.series['0'].data[0] = value;
    // this.gaugeChartOptions.series['0'].update();
    this.update = true;
  }

  ngOnInit(): void {
    // console.log('onInit');
    this.updateChart(this.chartData);
  }

  ngOnChanges(changes: SimpleChanges): void {
    // console.log('onChanges');
    // console.log('Chart data: ', this.chartData);
    this.updateChart(this.chartData);
  }

}

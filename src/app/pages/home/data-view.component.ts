import { Component, OnInit } from '@angular/core';
import { printPage } from '../../shared/reusablecomponents/print-function';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { DataService } from '../../services/data.service';
import { DataHandlerService } from '../../services/data-handler.service';
import { CountryTableData, EuropeData } from '../../domain/overview-map-data';
import { environment } from '../../../environments/environment';
import { TreemapHighchartsData } from '../../domain/treemap-highcharts-data';
import { publicationPalette, datasetPalette, softwarePalette, otherResearchProductsPalette, resultColor } from '../../chart-palettes';

@Component({
  selector: 'app-data-view',
  templateUrl: './data-view.component.html',
})

export class DataViewComponent implements OnInit {

  // @Input() type: string;
  // @Input() countries: CountryOverview[];

  private chartsURL = environment.API_ENDPOINT + 'chart?json=';
  private profileName = environment.profileName;

  private publicationPalette = publicationPalette;
  private datasetPalette = datasetPalette;
  private softwarePalette = softwarePalette;
  private otherResearchProductsPalette = otherResearchProductsPalette;
  private resultColor = resultColor;

  activeView: string = 'absolute';
  contentAbsoluteSelection: string = 'affiliated_peer_reviewed';
  contentPercentageSelection: string = 'affiliated_peer_reviewed';

  tableAbsoluteData: CountryTableData[];
  tablePercentageData: CountryTableData[];

  loadingAbsoluteTable: boolean = true;
  loadingPercentageTable: boolean = true;

  europeOverviewData: EuropeData;

  oaNoaPublicationsTimelineURL: SafeResourceUrl;
  oaNoaDatasetsTimelineURL: SafeResourceUrl;
  oaNoaSoftwareTimelineURL: SafeResourceUrl;
  oaNoaOtherTimelineURL: SafeResourceUrl;

  fundersResultsData: TreemapHighchartsData[];

  constructor(private sanitizer: DomSanitizer,
              private dataService: DataService,
              private dataHandlerService: DataHandlerService) {}

  ngOnInit(): void {
    this.getAbsoluteData();
    this.getPercentageData();
    this.getEuropeOverviewData();
    this.getFundersResults();
  }

  getAbsoluteData() {
    this.loadingAbsoluteTable = true;
    this.dataService.getOverviewTableAbsoluteData(this.contentAbsoluteSelection).subscribe(
      rawData => {
        this.tableAbsoluteData = this.dataHandlerService.convertRawDataToAbsoluteTableData(rawData);
        this.loadingAbsoluteTable = false;
      }, error => {
        console.log(error);
        this.loadingAbsoluteTable = false;
      }
    );
  }

  getPercentageData() {
    this.loadingPercentageTable = true;
    this.dataService.getOverviewTablePercentageData(this.contentPercentageSelection).subscribe(
      rawData => {
        this.tablePercentageData = this.dataHandlerService.convertRawDataToPercentageTableData(rawData);
        this.loadingPercentageTable = false;
      }, error => {
        console.log(error);
        this.loadingPercentageTable = false;
      }
    );
  }

  getEuropeOverviewData() {
    this.dataService.getEuropeOAPercentages().subscribe(
      rawData => {
        this.europeOverviewData = this.dataHandlerService.convertRawDataToEuropeOverviewData(rawData);
      }, error => {
        console.log(error);
      }
    );
  }

  getFundersResults() {
    this.dataService.getFundersResults().subscribe(
      rawData => {
        this.fundersResultsData = this.dataHandlerService.convertRawDataToTreemapHighchartsData(rawData);
      }, error => {
        console.log(error);
      }
    );
  }

  getContentAbsolute(contentSelection: string): void {
    this.contentAbsoluteSelection = contentSelection;
    this.getAbsoluteData();
  }

  getContentPercentage(contentSelection: string): void {
    this.contentPercentageSelection = contentSelection;
    this.getPercentageData();
  }

  changeView(view: string) {
    this.activeView = view;
    if (view === 'graph') {

      if (!this.oaNoaPublicationsTimelineURL) {
        this.oaNoaPublicationsTimelineURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"OA","type":"column","query":{"name":"new.oso.publication_timeline.peer_reviewed.results.oa","parameters":[1],"profile":"${this.profileName}"}},{"name":"Non-OA","type":"column","query":{"name":"new.oso.publication_timeline.peer_reviewed.results.oa","parameters":[0],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Publications","align":"left","margin":50},"subtitle":{"text":"over time","align":"left","margin":50},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.publicationPalette.join('","')}\"]}}`));
      }
      if (!this.oaNoaDatasetsTimelineURL) {
        this.oaNoaDatasetsTimelineURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"OA","type":"column","query":{"name":"oso.result_timeline.results.oa","parameters":["dataset"],"profile":"${this.profileName}"}},{"name":"Non-OA","type":"column","query":{"name":"oso.result_timeline.results.non_oa","parameters":["dataset"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Datasets","align":"left","margin":50},"subtitle":{"text":"over time","align":"left","margin":50},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetPalette.join('","')}\"]}}`));
      }
      if (!this.oaNoaSoftwareTimelineURL) {
        this.oaNoaSoftwareTimelineURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"OA","type":"column","query":{"name":"oso.result_timeline.results.oa","parameters":["software"],"profile":"${this.profileName}"}},{"name":"Non-OA","type":"column","query":{"name":"oso.result_timeline.results.non_oa","parameters":["software"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Software","align":"left","margin":50},"subtitle":{"text":"over time","align":"left","margin":50},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.softwarePalette.join('","')}\"]}}`));
      }
      if (!this.oaNoaOtherTimelineURL) {
        this.oaNoaOtherTimelineURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"OA","type":"column","query":{"name":"oso.result_timeline.results.oa","parameters":["other"],"profile":"${this.profileName}"}},{"name":"Non-OA","type":"column","query":{"name":"oso.result_timeline.results.non_oa","parameters":["other"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Other research products","align":"left","margin":50},"subtitle":{"text":"over time","align":"left","margin":50},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherResearchProductsPalette.join('","')}\"]}}`));
      }
    }
  }

  printOverviewData(sectionID: string) {
    printPage(sectionID);
  }
}

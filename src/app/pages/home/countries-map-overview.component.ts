import { MapCountryData } from '../../domain/map-country-data';

declare var require: any;

import { Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { CountryOverview, OverviewData } from '../../domain/overview-data';

import * as Highcharts from 'highcharts';
import MapModule from 'highcharts/modules/map';
import { DataService } from '../../services/data.service';
import { Router } from '@angular/router';

const mapWorld = require('@highcharts/map-collection/custom/world.geo.json');
const mapEurope = require('@highcharts/map-collection/custom/europe.geo.json');

MapModule(Highcharts);

@Component({
  selector: 'app-map-overview',
  templateUrl: './countries-map-overview.component.html',
  // styleUrls: ['./top-menu.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class CountriesMapOverviewComponent implements OnInit {
  // @Input() countries: CountryOverview[];

  countries: CountryOverview[];

  @Output() emitSelectedCountry: EventEmitter<any> = new EventEmitter();

  activeView: string = 'publications';

  Highcharts: typeof Highcharts = Highcharts;

  chartMapOptions: Highcharts.Options;

  mapCountryData: MapCountryData[] = [];
  seriesColor: string = '#ff9800';
  seriesName: string = 'OA publications';

  europe: string[] = [];

  constructor(private dataService: DataService,
              private router: Router) {}

  ngOnInit(): void {

    if (this.isEmbedRoute()) {
      const body = document.getElementsByTagName('body')[0];
      body.classList.remove('header_sticky');
    }

    this.dataService.getOverviewData().subscribe(
      overviewData => {
        this.countries = overviewData.countries;
        this.europe = ['al', 'at', 'ba', 'be', 'by', 'bg', 'hr', 'cz', 'dk', 'ee', 'fi', 'fr', 'de', 'gr', 'hu', 'is', 'ie', 'it',
          'lv', 'lt', 'lu', 'mk', 'mt', 'md', 'me', 'nl', 'no', 'pl',  'pt', 'ro', 'rs', 'si',  'sk', 'es', 'se', 'ch', 'tr', 'ua', 'gb'];
        Highcharts.setOptions({
          lang: {
            // printChart: 'Aaaa',
            thousandsSep: ','
          }
        });
        this.loadMapCountryData();
        this.loadMap(this.mapCountryData, this.seriesColor, this.seriesName, this.europe);
      },
      error => {
        console.log(error);
      }
    );

    // this.Highcharts.chart.get('DE').zoomTo();
    // this.Highcharts.chart.mapZoom(0.2);
    // this.Highcharts.get('DE').zoomTo();
  }

  changeView(view: string) {
    this.activeView = view;
    this.loadMapCountryData();
    this.loadMap(this.mapCountryData, this.seriesColor, this.seriesName, this.europe);
    // this.updateMapData();
  }

  loadMapCountryData() {
    this.mapCountryData = [];
    for (const index in this.countries) {
      this.mapCountryData.push(this.overviewToMapData(this.countries[index]));
    }

    console.log(this.mapCountryData);
  }

  overviewToMapData(countryOverview: CountryOverview): MapCountryData {
    if (this.activeView === 'publications') {
      // this.seriesColor = '#19647E';
      this.seriesColor = '#F17AA9';
      this.seriesName = 'OA publications';
      return {
        id: countryOverview.country,
        country: countryOverview.country,
        z: countryOverview.publications.oa
      };
    } else if (this.activeView === 'datasets') {
      this.seriesColor = '#A98BD4';
      this.seriesName = 'OA datasets';
      return {
        id: countryOverview.country,
        country: countryOverview.country,
        z: countryOverview.datasets.oa
      };
    } else if (this.activeView === 'repositories') {
      this.seriesColor = '#708AA5';
      this.seriesName = 'OA repositories';
      return {
        id: countryOverview.country,
        country: countryOverview.country,
        z: countryOverview.repositories.oa
      };
    } else if (this.activeView === 'journals') {
      this.seriesColor = '#FFCE4E';
      this.seriesName = 'OA journals';
      return {
        id: countryOverview.country,
        country: countryOverview.country,
        z: countryOverview.journals.oa
      };
    } else {
      this.seriesColor = '#639C66';
      this.seriesName = 'OA policies';
      return {
        id: countryOverview.country,
        country: countryOverview.country,
        z: countryOverview.policies.oa
      };
    }
  }

  loadMap(mapCountryData: MapCountryData[], seriesColor: string, seriesName: string, countryCodes: string[]) {
    this.chartMapOptions = {
      chart: {
        map: mapWorld,
        // map: mapEurope,
        events: {
          load: function() {
            this.series[0].data = this.series[0].data.map((el) => {
              if (countryCodes.includes(el['hc-key'])) {
                el.color = 'rgba(139,151,167,0.6)';
                return el;
              }
              return el;
            });
            this.update({
              series: [{
                data: this.series[0].data as Highcharts.SeriesMapbubbleDataOptions,
              } as Highcharts.SeriesMapbubbleOptions]
            });
            // this.mapZoom(0.24, 4518.24, -8188.36);
            this.mapZoom(0.24, this.get('Germany')['x'], this.get('Germany')['y']);
          },
          click: event => {
            if (event.target.hasOwnProperty('point')) {
              console.log(event.target['point']['name']);
              // this.selectedCountry(event.target['point']['name']);
            }
          }
        }
      },
      lang: {
        thousandsSep: ',',
        decimalPoint: '.'
      },
      title: {
        text: ''
      },
      plotOptions: {
        series: {
          cursor: 'pointer',
          events: {
            click: event => {
              console.log(event);
              this.countrySelected(event.point.name);
            }
          }
        }
      },
      // subtitle: {
      //   text: 'Source map: <a href="http://code.highcharts.com/mapdata/custom/world.js">World, Miller projection, medium resolution</a>'
      // },
      // mapNavigation: {
      //   enabled: true,
      //   buttonOptions: {
      //     alignTo: 'spacingBox'
      //   }
      // },
      legend: {
        enabled: false
      },
      // colorAxis: {
      //   min: 0
      // },
      series: [{
        name: 'Countries',
        borderColor: '#fff',
        negativeColor: 'rgba(139,151,167,0.2)',
        enableMouseTracking: false,
        type: 'map'
        }, {
        name: seriesName,
        type: 'mapbubble',
        color: seriesColor,
        marker: {
          fillOpacity: 0.6,
          // states: {
          //   hover: {
          //     fillOpacity: 0.9
          //   }
          // }
        },
        joinBy: ['name', 'country'],
        states: {
          hover: {
            brightness: 0.7,
            borderWidth: 7
            // color: '#a4edba',
            // borderColor: 'gray'
          }
        },
        data : mapCountryData as Highcharts.SeriesMapbubbleDataOptions,
        dataLabels: {
          enabled: true,
          style: {
            color: '#fff',
            fontSize: '13px',
            fontWeight: 'bold',
            // textOutline: '1px #a1a1a1'
            textOutline: '1px #000'
          },
          allowOverlap: true,
          formatter: function() {
            // return this.point.z.toFixed(1) + '%';
            return this.point['z'].toLocaleString();
          }
        },
        // minSize: 4,
        // maxSize: '12%',
        tooltip: {
          headerFormat: '<span style="font-size: 120%; font-weight: bold; margin-bottom: 15px">{point.key}</span><br>',
          // pointFormat: '{point.properties.name}: {point.z:.1f}%'
          pointFormat: '{point.z} {series.name}',
        }
      } as Highcharts.SeriesMapbubbleOptions]
    };
  }

  countrySelected(countryName: string) {
    this.emitSelectedCountry.emit(countryName);
  }

  isEmbedRoute() {
    return (this.router.url === '/overview-map-embed');
  }
}

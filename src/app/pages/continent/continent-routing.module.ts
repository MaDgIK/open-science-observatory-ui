import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { ContinentComponent } from './continent.component';
import { ContinentOverviewComponent } from './overview/continent-overview.compopnent';

const continentRoutes: Routes = [
  {
    path: ':continentName',
    component: ContinentComponent,
    children: [
      {
        path: '',
        redirectTo: 'overview',
        // pathMatch: 'full'
      },
      {
        path: 'overview',
        component: ContinentOverviewComponent
      },
      {
        path: 'open-science',
        loadChildren: () => import('./open-science/continent-open-science.module').then(m => m.ContinentOpenScienceModule),
      },
      {
        path: 'collaboration',
        loadChildren: () => import('./collaboration/continent-collaboration.module').then(m => m.ContinentCollaborationModule),
      }
    ]
  }
];

@NgModule ({
  imports: [RouterModule.forChild(continentRoutes)],
  exports: [RouterModule]
})

export class ContinentRoutingModule {}

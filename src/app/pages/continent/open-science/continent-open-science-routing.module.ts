import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { ContinentOpenScienceComponent } from './continent-open-science.component';

const continentOpenScienceRoutes: Routes = [
  {
    path: '',
    component: ContinentOpenScienceComponent,
    children : [
      {
        path: '',
        redirectTo: 'publications',
        pathMatch: 'full',
      },
      {
        path: 'publications',
        loadChildren: () => import('./publications/continent-os-publications.module').then(m => m.ContinentOpenSciencePublicationsModule),
      },
      {
        path: 'datasets',
        loadChildren: () => import('./datasets/continent-os-datasets.module').then(m => m.ContinentOpenScienceDatasetsModule),
      },
      {
        path: 'software',
        loadChildren: () => import('./software/continent-os-software.module').then(m => m.ContinentOpenScienceSoftwareModule),
      },
      {
        path: 'other-research-products',
        loadChildren: () => import('./other-research-products/continent-os-orp.module').then(m => m.ContinentOpenScienceORPModule),
      },
    ]
  },
];

@NgModule ({
  imports: [RouterModule.forChild(continentOpenScienceRoutes)],
  exports: [RouterModule]
})

export class ContinentOpenScienceRoutingModule {}

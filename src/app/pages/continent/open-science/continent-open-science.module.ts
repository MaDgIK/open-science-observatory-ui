import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContinentOpenScienceRoutingModule } from './continent-open-science-routing.module';
import { ReusableComponentsModule } from '../../../shared/reusablecomponents/reusable-components.module';
import { DataService } from '../../../services/data.service';
import { DataHandlerService } from '../../../services/data-handler.service';
import { ContinentOpenScienceComponent } from './continent-open-science.component';

@NgModule ({
  imports: [
    CommonModule,
    ContinentOpenScienceRoutingModule,
    ReusableComponentsModule,
  ],
  declarations: [
    ContinentOpenScienceComponent,
  ],
  providers: [
    DataService,
    DataHandlerService
  ],
})

export class ContinentOpenScienceModule {}

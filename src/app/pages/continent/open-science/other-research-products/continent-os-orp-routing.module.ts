import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { ContinentOSORPComponent } from './continent-os-orp.component';
import { ContinentOSORPIndicatorsComponent } from './continent-os-orp-indicators.component';

const continentOpenScienceORPRoutes: Routes = [
  {
    path: '',
    component: ContinentOSORPComponent,
    children : [
      {
        path: '',
        redirectTo: 'licence',
        pathMatch: 'full',
      },
      {
        path: ':indicator',
        component: ContinentOSORPIndicatorsComponent,
      }
    ]
  },
];

@NgModule ({
  imports: [RouterModule.forChild(continentOpenScienceORPRoutes)],
  exports: [RouterModule]
})

export class ContinentOpenScienceORPRoutingModule {}

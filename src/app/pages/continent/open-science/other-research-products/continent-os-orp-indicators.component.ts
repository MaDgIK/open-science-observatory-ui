import {Component, OnInit} from '@angular/core';
import {environment} from '../../../../../environments/environment';
import {
  gradientStartColor,
  otherResearchProductsColor, otherResearchProductsPalette, otherIndicatorsPalette
} from '../../../../chart-palettes';
import {DataHandlerService} from '../../../../services/data-handler.service';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';
import {DataService} from '../../../../services/data.service';
import {ActivatedRoute} from '@angular/router';
import {CountryTableData} from '../../../../domain/overview-map-data';
import {TreemapHighchartsData} from '../../../../domain/treemap-highcharts-data';

@Component({
  selector: 'app-continent-os-orp-indicators',
  templateUrl: './continent-os-orp-indicators.component.html',
})

export class ContinentOSORPIndicatorsComponent implements OnInit {

  private chartsURL = environment.API_ENDPOINT + 'chart?json=';
  private profileName = environment.profileName;

  indicator: string;

  private otherResearchProductsPalette = otherResearchProductsPalette;
  private otherIndicatorsPalette = otherIndicatorsPalette;

  otherColor = otherResearchProductsColor;
  gradientStartColor = gradientStartColor;

  otherWithPIDTimeline: SafeResourceUrl;
  otherWithLicenceTimeline: SafeResourceUrl;
  otherWithCCLicenceTimeline: SafeResourceUrl;

  otherWithPIDGroupByView = 'country';
  otherWithPIDByCountryChartURL: SafeResourceUrl;
  otherWithPIDByDatasourceChartURL: SafeResourceUrl;
  otherWithPIDByOrganizationChartURL: SafeResourceUrl;
  otherWithPIDByCountryChartURLMobile: SafeResourceUrl;

  otherWithLicenceGroupByView = 'country';
  otherWithLicenceByCountryChartURL: SafeResourceUrl;
  otherWithLicenceByDatasourceChartURL: SafeResourceUrl;
  otherWithLicenceByOrganizationChartURL: SafeResourceUrl;
  otherWithLicenceByCountryChartURLMobile: SafeResourceUrl;

  otherWithCCLicenceGroupByView = 'country';
  otherWithCCLicenceByCountryChartURL: SafeResourceUrl;
  otherWithCCLicenceByDatasourceChartURL: SafeResourceUrl;
  otherWithCCLicenceByOrganizationChartURL: SafeResourceUrl;
  otherWithCCLicenceByCountryChartURLMobile: SafeResourceUrl;

  otherWithPIDByFunderData: TreemapHighchartsData[];
  otherWithLicenceByFunderData: TreemapHighchartsData[];
  otherWithCCLicenceByFunderData: TreemapHighchartsData[];

  loadingOtherAbsoluteTable: boolean = true;
  loadingOtherPercentageTable: boolean = true;
  otherTableContentSelection: string = 'affiliated';
  otherAbsoluteTableData: CountryTableData[];
  otherPercentageTableData: CountryTableData[];

  constructor(private dataService: DataService,
              private dataHandlerService: DataHandlerService,
              private route: ActivatedRoute,
              private sanitizer: DomSanitizer) { }

  ngOnInit(): void {

    this.route.params.subscribe(params => {

      this.indicator = params['indicator'];
    });

    this.otherWithPIDTimeline = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with PID","type":"column","query":{"name":"new.oso.results.pid_timeline.affiliated","parameters":[1,"other"],"profile":"${this.profileName}"}},{"name":"without PID","type":"column","query":{"name":"new.oso.results.pid_timeline.affiliated","parameters":[0,"other"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access ORPs with PID","align":"left","margin":50},"subtitle":{"text":"over time","align":"left","margin":50},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherIndicatorsPalette.join('","')}\"]}}`));
    this.otherWithLicenceTimeline = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with licence","type":"column","query":{"name":"new.oso.results.licence_timeline.affiliated","parameters":[1,"other"],"profile":"${this.profileName}"}},{"name":"without licence","type":"column","query":{"name":"new.oso.results.licence_timeline.affiliated","parameters":[0,"other"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access ORPs with licence","align":"left","margin":50},"subtitle":{"text":"over time","align":"left","margin":50},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherIndicatorsPalette.join('","')}\"]}}`));
    this.otherWithCCLicenceTimeline = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with CC licence","type":"column","query":{"name":"new.oso.results.cc_licence_timeline.affiliated","parameters":[1,"other"],"profile":"${this.profileName}"}},{"name":"without CC licence","type":"column","query":{"name":"new.oso.results.cc_licence_timeline.affiliated","parameters":[0,"other"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access ORPs with CC licence","align":"left","margin":50},"subtitle":{"text":"over time","align":"left","margin":50},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherIndicatorsPalette.join('","')}\"]}}`));

    this.otherWithPIDByCountryChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with PID","type":"column","query":{"name":"new.oso.results.pid.affiliated.bycountry.all","parameters":[1,"other"],"profile":"${this.profileName}"}},{"name":"without PID","type":"column","query":{"name":"new.oso.results.pid.affiliated.bycountry.all","parameters":[0,"other"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access ORPs with PID","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherIndicatorsPalette.join('","')}\"]}}`));
    this.otherWithPIDByCountryChartURLMobile = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with PID","type":"bar","query":{"name":"new.oso.results.pid.affiliated.bycountry","parameters":[1,"other"],"profile":"${this.profileName}"}},{"name":"without PID","type":"bar","query":{"name":"new.oso.results.pid.affiliated.bycountry","parameters":[0,"other"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access ORPs with PID","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherIndicatorsPalette.join('","')}\"]}}`));

    this.otherWithLicenceByCountryChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with licence","type":"column","query":{"name":"new.oso.results.licence.affiliated.bycountry.all","parameters":[1,"other"],"profile":"${this.profileName}"}},{"name":"without licence","type":"column","query":{"name":"new.oso.results.licence.affiliated.bycountry.all","parameters":[0,"other"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access ORPs with licence","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherIndicatorsPalette.join('","')}\"]}}`));
    this.otherWithLicenceByCountryChartURLMobile = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with licence","type":"bar","query":{"name":"new.oso.results.licence.affiliated.bycountry","parameters":[1,"other"],"profile":"${this.profileName}"}},{"name":"without licence","type":"bar","query":{"name":"new.oso.results.licence.affiliated.bycountry","parameters":[0,"other"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access ORPs with licence","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherIndicatorsPalette.join('","')}\"]}}`));

    this.otherWithCCLicenceByCountryChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with CC licence","type":"column","query":{"name":"new.oso.results.cc_licence.affiliated.bycountry.all","parameters":[1,"other"],"profile":"${this.profileName}"}},{"name":"without CC licence","type":"column","query":{"name":"new.oso.results.cc_licence.affiliated.bycountry.all","parameters":[0,"other"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access ORPs with CC licence","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherIndicatorsPalette.join('","')}\"]}}`));
    this.otherWithCCLicenceByCountryChartURLMobile = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with CC licence","type":"bar","query":{"name":"new.oso.results.cc_licence.affiliated.bycountry","parameters":[1,"other"],"profile":"${this.profileName}"}},{"name":"without CC licence","type":"bar","query":{"name":"new.oso.results.cc_licence.affiliated.bycountry","parameters":[0,"other"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access ORPs with CC licence","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherIndicatorsPalette.join('","')}\"]}}`));


    this.dataService.getFundersResultsByTypeForPID('other').subscribe(
      rawData => {
        this.otherWithPIDByFunderData = this.dataHandlerService.convertRawDataToTreemapHighchartsData(rawData);
      }, error => {
        console.log(error);
      }
    );

    this.dataService.getFundersResultsByTypeForLicence('other').subscribe(
      rawData => {
        this.otherWithLicenceByFunderData = this.dataHandlerService.convertRawDataToTreemapHighchartsData(rawData);
      }, error => {
        console.log(error);
      }
    );

    this.dataService.getFundersResultsByTypeForCCLicence('other').subscribe(
      rawData => {
        this.otherWithCCLicenceByFunderData = this.dataHandlerService.convertRawDataToTreemapHighchartsData(rawData);
      }, error => {
        console.log(error);
      }
    );

    this.getOtherTableData(this.otherTableContentSelection);
  }

  getOtherTableData(contentSelection: string) {
    this.loadingOtherAbsoluteTable = true;
    this.loadingOtherPercentageTable = true;
    this.dataService.getOpenScienceIndicatorsTableData('other', contentSelection).subscribe(
      rawData => {
        this.otherAbsoluteTableData = this.dataHandlerService.convertRawDataToIndicatorsTableData(rawData);
        this.otherPercentageTableData = this.dataHandlerService.convertRawDataToIndicatorsTableData(rawData);
        this.loadingOtherAbsoluteTable = false;
        this.loadingOtherPercentageTable = false;
      }, error => {
        console.log(error);
        this.loadingOtherAbsoluteTable = false;
        this.loadingOtherPercentageTable = false;
      }
    );
  }

  getContent(type: string, contentSelection: string): void {

    this.otherTableContentSelection = contentSelection;
    this.getOtherTableData(this.otherTableContentSelection);
  }

  getOtherWithPIDGroupBy(contentSelection: string): void {
    this.otherWithPIDGroupByView = contentSelection;
    if (contentSelection === 'country' && !this.otherWithPIDByCountryChartURL) {
      this.otherWithPIDByCountryChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with PID","type":"column","query":{"name":"new.oso.results.pid.affiliated.bycountry.all","parameters":[1,"other"],"profile":"${this.profileName}"}},{"name":"without PID","type":"column","query":{"name":"new.oso.results.pid.affiliated.bycountry.all","parameters":[0,"other"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access ORPs with PID","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'datasource' && !this.otherWithPIDByDatasourceChartURL) {
      this.otherWithPIDByDatasourceChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with PID","type":"bar","query":{"name":"new.oso.results.pid.affiliated.bydatasource","parameters":[1,"other"],"profile":"${this.profileName}"}},{"name":"without PID","type":"bar","query":{"name":"new.oso.results.pid.affiliated.bydatasource","parameters":[0,"other"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access ORPs with PID","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'organization' && !this.otherWithPIDByOrganizationChartURL) {
      this.otherWithPIDByOrganizationChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with PID","type":"bar","query":{"name":"new.oso.results.pid.affiliated.byorganization","parameters":[1,"other"],"profile":"${this.profileName}"}},{"name":"without PID","type":"bar","query":{"name":"new.oso.results.pid.affiliated.byorganization","parameters":[0,"other"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access ORPs with PID","align":"left","margin":50},"subtitle":{"text":"by organization (top organizations)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherIndicatorsPalette.join('","')}\"]}}`));
    }
  }

  getOtherWithPIDGroupByMobile(contentSelection: string): void {
    this.otherWithPIDGroupByView = contentSelection;
    if (contentSelection === 'country' && !this.otherWithPIDByCountryChartURLMobile) {
      this.otherWithPIDByCountryChartURLMobile = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with PID","type":"bar","query":{"name":"new.oso.results.pid.affiliated.bycountry","parameters":[1,"other"],"profile":"${this.profileName}"}},{"name":"without PID","type":"bar","query":{"name":"new.oso.results.pid.affiliated.bycountry","parameters":[0,"other"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access ORPs with PID","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'datasource' && !this.otherWithPIDByDatasourceChartURL) {
      this.otherWithPIDByDatasourceChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with PID","type":"bar","query":{"name":"new.oso.results.pid.affiliated.bydatasource","parameters":[1,"other"],"profile":"${this.profileName}"}},{"name":"without PID","type":"bar","query":{"name":"new.oso.results.pid.affiliated.bydatasource","parameters":[0,"other"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access ORPs with PID","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'organization' && !this.otherWithPIDByOrganizationChartURL) {
      this.otherWithPIDByOrganizationChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with PID","type":"bar","query":{"name":"new.oso.results.pid.affiliated.byorganization","parameters":[1,"other"],"profile":"${this.profileName}"}},{"name":"without PID","type":"bar","query":{"name":"new.oso.results.pid.affiliated.byorganization","parameters":[0,"other"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access ORPs with PID","align":"left","margin":50},"subtitle":{"text":"by organization (top organizations)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherIndicatorsPalette.join('","')}\"]}}`));
    }
  }

  getOtherWithLicenceGroupBy(contentSelection: string): void {
    this.otherWithLicenceGroupByView = contentSelection;
    if (contentSelection === 'country' && !this.otherWithLicenceByCountryChartURL) {
      this.otherWithLicenceByCountryChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with licence","type":"column","query":{"name":"new.oso.results.licence.affiliated.bycountry.all","parameters":[1,"other"],"profile":"${this.profileName}"}},{"name":"without licence","type":"column","query":{"name":"new.oso.results.licence.affiliated.bycountry.all","parameters":[0,"other"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access ORPs with licence","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'datasource' && !this.otherWithLicenceByDatasourceChartURL) {
      this.otherWithLicenceByDatasourceChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with licence","type":"bar","query":{"name":"new.oso.results.licence.affiliated.bydatasource","parameters":[1,"other"],"profile":"${this.profileName}"}},{"name":"without licence","type":"bar","query":{"name":"new.oso.results.licence.affiliated.bydatasource","parameters":[0,"other"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access ORPs with licence","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'organization' && !this.otherWithLicenceByOrganizationChartURL) {
      this.otherWithLicenceByOrganizationChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with licence","type":"bar","query":{"name":"new.oso.results.licence.affiliated.byorganization","parameters":[1,"other"],"profile":"${this.profileName}"}},{"name":"without licence","type":"bar","query":{"name":"new.oso.results.licence.affiliated.byorganization","parameters":[0,"other"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access ORPs with licence","align":"left","margin":50},"subtitle":{"text":"by organization (top organizations)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherIndicatorsPalette.join('","')}\"]}}`));
    }
  }

  getOtherWithLicenceGroupByMobile(contentSelection: string): void {
    this.otherWithLicenceGroupByView = contentSelection;
    if (contentSelection === 'country' && !this.otherWithLicenceByCountryChartURLMobile) {
      this.otherWithLicenceByCountryChartURLMobile = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with licence","type":"bar","query":{"name":"new.oso.results.licence.affiliated.bycountry","parameters":[1,"other"],"profile":"${this.profileName}"}},{"name":"without licence","type":"bar","query":{"name":"new.oso.results.licence.affiliated.bycountry","parameters":[0,"other"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access ORPs with licence","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'datasource' && !this.otherWithLicenceByDatasourceChartURL) {
      this.otherWithLicenceByDatasourceChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with licence","type":"bar","query":{"name":"new.oso.results.licence.affiliated.bydatasource","parameters":[1,"other"],"profile":"${this.profileName}"}},{"name":"without licence","type":"bar","query":{"name":"new.oso.results.licence.affiliated.bydatasource","parameters":[0,"other"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access ORPs with licence","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'organization' && !this.otherWithLicenceByOrganizationChartURL) {
      this.otherWithLicenceByOrganizationChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with licence","type":"bar","query":{"name":"new.oso.results.licence.affiliated.byorganization","parameters":[1,"other"],"profile":"${this.profileName}"}},{"name":"without licence","type":"bar","query":{"name":"new.oso.results.licence.affiliated.byorganization","parameters":[0,"other"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access ORPs with licence","align":"left","margin":50},"subtitle":{"text":"by organization (top organizations)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherIndicatorsPalette.join('","')}\"]}}`));
    }
  }

  getOtherWithCCLicenceGroupBy(contentSelection: string): void {
    this.otherWithCCLicenceGroupByView = contentSelection;
    if (contentSelection === 'country' && !this.otherWithCCLicenceByCountryChartURL) {
      this.otherWithCCLicenceByCountryChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with CC licence","type":"column","query":{"name":"new.oso.results.cc_licence.affiliated.bycountry.all","parameters":[1,"other"],"profile":"${this.profileName}"}},{"name":"without CC licence","type":"column","query":{"name":"new.oso.results.cc_licence.affiliated.bycountry.all","parameters":[0,"other"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access ORPs with CC licence","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'datasource' && !this.otherWithCCLicenceByDatasourceChartURL) {
      this.otherWithCCLicenceByDatasourceChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with CC licence","type":"bar","query":{"name":"new.oso.results.cc_licence.affiliated.bydatasource","parameters":[1,"other"],"profile":"${this.profileName}"}},{"name":"without CC licence","type":"bar","query":{"name":"new.oso.results.cc_licence.affiliated.bydatasource","parameters":[0,"other"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access ORPs with CC licence","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'organization' && !this.otherWithCCLicenceByOrganizationChartURL) {
      this.otherWithCCLicenceByOrganizationChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with CC licence","type":"bar","query":{"name":"new.oso.results.cc_licence.affiliated.byorganization","parameters":[1,"other"],"profile":"${this.profileName}"}},{"name":"without CC licence","type":"bar","query":{"name":"new.oso.results.cc_licence.affiliated.byorganization","parameters":[0,"other"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access ORPs with CC licence","align":"left","margin":50},"subtitle":{"text":"by organization (top organizations)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherIndicatorsPalette.join('","')}\"]}}`));
    }
  }

  getOtherWithCCLicenceGroupByMobile(contentSelection: string): void {
    this.otherWithCCLicenceGroupByView = contentSelection;
    if (contentSelection === 'country' && !this.otherWithCCLicenceByCountryChartURLMobile) {
      this.otherWithCCLicenceByCountryChartURLMobile = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with CC licence","type":"bar","query":{"name":"new.oso.results.cc_licence.affiliated.bycountry","parameters":[1,"other"],"profile":"${this.profileName}"}},{"name":"without CC licence","type":"bar","query":{"name":"new.oso.results.cc_licence.affiliated.bycountry","parameters":[0,"other"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access ORPs with CC licence","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'datasource' && !this.otherWithCCLicenceByDatasourceChartURL) {
      this.otherWithCCLicenceByDatasourceChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with CC licence","type":"bar","query":{"name":"new.oso.results.cc_licence.affiliated.bydatasource","parameters":[1,"other"],"profile":"${this.profileName}"}},{"name":"without CC licence","type":"bar","query":{"name":"new.oso.results.cc_licence.affiliated.bydatasource","parameters":[0,"other"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access ORPs with CC licence","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'organization' && !this.otherWithCCLicenceByOrganizationChartURL) {
      this.otherWithCCLicenceByOrganizationChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with CC licence","type":"bar","query":{"name":"new.oso.results.cc_licence.affiliated.byorganization","parameters":[1,"other"],"profile":"${this.profileName}"}},{"name":"without CC licence","type":"bar","query":{"name":"new.oso.results.cc_licence.affiliated.byorganization","parameters":[0,"other"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access ORPs with CC licence","align":"left","margin":50},"subtitle":{"text":"by organization (top organizations)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherIndicatorsPalette.join('","')}\"]}}`));
    }
  }

}

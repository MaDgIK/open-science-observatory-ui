import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContinentOpenScienceORPRoutingModule } from './continent-os-orp-routing.module';
import { ReusableComponentsModule } from '../../../../shared/reusablecomponents/reusable-components.module';
import { ContinentOSORPComponent } from './continent-os-orp.component';
import { ContinentOSORPIndicatorsComponent } from './continent-os-orp-indicators.component';
import { DataService } from '../../../../services/data.service';
import { DataHandlerService } from '../../../../services/data-handler.service';


@NgModule ({
  imports: [
    CommonModule,
    ContinentOpenScienceORPRoutingModule,
    ReusableComponentsModule,
  ],
  declarations: [
    ContinentOSORPComponent,
    ContinentOSORPIndicatorsComponent,
  ],
  providers: [
    DataService,
    DataHandlerService
  ],
})

export class ContinentOpenScienceORPModule {}

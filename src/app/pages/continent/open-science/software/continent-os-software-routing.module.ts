import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { ContinentOSSoftwareComponent } from './continent-os-software.component';
import { ContinentOSSoftwareIndicatorsComponent } from './continent-os-software-indicators.component';

const continentOpenScienceSoftwareRoutes: Routes = [
  {
    path: '',
    component: ContinentOSSoftwareComponent,
    children : [
      {
        path: '',
        redirectTo: 'licence',
        pathMatch: 'full',
      },
      {
        path: ':indicator',
        component: ContinentOSSoftwareIndicatorsComponent,
      }
    ]
  },
];

@NgModule ({
  imports: [RouterModule.forChild(continentOpenScienceSoftwareRoutes)],
  exports: [RouterModule]
})

export class ContinentOpenScienceSoftwareRoutingModule {}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContinentOpenScienceSoftwareRoutingModule } from './continent-os-software-routing.module';
import { ReusableComponentsModule } from '../../../../shared/reusablecomponents/reusable-components.module';
import { ContinentOSSoftwareComponent } from './continent-os-software.component';
import { ContinentOSSoftwareIndicatorsComponent } from './continent-os-software-indicators.component';
import { DataService } from '../../../../services/data.service';
import { DataHandlerService } from '../../../../services/data-handler.service';


@NgModule ({
  imports: [
    CommonModule,
    ContinentOpenScienceSoftwareRoutingModule,
    ReusableComponentsModule,
  ],
  declarations: [
    ContinentOSSoftwareComponent,
    ContinentOSSoftwareIndicatorsComponent,
  ],
  providers: [
    DataService,
    DataHandlerService
  ],
})

export class ContinentOpenScienceSoftwareModule {}

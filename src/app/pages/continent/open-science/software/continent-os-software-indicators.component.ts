import {Component, OnInit} from '@angular/core';
import {
  gradientStartColor, softwareColor, softwarePalette, softwareIndicatorsPalette
} from '../../../../chart-palettes';
import {environment} from '../../../../../environments/environment';
import {CountryTableData} from '../../../../domain/overview-map-data';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';
import {TreemapHighchartsData} from '../../../../domain/treemap-highcharts-data';
import {DataHandlerService} from '../../../../services/data-handler.service';
import {DataService} from '../../../../services/data.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-continent-os-software-indicators',
  templateUrl: './continent-os-software-indicators.component.html',
})

export class ContinentOSSoftwareIndicatorsComponent implements OnInit {

  private chartsURL = environment.API_ENDPOINT + 'chart?json=';
  private profileName = environment.profileName;

  indicator: string;

  private softwarePalette = softwarePalette;
  private softwareIndicatorsPalette = softwareIndicatorsPalette;

  softwareColor = softwareColor;
  gradientStartColor = gradientStartColor;

  softwareWithPIDTimeline: SafeResourceUrl;
  softwareWithLicenceTimeline: SafeResourceUrl;
  softwareWithCCLicenceTimeline: SafeResourceUrl;

  softwareWithPIDGroupByView = 'country';
  softwareWithPIDByCountryChartURL: SafeResourceUrl;
  softwareWithPIDByDatasourceChartURL: SafeResourceUrl;
  softwareWithPIDByOrganizationChartURL: SafeResourceUrl;
  softwareWithPIDByCountryChartURLMobile: SafeResourceUrl;

  softwareWithLicenceGroupByView = 'country';
  softwareWithLicenceByCountryChartURL: SafeResourceUrl;
  softwareWithLicenceByDatasourceChartURL: SafeResourceUrl;
  softwareWithLicenceByOrganizationChartURL: SafeResourceUrl;
  softwareWithLicenceByCountryChartURLMobile: SafeResourceUrl;

  softwareWithCCLicenceGroupByView = 'country';
  softwareWithCCLicenceByCountryChartURL: SafeResourceUrl;
  softwareWithCCLicenceByDatasourceChartURL: SafeResourceUrl;
  softwareWithCCLicenceByOrganizationChartURL: SafeResourceUrl;
  softwareWithCCLicenceByCountryChartURLMobile: SafeResourceUrl;

  softwareWithPIDByFunderData: TreemapHighchartsData[];
  softwareWithLicenceByFunderData: TreemapHighchartsData[];
  softwareWithCCLicenceByFunderData: TreemapHighchartsData[];

  loadingSoftwareAbsoluteTable: boolean = true;
  loadingSoftwarePercentageTable: boolean = true;
  softwareTableContentSelection: string = 'affiliated';
  softwareAbsoluteTableData: CountryTableData[];
  softwarePercentageTableData: CountryTableData[];

  constructor(private dataService: DataService,
              private dataHandlerService: DataHandlerService,
              private route: ActivatedRoute,
              private sanitizer: DomSanitizer) { }

  ngOnInit(): void {

    this.route.params.subscribe(params => {

      this.indicator = params['indicator'];
    });

    this.softwareWithPIDTimeline = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with PID","type":"column","query":{"name":"new.oso.results.pid_timeline.affiliated","parameters":[1,"software"],"profile":"${this.profileName}"}},{"name":"without PID","type":"column","query":{"name":"new.oso.results.pid_timeline.affiliated","parameters":[0,"software"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Source software with PID","align":"left","margin":50},"subtitle":{"text":"over time","align":"left","margin":50},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.softwareIndicatorsPalette.join('","')}\"]}}`));
    this.softwareWithLicenceTimeline = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with licence","type":"column","query":{"name":"new.oso.results.licence_timeline.affiliated","parameters":[1,"software"],"profile":"${this.profileName}"}},{"name":"without licence","type":"column","query":{"name":"new.oso.results.licence_timeline.affiliated","parameters":[0,"software"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Source software with licence","align":"left","margin":50},"subtitle":{"text":"over time","align":"left","margin":50},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.softwareIndicatorsPalette.join('","')}\"]}}`));
    this.softwareWithCCLicenceTimeline = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with CC licence","type":"column","query":{"name":"new.oso.results.cc_licence_timeline.affiliated","parameters":[1,"software"],"profile":"${this.profileName}"}},{"name":"without CC licence","type":"column","query":{"name":"new.oso.results.cc_licence_timeline.affiliated","parameters":[0,"software"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Source software with CC licence","align":"left","margin":50},"subtitle":{"text":"over time","align":"left","margin":50},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.softwareIndicatorsPalette.join('","')}\"]}}`));

    this.softwareWithPIDByCountryChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with PID","type":"column","query":{"name":"new.oso.results.pid.affiliated.bycountry.all","parameters":[1,"software"],"profile":"${this.profileName}"}},{"name":"without PID","type":"column","query":{"name":"new.oso.results.pid.affiliated.bycountry.all","parameters":[0,"software"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Source software with PID","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.softwareIndicatorsPalette.join('","')}\"]}}`));
    this.softwareWithPIDByCountryChartURLMobile = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with PID","type":"bar","query":{"name":"new.oso.results.pid.affiliated.bycountry","parameters":[1,"software"],"profile":"${this.profileName}"}},{"name":"without PID","type":"bar","query":{"name":"new.oso.results.pid.affiliated.bycountry","parameters":[0,"software"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Source software with PID","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.softwareIndicatorsPalette.join('","')}\"]}}`));

    this.softwareWithLicenceByCountryChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with licence","type":"column","query":{"name":"new.oso.results.licence.affiliated.bycountry.all","parameters":[1,"software"],"profile":"${this.profileName}"}},{"name":"without licence","type":"column","query":{"name":"new.oso.results.licence.affiliated.bycountry.all","parameters":[0,"software"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Source software with licence","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.softwareIndicatorsPalette.join('","')}\"]}}`));
    this.softwareWithLicenceByCountryChartURLMobile = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with licence","type":"bar","query":{"name":"new.oso.results.licence.affiliated.bycountry","parameters":[1,"software"],"profile":"${this.profileName}"}},{"name":"without licence","type":"bar","query":{"name":"new.oso.results.licence.affiliated.bycountry","parameters":[0,"software"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Source software with licence","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.softwareIndicatorsPalette.join('","')}\"]}}`));

    this.softwareWithCCLicenceByCountryChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with CC licence","type":"column","query":{"name":"new.oso.results.cc_licence.affiliated.bycountry.all","parameters":[1,"software"],"profile":"${this.profileName}"}},{"name":"without CC licence","type":"column","query":{"name":"new.oso.results.cc_licence.affiliated.bycountry.all","parameters":[0,"software"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Source software with CC licence","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.softwareIndicatorsPalette.join('","')}\"]}}`));
    this.softwareWithCCLicenceByCountryChartURLMobile = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with CC licence","type":"bar","query":{"name":"new.oso.results.cc_licence.affiliated.bycountry","parameters":[1,"software"],"profile":"${this.profileName}"}},{"name":"without CC licence","type":"bar","query":{"name":"new.oso.results.cc_licence.affiliated.bycountry","parameters":[0,"software"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Source software with CC licence","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.softwareIndicatorsPalette.join('","')}\"]}}`));


    this.dataService.getFundersResultsByTypeForPID('software').subscribe(
      rawData => {
        this.softwareWithPIDByFunderData = this.dataHandlerService.convertRawDataToTreemapHighchartsData(rawData);
      }, error => {
        console.log(error);
      }
    );

    this.dataService.getFundersResultsByTypeForLicence('software').subscribe(
      rawData => {
        this.softwareWithLicenceByFunderData = this.dataHandlerService.convertRawDataToTreemapHighchartsData(rawData);
      }, error => {
        console.log(error);
      }
    );

    this.dataService.getFundersResultsByTypeForCCLicence('software').subscribe(
      rawData => {
        this.softwareWithCCLicenceByFunderData = this.dataHandlerService.convertRawDataToTreemapHighchartsData(rawData);
      }, error => {
        console.log(error);
      }
    );

    this.getSoftwareTableData(this.softwareTableContentSelection);
  }

  getSoftwareTableData(contentSelection: string) {
    this.loadingSoftwareAbsoluteTable = true;
    this.loadingSoftwarePercentageTable = true;
    this.dataService.getOpenScienceIndicatorsTableData('software', contentSelection).subscribe(
      rawData => {
        this.softwareAbsoluteTableData = this.dataHandlerService.convertRawDataToIndicatorsTableData(rawData);
        this.softwarePercentageTableData = this.dataHandlerService.convertRawDataToIndicatorsTableData(rawData);
        this.loadingSoftwareAbsoluteTable = false;
        this.loadingSoftwarePercentageTable = false;
      }, error => {
        console.log(error);
        this.loadingSoftwareAbsoluteTable = false;
        this.loadingSoftwarePercentageTable = false;
      }
    );
  }

  getContent(type: string, contentSelection: string): void {

    this.softwareTableContentSelection = contentSelection;
    this.getSoftwareTableData(this.softwareTableContentSelection);
  }

  getSoftwareWithPIDGroupBy(contentSelection: string): void {
    this.softwareWithPIDGroupByView = contentSelection;
    if (contentSelection === 'country' && !this.softwareWithPIDByCountryChartURL) {
      this.softwareWithPIDByCountryChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with PID","type":"column","query":{"name":"new.oso.results.pid.affiliated.bycountry.all","parameters":[1,"software"],"profile":"${this.profileName}"}},{"name":"without PID","type":"column","query":{"name":"new.oso.results.pid.affiliated.bycountry.all","parameters":[0,"software"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Source software with PID","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.softwareIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'datasource' && !this.softwareWithPIDByDatasourceChartURL) {
      this.softwareWithPIDByDatasourceChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with PID","type":"bar","query":{"name":"new.oso.results.pid.affiliated.bydatasource","parameters":[1,"software"],"profile":"${this.profileName}"}},{"name":"without PID","type":"bar","query":{"name":"new.oso.results.pid.affiliated.bydatasource","parameters":[0,"software"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Source software with PID","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.softwareIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'organization' && !this.softwareWithPIDByOrganizationChartURL) {
      this.softwareWithPIDByOrganizationChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with PID","type":"bar","query":{"name":"new.oso.results.pid.affiliated.byorganization","parameters":[1,"software"],"profile":"${this.profileName}"}},{"name":"without PID","type":"bar","query":{"name":"new.oso.results.pid.affiliated.byorganization","parameters":[0,"software"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Source software with PID","align":"left","margin":50},"subtitle":{"text":"by organization (top organizations)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.softwareIndicatorsPalette.join('","')}\"]}}`));
    }
  }

  getSoftwareWithPIDGroupByMobile(contentSelection: string): void {
    this.softwareWithPIDGroupByView = contentSelection;
    if (contentSelection === 'country' && !this.softwareWithPIDByCountryChartURLMobile) {
      this.softwareWithPIDByCountryChartURLMobile = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with PID","type":"bar","query":{"name":"new.oso.results.pid.affiliated.bycountry","parameters":[1,"software"],"profile":"${this.profileName}"}},{"name":"without PID","type":"bar","query":{"name":"new.oso.results.pid.affiliated.bycountry","parameters":[0,"software"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Source software with PID","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.softwareIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'datasource' && !this.softwareWithPIDByDatasourceChartURL) {
      this.softwareWithPIDByDatasourceChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with PID","type":"bar","query":{"name":"new.oso.results.pid.affiliated.bydatasource","parameters":[1,"software"],"profile":"${this.profileName}"}},{"name":"without PID","type":"bar","query":{"name":"new.oso.results.pid.affiliated.bydatasource","parameters":[0,"software"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Source software with PID","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.softwareIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'organization' && !this.softwareWithPIDByOrganizationChartURL) {
      this.softwareWithPIDByOrganizationChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with PID","type":"bar","query":{"name":"new.oso.results.pid.affiliated.byorganization","parameters":[1,"software"],"profile":"${this.profileName}"}},{"name":"without PID","type":"bar","query":{"name":"new.oso.results.pid.affiliated.byorganization","parameters":[0,"software"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Source software with PID","align":"left","margin":50},"subtitle":{"text":"by organization (top organizations)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.softwareIndicatorsPalette.join('","')}\"]}}`));
    }
  }

  getSoftwareWithLicenceGroupBy(contentSelection: string): void {
    this.softwareWithLicenceGroupByView = contentSelection;
    if (contentSelection === 'country' && !this.softwareWithLicenceByCountryChartURL) {
      this.softwareWithLicenceByCountryChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with licence","type":"column","query":{"name":"new.oso.results.licence.affiliated.bycountry.all","parameters":[1,"software"],"profile":"${this.profileName}"}},{"name":"without licence","type":"column","query":{"name":"new.oso.results.licence.affiliated.bycountry.all","parameters":[0,"software"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Source software with licence","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.softwareIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'datasource' && !this.softwareWithLicenceByDatasourceChartURL) {
      this.softwareWithLicenceByDatasourceChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with licence","type":"bar","query":{"name":"new.oso.results.licence.affiliated.bydatasource","parameters":[1,"software"],"profile":"${this.profileName}"}},{"name":"without licence","type":"bar","query":{"name":"new.oso.results.licence.affiliated.bydatasource","parameters":[0,"software"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Source software with licence","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.softwareIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'organization' && !this.softwareWithLicenceByOrganizationChartURL) {
      this.softwareWithLicenceByOrganizationChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with licence","type":"bar","query":{"name":"new.oso.results.licence.affiliated.byorganization","parameters":[1,"software"],"profile":"${this.profileName}"}},{"name":"without licence","type":"bar","query":{"name":"new.oso.results.licence.affiliated.byorganization","parameters":[0,"software"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Source software with licence","align":"left","margin":50},"subtitle":{"text":"by organization (top organizations)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.softwareIndicatorsPalette.join('","')}\"]}}`));
    }
  }

  getSoftwareWithLicenceGroupByMobile(contentSelection: string): void {
    this.softwareWithLicenceGroupByView = contentSelection;
    if (contentSelection === 'country' && !this.softwareWithLicenceByCountryChartURLMobile) {
      this.softwareWithLicenceByCountryChartURLMobile = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with licence","type":"bar","query":{"name":"new.oso.results.licence.affiliated.bycountry","parameters":[1,"software"],"profile":"${this.profileName}"}},{"name":"without licence","type":"bar","query":{"name":"new.oso.results.licence.affiliated.bycountry","parameters":[0,"software"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Source software with licence","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.softwareIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'datasource' && !this.softwareWithLicenceByDatasourceChartURL) {
      this.softwareWithLicenceByDatasourceChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with licence","type":"bar","query":{"name":"new.oso.results.licence.affiliated.bydatasource","parameters":[1,"software"],"profile":"${this.profileName}"}},{"name":"without licence","type":"bar","query":{"name":"new.oso.results.licence.affiliated.bydatasource","parameters":[0,"software"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Source software with licence","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.softwareIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'organization' && !this.softwareWithLicenceByOrganizationChartURL) {
      this.softwareWithLicenceByOrganizationChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with licence","type":"bar","query":{"name":"new.oso.results.licence.affiliated.byorganization","parameters":[1,"software"],"profile":"${this.profileName}"}},{"name":"without licence","type":"bar","query":{"name":"new.oso.results.licence.affiliated.byorganization","parameters":[0,"software"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Source software with licence","align":"left","margin":50},"subtitle":{"text":"by organization (top organizations)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.softwareIndicatorsPalette.join('","')}\"]}}`));
    }
  }

  getSoftwareWithCCLicenceGroupBy(contentSelection: string): void {
    this.softwareWithCCLicenceGroupByView = contentSelection;
    if (contentSelection === 'country' && !this.softwareWithCCLicenceByCountryChartURL) {
      this.softwareWithCCLicenceByCountryChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with CC licence","type":"column","query":{"name":"new.oso.results.cc_licence.affiliated.bycountry.all","parameters":[1,"software"],"profile":"${this.profileName}"}},{"name":"without CC licence","type":"column","query":{"name":"new.oso.results.cc_licence.affiliated.bycountry.all","parameters":[0,"software"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Source software with CC licence","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.softwareIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'datasource' && !this.softwareWithCCLicenceByDatasourceChartURL) {
      this.softwareWithCCLicenceByDatasourceChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with CC licence","type":"bar","query":{"name":"new.oso.results.cc_licence.affiliated.bydatasource","parameters":[1,"software"],"profile":"${this.profileName}"}},{"name":"without CC licence","type":"bar","query":{"name":"new.oso.results.cc_licence.affiliated.bydatasource","parameters":[0,"software"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Source software with CC licence","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.softwareIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'organization' && !this.softwareWithCCLicenceByOrganizationChartURL) {
      this.softwareWithCCLicenceByOrganizationChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with CC licence","type":"bar","query":{"name":"new.oso.results.cc_licence.affiliated.byorganization","parameters":[1,"software"],"profile":"${this.profileName}"}},{"name":"without CC licence","type":"bar","query":{"name":"new.oso.results.cc_licence.affiliated.byorganization","parameters":[0,"software"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Source software with CC licence","align":"left","margin":50},"subtitle":{"text":"by organization (top organizations)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.softwareIndicatorsPalette.join('","')}\"]}}`));
    }
  }

  getSoftwareWithCCLicenceGroupByMobile(contentSelection: string): void {
    this.softwareWithCCLicenceGroupByView = contentSelection;
    if (contentSelection === 'country' && !this.softwareWithCCLicenceByCountryChartURLMobile) {
      this.softwareWithCCLicenceByCountryChartURLMobile = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with CC licence","type":"bar","query":{"name":"new.oso.results.cc_licence.affiliated.bycountry","parameters":[1,"software"],"profile":"${this.profileName}"}},{"name":"without CC licence","type":"bar","query":{"name":"new.oso.results.cc_licence.affiliated.bycountry","parameters":[0,"software"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Source software with CC licence","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.softwareIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'datasource' && !this.softwareWithCCLicenceByDatasourceChartURL) {
      this.softwareWithCCLicenceByDatasourceChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with CC licence","type":"bar","query":{"name":"new.oso.results.cc_licence.affiliated.bydatasource","parameters":[1,"software"],"profile":"${this.profileName}"}},{"name":"without CC licence","type":"bar","query":{"name":"new.oso.results.cc_licence.affiliated.bydatasource","parameters":[0,"software"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Source software with CC licence","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.softwareIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'organization' && !this.softwareWithCCLicenceByOrganizationChartURL) {
      this.softwareWithCCLicenceByOrganizationChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with CC licence","type":"bar","query":{"name":"new.oso.results.cc_licence.affiliated.byorganization","parameters":[1,"software"],"profile":"${this.profileName}"}},{"name":"without CC licence","type":"bar","query":{"name":"new.oso.results.cc_licence.affiliated.byorganization","parameters":[0,"software"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Source software with CC licence","align":"left","margin":50},"subtitle":{"text":"by organization (top organizations)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.softwareIndicatorsPalette.join('","')}\"]}}`));
    }
  }

}

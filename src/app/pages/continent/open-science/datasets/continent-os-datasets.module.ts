import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContinentOpenScienceDatasetsRoutingModule } from './continent-os-datasets-routing.module';
import { ReusableComponentsModule } from '../../../../shared/reusablecomponents/reusable-components.module';
import { ContinentOSDatasetsComponent } from './continent-os-datasets.component';
import { ContinentOSDatasetsIndicatorsComponent } from './continent-os-datasets-indicators.component';
import { DataService } from '../../../../services/data.service';
import { DataHandlerService } from '../../../../services/data-handler.service';


@NgModule ({
  imports: [
    CommonModule,
    ContinentOpenScienceDatasetsRoutingModule,
    ReusableComponentsModule,
  ],
  declarations: [
    ContinentOSDatasetsComponent,
    ContinentOSDatasetsIndicatorsComponent,
  ],
  providers: [
    DataService,
    DataHandlerService
  ],
})

export class ContinentOpenScienceDatasetsModule {}

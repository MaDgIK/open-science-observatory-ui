import { Component, OnInit } from '@angular/core';
import {
  datasetColor,
  datasetPalette,
  datasetIndicatorsPalette,
  gradientStartColor,
} from '../../../../chart-palettes';
import {environment} from '../../../../../environments/environment';
import {CountryTableData} from '../../../../domain/overview-map-data';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';
import {TreemapHighchartsData} from '../../../../domain/treemap-highcharts-data';
import {DataHandlerService} from '../../../../services/data-handler.service';
import {DataService} from '../../../../services/data.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-continent-os-datasets-indicators',
  templateUrl: './continent-os-datasets-indicators.component.html',
})

export class ContinentOSDatasetsIndicatorsComponent implements OnInit {

  private chartsURL = environment.API_ENDPOINT + 'chart?json=';
  private profileName = environment.profileName;

  indicator: string;

  private datasetPalette = datasetPalette;
  private datasetIndicatorsPalette = datasetIndicatorsPalette;

  datasetColor = datasetColor;
  gradientStartColor = gradientStartColor;


  datasetsWithPIDTimeline: SafeResourceUrl;
  datasetsWithLicenceTimeline: SafeResourceUrl;
  datasetsWithCCLicenceTimeline: SafeResourceUrl;

  datasetsWithPIDGroupByView = 'country';
  datasetsWithPIDByCountryChartURL: SafeResourceUrl;
  datasetsWithPIDByDatasourceChartURL: SafeResourceUrl;
  datasetsWithPIDByOrganizationChartURL: SafeResourceUrl;
  datasetsWithPIDByCountryChartURLMobile: SafeResourceUrl;

  datasetsWithLicenceGroupByView = 'country';
  datasetsWithLicenceByCountryChartURL: SafeResourceUrl;
  datasetsWithLicenceByDatasourceChartURL: SafeResourceUrl;
  datasetsWithLicenceByOrganizationChartURL: SafeResourceUrl;
  datasetsWithLicenceByCountryChartURLMobile: SafeResourceUrl;

  datasetsWithCCLicenceGroupByView = 'country';
  datasetsWithCCLicenceByCountryChartURL: SafeResourceUrl;
  datasetsWithCCLicenceByDatasourceChartURL: SafeResourceUrl;
  datasetsWithCCLicenceByOrganizationChartURL: SafeResourceUrl;
  datasetsWithCCLicenceByCountryChartURLMobile: SafeResourceUrl;

  datasetsWithPIDByFunderData: TreemapHighchartsData[];
  datasetsWithLicenceByFunderData: TreemapHighchartsData[];
  datasetsWithCCLicenceByFunderData: TreemapHighchartsData[];

  loadingDatasetsAbsoluteTable: boolean = true;
  loadingDatasetsPercentageTable: boolean = true;
  datasetsTableContentSelection: string = 'affiliated';
  datasetsAbsoluteTableData: CountryTableData[];
  datasetsPercentageTableData: CountryTableData[];

  constructor(private dataService: DataService,
              private dataHandlerService: DataHandlerService,
              private route: ActivatedRoute,
              private sanitizer: DomSanitizer) { }

  ngOnInit(): void {

    this.route.params.subscribe(params => {

      this.indicator = params['indicator'];
    });

    this.datasetsWithPIDTimeline = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with PID","type":"column","query":{"name":"new.oso.results.pid_timeline.affiliated","parameters":[1,"dataset"],"profile":"${this.profileName}"}},{"name":"without PID","type":"column","query":{"name":"new.oso.results.pid_timeline.affiliated","parameters":[0,"dataset"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets with PID","align":"left","margin":50},"subtitle":{"text":"over time","align":"left","margin":50},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetIndicatorsPalette.join('","')}\"]}}`));
    this.datasetsWithLicenceTimeline = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with licence","type":"column","query":{"name":"new.oso.results.licence_timeline.affiliated","parameters":[1,"dataset"],"profile":"${this.profileName}"}},{"name":"without licence","type":"column","query":{"name":"new.oso.results.licence_timeline.affiliated","parameters":[0,"dataset"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets with licence","align":"left","margin":50},"subtitle":{"text":"over time","align":"left","margin":50},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetIndicatorsPalette.join('","')}\"]}}`));
    this.datasetsWithCCLicenceTimeline = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with CC licence","type":"column","query":{"name":"new.oso.results.cc_licence_timeline.affiliated","parameters":[1,"dataset"],"profile":"${this.profileName}"}},{"name":"without CC licence","type":"column","query":{"name":"new.oso.results.cc_licence_timeline.affiliated","parameters":[0,"dataset"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets with CC licence","align":"left","margin":50},"subtitle":{"text":"over time","align":"left","margin":50},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetIndicatorsPalette.join('","')}\"]}}`));

    this.datasetsWithPIDByCountryChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with PID","type":"column","query":{"name":"new.oso.results.pid.affiliated.bycountry.all","parameters":[1,"dataset"],"profile":"${this.profileName}"}},{"name":"without PID","type":"column","query":{"name":"new.oso.results.pid.affiliated.bycountry.all","parameters":[0,"dataset"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets with PID","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetIndicatorsPalette.join('","')}\"]}}`));
    this.datasetsWithPIDByCountryChartURLMobile = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with PID","type":"bar","query":{"name":"new.oso.results.pid.affiliated.bycountry","parameters":[1,"dataset"],"profile":"${this.profileName}"}},{"name":"without PID","type":"bar","query":{"name":"new.oso.results.pid.affiliated.bycountry","parameters":[0,"dataset"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets with PID","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetIndicatorsPalette.join('","')}\"]}}`));

    this.datasetsWithLicenceByCountryChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with licence","type":"column","query":{"name":"new.oso.results.licence.affiliated.bycountry.all","parameters":[1,"dataset"],"profile":"${this.profileName}"}},{"name":"without licence","type":"column","query":{"name":"new.oso.results.licence.affiliated.bycountry.all","parameters":[0,"dataset"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets with licence","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetIndicatorsPalette.join('","')}\"]}}`));
    this.datasetsWithLicenceByCountryChartURLMobile = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with licence","type":"bar","query":{"name":"new.oso.results.licence.affiliated.bycountry","parameters":[1,"dataset"],"profile":"${this.profileName}"}},{"name":"without licence","type":"bar","query":{"name":"new.oso.results.licence.affiliated.bycountry","parameters":[0,"dataset"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets with licence","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetIndicatorsPalette.join('","')}\"]}}`));

    this.datasetsWithCCLicenceByCountryChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with CC licence","type":"column","query":{"name":"new.oso.results.cc_licence.affiliated.bycountry.all","parameters":[1,"dataset"],"profile":"${this.profileName}"}},{"name":"without CC licence","type":"column","query":{"name":"new.oso.results.cc_licence.affiliated.bycountry.all","parameters":[0,"dataset"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets with CC licence","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetIndicatorsPalette.join('","')}\"]}}`));
    this.datasetsWithCCLicenceByCountryChartURLMobile = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with CC licence","type":"bar","query":{"name":"new.oso.results.cc_licence.affiliated.bycountry","parameters":[1,"dataset"],"profile":"${this.profileName}"}},{"name":"without CC licence","type":"bar","query":{"name":"new.oso.results.cc_licence.affiliated.bycountry","parameters":[0,"dataset"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets with CC licence","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetIndicatorsPalette.join('","')}\"]}}`));

    this.dataService.getFundersResultsByTypeForPID('dataset').subscribe(
      rawData => {
        this.datasetsWithPIDByFunderData = this.dataHandlerService.convertRawDataToTreemapHighchartsData(rawData);
      }, error => {
        console.log(error);
      }
    );

    this.dataService.getFundersResultsByTypeForLicence('dataset').subscribe(
      rawData => {
        this.datasetsWithLicenceByFunderData = this.dataHandlerService.convertRawDataToTreemapHighchartsData(rawData);
      }, error => {
        console.log(error);
      }
    );

    this.dataService.getFundersResultsByTypeForCCLicence('dataset').subscribe(
      rawData => {
        this.datasetsWithCCLicenceByFunderData = this.dataHandlerService.convertRawDataToTreemapHighchartsData(rawData);
      }, error => {
        console.log(error);
      }
    );

    this.getDatasetsTableData(this.datasetsTableContentSelection);
  }

  getDatasetsTableData(contentSelection: string) {
    this.loadingDatasetsAbsoluteTable = true;
    this.loadingDatasetsPercentageTable = true;
    this.dataService.getOpenScienceIndicatorsTableData('dataset', contentSelection).subscribe(
      rawData => {
        this.datasetsAbsoluteTableData = this.dataHandlerService.convertRawDataToIndicatorsTableData(rawData);
        this.datasetsPercentageTableData = this.dataHandlerService.convertRawDataToIndicatorsTableData(rawData);
        this.loadingDatasetsAbsoluteTable = false;
        this.loadingDatasetsPercentageTable = false;
      }, error => {
        console.log(error);
        this.loadingDatasetsAbsoluteTable = false;
        this.loadingDatasetsPercentageTable = false;
      }
    );
  }

  getContent(type: string, contentSelection: string): void {

    this.datasetsTableContentSelection = contentSelection;
    this.getDatasetsTableData(this.datasetsTableContentSelection);
  }

  getDatasetsWithPIDGroupBy(contentSelection: string): void {
    this.datasetsWithPIDGroupByView = contentSelection;
    if (contentSelection === 'country' && !this.datasetsWithPIDByCountryChartURL) {
      this.datasetsWithPIDByCountryChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with PID","type":"column","query":{"name":"new.oso.results.pid.affiliated.bycountry.all","parameters":[1,"dataset"],"profile":"${this.profileName}"}},{"name":"without PID","type":"column","query":{"name":"new.oso.results.pid.affiliated.bycountry.all","parameters":[0,"dataset"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets with PID","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'datasource' && !this.datasetsWithPIDByDatasourceChartURL) {
      this.datasetsWithPIDByDatasourceChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with PID","type":"bar","query":{"name":"new.oso.results.pid.affiliated.bydatasource","parameters":[1,"dataset"],"profile":"${this.profileName}"}},{"name":"without PID","type":"bar","query":{"name":"new.oso.results.pid.affiliated.bydatasource","parameters":[0,"dataset"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets with PID","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'organization' && !this.datasetsWithPIDByOrganizationChartURL) {
      this.datasetsWithPIDByOrganizationChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with PID","type":"bar","query":{"name":"new.oso.results.pid.affiliated.byorganization","parameters":[1,"dataset"],"profile":"${this.profileName}"}},{"name":"without PID","type":"bar","query":{"name":"new.oso.results.pid.affiliated.byorganization","parameters":[0,"dataset"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets with PID","align":"left","margin":50},"subtitle":{"text":"by organization (top organizations)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetIndicatorsPalette.join('","')}\"]}}`));
    }
  }

  getDatasetsWithPIDGroupByMobile(contentSelection: string): void {
    this.datasetsWithPIDGroupByView = contentSelection;
    if (contentSelection === 'country' && !this.datasetsWithPIDByCountryChartURLMobile) {
      this.datasetsWithPIDByCountryChartURLMobile = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with PID","type":"bar","query":{"name":"new.oso.results.pid.affiliated.bycountry","parameters":[1,"dataset"],"profile":"${this.profileName}"}},{"name":"without PID","type":"bar","query":{"name":"new.oso.results.pid.affiliated.bycountry","parameters":[0,"dataset"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets with PID","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'datasource' && !this.datasetsWithPIDByDatasourceChartURL) {
      this.datasetsWithPIDByDatasourceChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with PID","type":"bar","query":{"name":"new.oso.results.pid.affiliated.bydatasource","parameters":[1,"dataset"],"profile":"${this.profileName}"}},{"name":"without PID","type":"bar","query":{"name":"new.oso.results.pid.affiliated.bydatasource","parameters":[0,"dataset"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets with PID","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'organization' && !this.datasetsWithPIDByOrganizationChartURL) {
      this.datasetsWithPIDByOrganizationChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with PID","type":"bar","query":{"name":"new.oso.results.pid.affiliated.byorganization","parameters":[1,"dataset"],"profile":"${this.profileName}"}},{"name":"without PID","type":"bar","query":{"name":"new.oso.results.pid.affiliated.byorganization","parameters":[0,"dataset"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets with PID","align":"left","margin":50},"subtitle":{"text":"by organization (top organizations)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetIndicatorsPalette.join('","')}\"]}}`));
    }
  }

  getDatasetsWithLicenceGroupBy(contentSelection: string): void {
    this.datasetsWithLicenceGroupByView = contentSelection;
    if (contentSelection === 'country' && !this.datasetsWithLicenceByCountryChartURL) {
      this.datasetsWithLicenceByCountryChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with licence","type":"column","query":{"name":"new.oso.results.licence.affiliated.bycountry.all","parameters":[1,"dataset"],"profile":"${this.profileName}"}},{"name":"without licence","type":"column","query":{"name":"new.oso.results.licence.affiliated.bycountry.all","parameters":[0,"dataset"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets with licence","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'datasource' && !this.datasetsWithLicenceByDatasourceChartURL) {
      this.datasetsWithLicenceByDatasourceChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with licence","type":"bar","query":{"name":"new.oso.results.licence.affiliated.bydatasource","parameters":[1,"dataset"],"profile":"${this.profileName}"}},{"name":"without licence","type":"bar","query":{"name":"new.oso.results.licence.affiliated.bydatasource","parameters":[0,"dataset"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets with licence","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'organization' && !this.datasetsWithLicenceByOrganizationChartURL) {
      this.datasetsWithLicenceByOrganizationChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with licence","type":"bar","query":{"name":"new.oso.results.licence.affiliated.byorganization","parameters":[1,"dataset"],"profile":"${this.profileName}"}},{"name":"without licence","type":"bar","query":{"name":"new.oso.results.licence.affiliated.byorganization","parameters":[0,"dataset"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets with licence","align":"left","margin":50},"subtitle":{"text":"by organization (top organizations)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetIndicatorsPalette.join('","')}\"]}}`));
    }
  }

  getDatasetsWithLicenceGroupByMobile(contentSelection: string): void {
    this.datasetsWithLicenceGroupByView = contentSelection;
    if (contentSelection === 'country' && !this.datasetsWithLicenceByCountryChartURLMobile) {
      this.datasetsWithLicenceByCountryChartURLMobile = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with licence","type":"bar","query":{"name":"new.oso.results.licence.affiliated.bycountry","parameters":[1,"dataset"],"profile":"${this.profileName}"}},{"name":"without licence","type":"bar","query":{"name":"new.oso.results.licence.affiliated.bycountry","parameters":[0,"dataset"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets with licence","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'datasource' && !this.datasetsWithLicenceByDatasourceChartURL) {
      this.datasetsWithLicenceByDatasourceChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with licence","type":"bar","query":{"name":"new.oso.results.licence.affiliated.bydatasource","parameters":[1,"dataset"],"profile":"${this.profileName}"}},{"name":"without licence","type":"bar","query":{"name":"new.oso.results.licence.affiliated.bydatasource","parameters":[0,"dataset"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets with licence","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'organization' && !this.datasetsWithLicenceByOrganizationChartURL) {
      this.datasetsWithLicenceByOrganizationChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with licence","type":"bar","query":{"name":"new.oso.results.licence.affiliated.byorganization","parameters":[1,"dataset"],"profile":"${this.profileName}"}},{"name":"without licence","type":"bar","query":{"name":"new.oso.results.licence.affiliated.byorganization","parameters":[0,"dataset"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets with licence","align":"left","margin":50},"subtitle":{"text":"by organization (top organizations)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetIndicatorsPalette.join('","')}\"]}}`));
    }
  }

  getDatasetsWithCCLicenceGroupBy(contentSelection: string): void {
    this.datasetsWithCCLicenceGroupByView = contentSelection;
    if (contentSelection === 'country' && !this.datasetsWithCCLicenceByCountryChartURL) {
      this.datasetsWithCCLicenceByCountryChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with CC licence","type":"column","query":{"name":"new.oso.results.cc_licence.affiliated.bycountry.all","parameters":[1,"dataset"],"profile":"${this.profileName}"}},{"name":"without CC licence","type":"column","query":{"name":"new.oso.results.cc_licence.affiliated.bycountry.all","parameters":[0,"dataset"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets with CC licence","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'datasource' && !this.datasetsWithCCLicenceByDatasourceChartURL) {
      this.datasetsWithCCLicenceByDatasourceChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with CC licence","type":"bar","query":{"name":"new.oso.results.cc_licence.affiliated.bydatasource","parameters":[1,"dataset"],"profile":"${this.profileName}"}},{"name":"without CC licence","type":"bar","query":{"name":"new.oso.results.cc_licence.affiliated.bydatasource","parameters":[0,"dataset"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets with CC licence","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'organization' && !this.datasetsWithCCLicenceByOrganizationChartURL) {
      this.datasetsWithCCLicenceByOrganizationChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with CC licence","type":"bar","query":{"name":"new.oso.results.cc_licence.affiliated.byorganization","parameters":[1,"dataset"],"profile":"${this.profileName}"}},{"name":"without CC licence","type":"bar","query":{"name":"new.oso.results.cc_licence.affiliated.byorganization","parameters":[0,"dataset"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets with CC licence","align":"left","margin":50},"subtitle":{"text":"by organization (top organizations)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetIndicatorsPalette.join('","')}\"]}}`));
    }
  }

  getDatasetsWithCCLicenceGroupByMobile(contentSelection: string): void {
    this.datasetsWithCCLicenceGroupByView = contentSelection;
    if (contentSelection === 'country' && !this.datasetsWithCCLicenceByCountryChartURLMobile) {
      this.datasetsWithCCLicenceByCountryChartURLMobile = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with CC licence","type":"bar","query":{"name":"new.oso.results.cc_licence.affiliated.bycountry","parameters":[1,"dataset"],"profile":"${this.profileName}"}},{"name":"without CC licence","type":"bar","query":{"name":"new.oso.results.cc_licence.affiliated.bycountry","parameters":[0,"dataset"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets with CC licence","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'datasource' && !this.datasetsWithCCLicenceByDatasourceChartURL) {
      this.datasetsWithCCLicenceByDatasourceChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with CC licence","type":"bar","query":{"name":"new.oso.results.cc_licence.affiliated.bydatasource","parameters":[1,"dataset"],"profile":"${this.profileName}"}},{"name":"without CC licence","type":"bar","query":{"name":"new.oso.results.cc_licence.affiliated.bydatasource","parameters":[0,"dataset"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets with CC licence","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'organization' && !this.datasetsWithCCLicenceByOrganizationChartURL) {
      this.datasetsWithCCLicenceByOrganizationChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"with CC licence","type":"bar","query":{"name":"new.oso.results.cc_licence.affiliated.byorganization","parameters":[1,"dataset"],"profile":"${this.profileName}"}},{"name":"without CC licence","type":"bar","query":{"name":"new.oso.results.cc_licence.affiliated.byorganization","parameters":[0,"dataset"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets with CC licence","align":"left","margin":50},"subtitle":{"text":"by organization (top organizations)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetIndicatorsPalette.join('","')}\"]}}`));
    }
  }
}

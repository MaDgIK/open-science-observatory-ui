import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { ContinentOSDatasetsComponent } from './continent-os-datasets.component';
import { ContinentOSDatasetsIndicatorsComponent } from './continent-os-datasets-indicators.component';

const continentOpenScienceDatasetsRoutes: Routes = [
  {
    path: '',
    component: ContinentOSDatasetsComponent,
    children : [
      {
        path: '',
        redirectTo: 'licence',
        pathMatch: 'full',
      },
      {
        path: ':indicator',
        component: ContinentOSDatasetsIndicatorsComponent,
      }
    ]
  },
];

@NgModule ({
  imports: [RouterModule.forChild(continentOpenScienceDatasetsRoutes)],
  exports: [RouterModule]
})

export class ContinentOpenScienceDatasetsRoutingModule {}

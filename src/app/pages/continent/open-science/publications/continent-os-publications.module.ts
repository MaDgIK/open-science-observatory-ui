import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContinentOpenSciencePublicationsRoutingModule } from './continent-os-publications-routing.module';
import { ReusableComponentsModule } from '../../../../shared/reusablecomponents/reusable-components.module';
import { ContinentOSPublicationsComponent } from './continent-os-publications.component';
import { ContinentOSPublicationsIndicatorsComponent } from './continent-os-publications-indicators.component';
import { DataService } from '../../../../services/data.service';
import { DataHandlerService } from '../../../../services/data-handler.service';


@NgModule ({
  imports: [
    CommonModule,
    ContinentOpenSciencePublicationsRoutingModule,
    ReusableComponentsModule,
  ],
  declarations: [
    ContinentOSPublicationsComponent,
    ContinentOSPublicationsIndicatorsComponent,
  ],
  providers: [
    DataService,
    DataHandlerService
  ],
})

export class ContinentOpenSciencePublicationsModule {}

import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { ContinentOSPublicationsComponent } from './continent-os-publications.component';
import { ContinentOSPublicationsIndicatorsComponent } from './continent-os-publications-indicators.component';

const continentOpenSciencePublicationsRoutes: Routes = [
  {
    path: '',
    component: ContinentOSPublicationsComponent,
    children : [
      {
        path: '',
        redirectTo: 'gold-green',
        pathMatch: 'full',
      },
      {
        path: ':indicator',
        component: ContinentOSPublicationsIndicatorsComponent,
      }
    ]
  },
];

@NgModule ({
  imports: [RouterModule.forChild(continentOpenSciencePublicationsRoutes)],
  exports: [RouterModule]
})

export class ContinentOpenSciencePublicationsRoutingModule {}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { DataService } from '../../services/data.service';
import { DataHandlerService } from '../../services/data-handler.service';
import { CountryOverviewData, EuropeData, SelectedCountry } from '../../domain/overview-map-data';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';

@Component({
  selector: 'app-continent',
  templateUrl: './continent.component.html',
})

export class ContinentComponent implements OnInit {

  lastUpdateDate: string;

  continentName: string;

  europeOverviewData: EuropeData;
  europeOverviewMapData: EuropeData;

  mapViewActive = false;
  selectedCountry: SelectedCountry = null;
  selectedCountryData: CountryOverviewData = null;

  constructor(private dataService: DataService,
              private dataHandlerService: DataHandlerService,
              private route: ActivatedRoute,
              private sanitizer: DomSanitizer) { }

  ngOnInit(): void {

    // window.scroll(0, 0);

    // this.continentName = this.route.snapshot.paramMap.get('continentName');
    this.continentName = 'europe';

    console.log('continentName: ', this.continentName);

    this.dataService.getLastUpdateDate().subscribe(
      rawData => {
        this.lastUpdateDate = this.dataHandlerService.convertRawDataToLastUpdateDate(rawData);
      }, error => {
        console.log(error);
      }
    );

    this.dataService.getEuropeOAPercentages().subscribe(
      rawData => {
        this.europeOverviewData = this.dataHandlerService.convertRawDataToEuropeOverviewData(rawData);
      }, error => {
        console.log(error);
      }
    );

    this.dataService.getEuropeOverviewData().subscribe(
      rawData => {
        this.europeOverviewMapData = this.dataHandlerService.convertRawDataToEuropeOverviewData(rawData);
      }, error => {
        console.log(error);
      }
    );

    // this.createOverviewContent();
  }

  getHeight(percentage: number) {
    if (percentage < 50) {
      return Math.round(percentage) * 1.5;
    } else {
      return Math.round(percentage);
    }
  }

  getNumberFontSize(percentage: number) {
    if (percentage < 50) {
      return Math.round(percentage * 45 / 100) * 1.5;
    } else {
      return Math.round(percentage * 45 / 100);
    }
  }

  getEntityNameFontSize(percentage: number) {
    if (percentage < 50) {
      return Math.round(percentage * 30 / 100) * 1.5;
    } else {
      return Math.round(percentage * 30 / 100);
    }
  }

  getPublicationsIconWidth(percentage: number) {
    if (percentage < 50) {
      return Math.round(percentage * 60 / 100) * 1.5;
    } else {
      return Math.round(percentage * 60 / 100);
    }
  }

  getDatasetsIconWidth(percentage: number) {
    if (percentage < 50) {
      return Math.round(percentage * 45 / 100) * 1.5;
    } else {
      return Math.round(percentage * 45 / 100);
    }
  }

  getSoftwareIconWidth(percentage: number) {
    if (percentage < 50) {
      return Math.round(percentage * 50 / 100) * 1.5;
    } else {
      return Math.round(percentage * 50 / 100);
    }
  }

  getOtherIconWidth(percentage: number) {
    if (percentage < 50) {
      return Math.round(percentage * 45 / 100) * 1.5;
    } else {
      return Math.round(percentage * 45 / 100);
    }
  }

  /** Europe Map  --> **/
  // toggleView(show: string) {
  //   if (show === 'map') {
  //     this.mapViewActive = true;
  //   } else if (show === 'columns') {
  //     this.mapViewActive = false;
  //   }
  // }

  toggleView(ob: MatSlideToggleChange) {
    if (ob.checked) {
      this.mapViewActive = true;
    } else {
      this.mapViewActive = false;
    }
  }

  countrySelected(selectedCountry: SelectedCountry) {
    this.selectedCountry = selectedCountry;
    this.selectedCountryData = null;
    this.dataService.getCountryOverviewData(this.selectedCountry.code).subscribe(
      rawData => {
        this.selectedCountryData = this.dataHandlerService.convertRawDataToCountryOverviewData(rawData);
      }, error => {
        console.log(error);
      }
    );
  }

  deselectCountry() {
    this.selectedCountry = null;
    this.selectedCountryData = null;
  }
  /** <-- Europe Map **/

}

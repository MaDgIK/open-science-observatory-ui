import {Component, OnInit} from '@angular/core';
import { datasetPalette, otherResearchProductsPalette, publicationColor, publicationPalette, publicationIndicatorsPalette,
  softwarePalette, datasetColor, softwareColor, otherResearchProductsColor, gradientStartColor } from '../../../chart-palettes';
import {environment} from '../../../../environments/environment';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';
import {TreemapHighchartsData} from '../../../domain/treemap-highcharts-data';
import {DataHandlerService} from '../../../services/data-handler.service';
import {DataService} from '../../../services/data.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-continent-overview',
  templateUrl: './continent-overview.component.html',
})

export class ContinentOverviewComponent implements OnInit {

  private chartsURL = environment.API_ENDPOINT + 'chart?json=';
  private profileName = environment.profileName;

  private publicationPalette = publicationPalette;
  private publicationIndicatorsPalette = publicationIndicatorsPalette;
  private datasetPalette = datasetPalette;
  private softwarePalette = softwarePalette;
  private otherResearchProductsPalette = otherResearchProductsPalette;

  publicationColor = publicationColor;
  datasetColor = datasetColor;
  softwareColor = softwareColor;
  otherResearchProductsColor = otherResearchProductsColor;
  gradientStartColor = gradientStartColor;

  peerReviewedPublicationsOverTimeChartURL: SafeResourceUrl;
  publicationsByTypeChartURL: SafeResourceUrl;


  publicationsByFunderData: TreemapHighchartsData[];
  datasetsByFunderData: TreemapHighchartsData[];
  softwareByFunderData: TreemapHighchartsData[];
  otherByFunderData: TreemapHighchartsData[];


  publicationGroupByView = 'country';
  publicationsByCountryChartURL: SafeResourceUrl;
  publicationsByDatasourceChartURL: SafeResourceUrl;
  publicationsByOrganizationChartURL: SafeResourceUrl;
  publicationsByCountryChartURLMobile: SafeResourceUrl;
  publicationsByDatasourceChartURLMobile: SafeResourceUrl;
  publicationsByOrganizationChartURLMobile: SafeResourceUrl;

  datasetsGroupByView = 'country';
  datasetsByCountryChartURL: SafeResourceUrl;
  datasetsByDatasourceChartURL: SafeResourceUrl;
  datasetsByOrganizationChartURL: SafeResourceUrl;
  datasetsByCountryChartURLMobile: SafeResourceUrl;
  datasetsByDatasourceChartURLMobile: SafeResourceUrl;
  datasetsByOrganizationChartURLMobile: SafeResourceUrl;

  softwareGroupByView = 'country';
  softwareByCountryChartURL: SafeResourceUrl;
  softwareByDatasourceChartURL: SafeResourceUrl;
  softwareByOrganizationChartURL: SafeResourceUrl;
  softwareByCountryChartURLMobile: SafeResourceUrl;
  softwareByDatasourceChartURLMobile: SafeResourceUrl;
  softwareByOrganizationChartURLMobile: SafeResourceUrl;

  orpGroupByView = 'country';
  otherByCountryChartURL: SafeResourceUrl;
  otherByDatasourceChartURL: SafeResourceUrl;
  otherByOrganizationChartURL: SafeResourceUrl;
  otherByCountryChartURLMobile: SafeResourceUrl;
  otherByDatasourceChartURLMobile: SafeResourceUrl;
  otherByOrganizationChartURLMobile: SafeResourceUrl;



  constructor(private dataService: DataService,
              private dataHandlerService: DataHandlerService,
              private route: ActivatedRoute,
              private sanitizer: DomSanitizer) { }

  ngOnInit(): void {

    this.publicationsByTypeChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"publications","type":"bar","query":{"name":"new.oso.publications.peer_reviewed.bytype","profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access publications","align":"left","margin":50},"subtitle":{"text":"by type","align":"left","margin":50},"yAxis":{"title":{"text":""}},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":false},"credits":{"href":null,"enabled":false},"colors":[\"${this.publicationIndicatorsPalette.join('","')}\"]}}`));
    this.peerReviewedPublicationsOverTimeChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"peer-reviewed","type":"column","query":{"name":"new.oso.publications.oa_timeline.peer_reviewed.affiliated","parameters":[1],"profile":"${this.profileName}"}},{"name":"non-peer-reviewed","type":"column","query":{"name":"new.oso.publications.oa_timeline.peer_reviewed.affiliated","parameters":[0],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access publications","align":"left","margin":50},"subtitle":{"text":"by peer-review, over time","align":"left","margin":50},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"text":"All publication indicators refer to peer-reviewed publications, unless explicitly stated otherwise.","enabled":true},"colors":[\"${this.publicationIndicatorsPalette.join('","')}\"]}}`));

    // this.publicationsByCountryChartURLMobile = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"Data","type":"bar","query":{"name":"oso.results.bycountry","parameters":["publication"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"OA Publications","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""}},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"color":{"linearGradient":{"x1":0,"y1":0,"x2":0,"y2":1},"stops":[[0,"${this.publicationColor}"],[1,"${this.gradientStartColor}"]]}}},"legend":{"enabled":false},"credits":{"href":null,"enabled":false}}}`));

    this.dataService.getFundersResultsByPublications().subscribe(
      rawData => {
        this.publicationsByFunderData = this.dataHandlerService.convertRawDataToTreemapHighchartsData(rawData);
      }, error => {
        console.log(error);
      }
    );

    this.dataService.getFundersResultsByType('dataset').subscribe(
      rawData => {
        this.datasetsByFunderData = this.dataHandlerService.convertRawDataToTreemapHighchartsData(rawData);
      }, error => {
        console.log(error);
      }
    );

    this.dataService.getFundersResultsByType('software').subscribe(
      rawData => {
        this.softwareByFunderData = this.dataHandlerService.convertRawDataToTreemapHighchartsData(rawData);
      }, error => {
        console.log(error);
      }
    );

    this.dataService.getFundersResultsByType('other').subscribe(
      rawData => {
        this.otherByFunderData = this.dataHandlerService.convertRawDataToTreemapHighchartsData(rawData);
      }, error => {
        console.log(error);
      }
    );

    this.publicationsByCountryChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"publications","type":"column","query":{"name":"new.oso.publications.peer_reviewed.bycountry.all","profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access publications","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""}},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":false},"credits":{"href":null,"enabled":false},"colors":[\"${this.publicationPalette.join('","')}\"]}}`));
    this.publicationsByCountryChartURLMobile = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"publications","type":"bar","query":{"name":"new.oso.publications.peer_reviewed.bycountry","profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access publications","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""}},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":false},"credits":{"href":null,"enabled":false},"colors":[\"${this.publicationPalette.join('","')}\"]}}`));

    // this.publicationsByCountryChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"Data","type":"column","query":{"name":"oso.results.bycountry","parameters":["publication"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"OA Publications","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""}},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"color":{"linearGradient":{"x1":0,"y1":0,"x2":0,"y2":1},"stops":[[0,"${this.publicationColor}"],[1,"#745b93"]]}}},"legend":{"enabled":false},"credits":{"href":null,"enabled":false}}}`));
    // this.publicationsByCountryChartURLMobile = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"Data","type":"bar","query":{"name":"oso.results.bycountry","parameters":["publication"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"OA Publications","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""}},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"color":{"linearGradient":{"x1":0,"y1":0,"x2":0,"y2":1},"stops":[[0,"${this.publicationColor}"],[1,"${this.gradientStartColor}"]]}}},"legend":{"enabled":false},"credits":{"href":null,"enabled":false}}}`));

    this.datasetsByCountryChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"datasets","type":"column","query":{"name":"new.oso.results.bycountry.all","parameters":["dataset"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""}},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":false},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetPalette.join('","')}\"]}}`));
    this.datasetsByCountryChartURLMobile = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"datasets","type":"bar","query":{"name":"oso.results.bycountry","parameters":["dataset"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""}},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":false},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetPalette.join('","')}\"]}}`));

    this.softwareByCountryChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"software","type":"column","query":{"name":"new.oso.results.bycountry.all","parameters":["software"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Source software","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""}},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":false},"credits":{"href":null,"enabled":false},"colors":[\"${this.softwarePalette.join('","')}\"]}}`));
    this.softwareByCountryChartURLMobile = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"software","type":"bar","query":{"name":"oso.results.bycountry","parameters":["software"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Source software","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""}},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":false},"credits":{"href":null,"enabled":false},"colors":[\"${this.softwarePalette.join('","')}\"]}}`));

    this.otherByCountryChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"other research products","type":"column","query":{"name":"new.oso.results.bycountry.all","parameters":["other"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access other research products","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""}},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":false},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherResearchProductsPalette.join('","')}\"]}}`));
    this.otherByCountryChartURLMobile = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"other research products","type":"bar","query":{"name":"oso.results.bycountry","parameters":["other"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access other research products","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""}},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":false},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherResearchProductsPalette.join('","')}\"]}}`));
  }

  getPublicationsGroupBy(contentSelection: string): void {
    this.publicationGroupByView = contentSelection;
    if (contentSelection === 'country' && !this.publicationsByCountryChartURL) {
      this.publicationsByCountryChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"publications","type":"column","query":{"name":"new.oso.publications.peer_reviewed.bycountry.all","profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access publications","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""}},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":false},"credits":{"href":null,"enabled":false},"colors":[\"${this.publicationPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'datasource' && !this.publicationsByDatasourceChartURL) {
      this.publicationsByDatasourceChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"publications","type":"column","query":{"name":"new.oso.publications.peer_reviewed.bydatasource","profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access publications","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""}},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":false},"credits":{"href":null,"enabled":false},"colors":[\"${this.publicationPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'organization' && !this.publicationsByOrganizationChartURL) {
      this.publicationsByOrganizationChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"publications","type":"column","query":{"name":"new.oso.publication.peer_reviewed.byorganization","profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access publications","align":"left","margin":50},"subtitle":{"text":"by organization (top organizations)","align":"left"},"yAxis":{"title":{"text":""}},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":false},"credits":{"href":null,"enabled":false},"colors":[\"${this.publicationPalette.join('","')}\"]}}`));
    }
  }

  getPublicationsGroupByMobile(contentSelection: string): void {
    this.publicationGroupByView = contentSelection;
    if (contentSelection === 'country' && !this.publicationsByCountryChartURLMobile) {
      this.publicationsByCountryChartURLMobile = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"publications","type":"bar","query":{"name":"new.oso.publications.peer_reviewed.bycountry","profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access publications","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""}},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":false},"credits":{"href":null,"enabled":false},"colors":[\"${this.publicationPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'datasource' && !this.publicationsByDatasourceChartURLMobile) {
      this.publicationsByDatasourceChartURLMobile = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"publications","type":"bar","query":{"name":"new.oso.publications.peer_reviewed.bydatasource","profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access publications","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""}},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":false},"credits":{"href":null,"enabled":false},"colors":[\"${this.publicationPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'organization' && !this.publicationsByOrganizationChartURLMobile) {
      this.publicationsByOrganizationChartURLMobile = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"publications","type":"bar","query":{"name":"new.oso.publication.peer_reviewed.byorganization","profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access publications","align":"left","margin":50},"subtitle":{"text":"by organization (top organizations)","align":"left"},"yAxis":{"title":{"text":""}},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":false},"credits":{"href":null,"enabled":false},"colors":[\"${this.publicationPalette.join('","')}\"]}}`));
    }
  }

  getDatasetsGroupBy(contentSelection: string): void {
    this.datasetsGroupByView = contentSelection;
    if (contentSelection === 'country' && !this.datasetsByCountryChartURL) {
      this.datasetsByCountryChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"datasets","type":"column","query":{"name":"new.oso.results.bycountry.all","parameters":["dataset"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""}},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":false},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'datasource' && !this.datasetsByDatasourceChartURL) {
      this.datasetsByDatasourceChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"datasets","type":"column","query":{"name":"oso.results.bydatasource","parameters":["dataset"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""}},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":false},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'organization' && !this.datasetsByOrganizationChartURL) {
      this.datasetsByOrganizationChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"datasets","type":"column","query":{"name":"oso.results.byorganization","parameters":["dataset"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets","align":"left","margin":50},"subtitle":{"text":"by organization (top organizations)","align":"left"},"yAxis":{"title":{"text":""}},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":false},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetPalette.join('","')}\"]}}`));
    }
  }

  getDatasetsGroupByMobile(contentSelection: string): void {
    this.datasetsGroupByView = contentSelection;
    if (contentSelection === 'country' && !this.datasetsByCountryChartURLMobile) {
      this.datasetsByCountryChartURLMobile = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"datasets","type":"bar","query":{"name":"oso.results.bycountry","parameters":["dataset"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""}},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":false},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'datasource' && !this.datasetsByDatasourceChartURLMobile) {
      this.datasetsByDatasourceChartURLMobile = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"datasets","type":"bar","query":{"name":"oso.results.bydatasource","parameters":["dataset"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""}},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":false},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'organization' && !this.datasetsByOrganizationChartURLMobile) {
      this.datasetsByOrganizationChartURLMobile = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"datasets","type":"bar","query":{"name":"oso.results.byorganization","parameters":["dataset"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets","align":"left","margin":50},"subtitle":{"text":"by organization (top organizations)","align":"left"},"yAxis":{"title":{"text":""}},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":false},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetPalette.join('","')}\"]}}`));
    }
  }

  getSoftwareGroupBy(contentSelection: string): void {
    this.softwareGroupByView = contentSelection;
    if (contentSelection === 'country' && !this.softwareByCountryChartURL) {
      this.softwareByCountryChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"software","type":"column","query":{"name":"new.oso.results.bycountry.all","parameters":["software"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Source software","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""}},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":false},"credits":{"href":null,"enabled":true},"colors":[\"${this.softwarePalette.join('","')}\"]}}`));
    } else if (contentSelection === 'datasource' && !this.softwareByDatasourceChartURL) {
      this.softwareByDatasourceChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"software","type":"column","query":{"name":"oso.results.bydatasource","parameters":["software"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Source software","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""}},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":false},"credits":{"href":null,"enabled":false},"colors":[\"${this.softwarePalette.join('","')}\"]}}`));
    } else if (contentSelection === 'organization' && !this.softwareByOrganizationChartURL) {
      this.softwareByOrganizationChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"software","type":"column","query":{"name":"oso.results.byorganization","parameters":["software"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Source software","align":"left","margin":50},"subtitle":{"text":"by organization (top organizations)","align":"left"},"yAxis":{"title":{"text":""}},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":false},"credits":{"href":null,"enabled":false},"colors":[\"${this.softwarePalette.join('","')}\"]}}`));
    }
  }

  getSoftwareGroupByMobile(contentSelection: string): void {
    this.softwareGroupByView = contentSelection;
    if (contentSelection === 'country' && !this.softwareByCountryChartURLMobile) {
      this.softwareByCountryChartURLMobile = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"software","type":"bar","query":{"name":"oso.results.bycountry","parameters":["software"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Source software","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""}},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":false},"credits":{"href":null,"enabled":true},"colors":[\"${this.softwarePalette.join('","')}\"]}}`));
    } else if (contentSelection === 'datasource' && !this.softwareByDatasourceChartURLMobile) {
      this.softwareByDatasourceChartURLMobile = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"software","type":"bar","query":{"name":"oso.results.bydatasource","parameters":["software"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Source software","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""}},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":false},"credits":{"href":null,"enabled":false},"colors":[\"${this.softwarePalette.join('","')}\"]}}`));
    } else if (contentSelection === 'organization' && !this.softwareByOrganizationChartURLMobile) {
      this.softwareByOrganizationChartURLMobile = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"software","type":"bar","query":{"name":"oso.results.byorganization","parameters":["software"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Source software","align":"left","margin":50},"subtitle":{"text":"by organization (top organizations)","align":"left"},"yAxis":{"title":{"text":""}},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":false},"credits":{"href":null,"enabled":false},"colors":[\"${this.softwarePalette.join('","')}\"]}}`));
    }
  }

  getORPGroupBy(contentSelection: string): void {
    this.orpGroupByView = contentSelection;
    if (contentSelection === 'country' && !this.otherByCountryChartURL) {
      this.otherByCountryChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"other research products","type":"column","query":{"name":"new.oso.results.bycountry.all","parameters":["other"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access other research products","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""}},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":false},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherResearchProductsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'datasource' && !this.otherByDatasourceChartURL) {
      this.otherByDatasourceChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"other research products","type":"column","query":{"name":"oso.results.bydatasource","parameters":["other"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access other research products","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""}},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":false},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherResearchProductsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'organization' && !this.otherByOrganizationChartURL) {
      this.otherByOrganizationChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"other research products","type":"column","query":{"name":"oso.results.byorganization","parameters":["other"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access other research products","align":"left","margin":50},"subtitle":{"text":"by organization (top organizations)","align":"left"},"yAxis":{"title":{"text":""}},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":false},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherResearchProductsPalette.join('","')}\"]}}`));
    }
  }

  getORPGroupByMobile(contentSelection: string): void {
    this.orpGroupByView = contentSelection;
    if (contentSelection === 'country' && !this.otherByCountryChartURLMobile) {
      this.otherByCountryChartURLMobile = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"other research products","type":"bar","query":{"name":"oso.results.bycountry","parameters":["other"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access other research products","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""}},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":false},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherResearchProductsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'datasource' && !this.otherByDatasourceChartURLMobile) {
      this.otherByDatasourceChartURLMobile = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"other research products","type":"bar","query":{"name":"oso.results.bydatasource","parameters":["other"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access other research products","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""}},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":false},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherResearchProductsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'organization' && !this.otherByOrganizationChartURLMobile) {
      this.otherByOrganizationChartURLMobile = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"other research products","type":"bar","query":{"name":"oso.results.byorganization","parameters":["other"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access other research products","align":"left","margin":50},"subtitle":{"text":"by organization (top organizations)","align":"left"},"yAxis":{"title":{"text":""}},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":false},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherResearchProductsPalette.join('","')}\"]}}`));
    }
  }
}

import { Component, OnInit } from '@angular/core';
import {
  gradientStartColor,
  softwareColor,
  softwarePalette, softwareIndicatorsPalette, softwareIndicatorsPaletteReversed
} from '../../../../chart-palettes';
import {environment} from '../../../../../environments/environment';
import {CountryTableData} from '../../../../domain/overview-map-data';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';
import {TreemapHighchartsData} from '../../../../domain/treemap-highcharts-data';
import {DataHandlerService} from '../../../../services/data-handler.service';
import {DataService} from '../../../../services/data.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-continent-collaboration-software-indicators',
  templateUrl: './continent-collaboration-software-indicators.component.html',
})

export class ContinentCollaborationSoftwareIndicatorsComponent implements OnInit {

  private chartsURL = environment.API_ENDPOINT + 'chart?json=';
  private profileName = environment.profileName;

  indicator: string;

  private softwareIndicatorsPalette = softwareIndicatorsPalette;
  private softwareIndicatorsPaletteReversed = softwareIndicatorsPaletteReversed;
  softwareColor = softwareColor;

  softwareCoFundedFundersTimeline: SafeResourceUrl;
  softwareCoFundedProjectsTimeline: SafeResourceUrl;

  softwareCoFundedFundersGroupByView = 'country';
  softwareCoFundedFundersByCountryChartURL: SafeResourceUrl;
  softwareCoFundedFundersByDatasourceChartURL: SafeResourceUrl;
  softwareCoFundedFundersByOrganizationChartURL: SafeResourceUrl;
  softwareCoFundedFundersByCountryChartURLMobile: SafeResourceUrl;

  softwareCoFundedProjectsGroupByView = 'country';
  softwareCoFundedProjectsByCountryChartURL: SafeResourceUrl;
  softwareCoFundedProjectsByDatasourceChartURL: SafeResourceUrl;
  softwareCoFundedProjectsByOrganizationChartURL: SafeResourceUrl;
  softwareCoFundedProjectsByCountryChartURLMobile: SafeResourceUrl;

  softwareCoFundedFundersByFunderData: TreemapHighchartsData[];
  softwareCoFundedProjectsByFunderData: TreemapHighchartsData[];

  loadingSoftwareAbsoluteTable: boolean = true;
  loadingSoftwarePercentageTable: boolean = true;
  softwareTableContentSelection: string = 'affiliated';
  softwareAbsoluteTableData: CountryTableData[];
  softwarePercentageTableData: CountryTableData[];

  constructor(private dataService: DataService,
              private dataHandlerService: DataHandlerService,
              private route: ActivatedRoute,
              private sanitizer: DomSanitizer) { }

  ngOnInit(): void {

    this.route.params.subscribe(params => {

      this.indicator = params['indicator'];
    });

    this.softwareCoFundedFundersTimeline = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"> 1 funders","type":"column","query":{"name":"new.oso.results.funders_collab_timeline.affiliated","parameters":[1,"software"],"profile":"${this.profileName}"}},{"name":"{0,1} funders","type":"column","query":{"name":"new.oso.results.funders_collab_timeline.affiliated","parameters":[0,"software"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Source software with multiple funders","align":"left","margin":50},"subtitle":{"text":"over time","align":"left","margin":50},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.softwareIndicatorsPaletteReversed.join('","')}\"]}}`));
    this.softwareCoFundedProjectsTimeline = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"> 1 projects","type":"column","query":{"name":"new.oso.results.projects_collab_timeline.affiliated","parameters":[1,"software"],"profile":"${this.profileName}"}},{"name":"{0,1} projects","type":"column","query":{"name":"new.oso.results.projects_collab_timeline.affiliated","parameters":[0,"software"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Source software linked to multiple projects","align":"left","margin":50},"subtitle":{"text":"over time","align":"left","margin":50},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.softwareIndicatorsPalette.join('","')}\"]}}`));

    this.softwareCoFundedFundersByCountryChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"> 1 funders","type":"column","query":{"name":"new.oso.results.funders_collab.affiliated.bycountry.all","parameters":[1,"software"],"profile":"${this.profileName}"}},{"name":"{0,1} funders","type":"column","query":{"name":"new.oso.results.funders_collab.affiliated.bycountry.all","parameters":[0,"software"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Source software with multiple funders","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.softwareIndicatorsPaletteReversed.join('","')}\"]}}`));
    this.softwareCoFundedFundersByCountryChartURLMobile = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"> 1 funders","type":"bar","query":{"name":"new.oso.results.funders_collab.affiliated.bycountry","parameters":[1,"software"],"profile":"${this.profileName}"}},{"name":"{0,1} funders","type":"bar","query":{"name":"new.oso.results.funders_collab.affiliated.bycountry","parameters":[0,"software"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Source software with multiple funders","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.softwareIndicatorsPaletteReversed.join('","')}\"]}}`));

    this.softwareCoFundedProjectsByCountryChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"> 1 projects","type":"column","query":{"name":"new.oso.results.projects_collab.affiliated.bycountry.all","parameters":[1,"software"],"profile":"${this.profileName}"}},{"name":"{0,1} projects","type":"column","query":{"name":"new.oso.results.projects_collab.affiliated.bycountry.all","parameters":[0,"software"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Source software linked to multiple projects","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.softwareIndicatorsPalette.join('","')}\"]}}`));
    this.softwareCoFundedProjectsByCountryChartURLMobile = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"> 1 projects","type":"bar","query":{"name":"new.oso.results.projects_collab.affiliated.bycountry","parameters":[1,"software"],"profile":"${this.profileName}"}},{"name":"{0,1} projects","type":"bar","query":{"name":"new.oso.results.projects_collab.affiliated.bycountry","parameters":[0,"software"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Source software linked to multiple projects","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.softwareIndicatorsPalette.join('","')}\"]}}`));


    this.dataService.getFundersResultsByTypeForCollaborationIndicatorForCountry('software', 'funders').subscribe(
      rawData => {
        this.softwareCoFundedFundersByFunderData = this.dataHandlerService.convertRawDataToTreemapHighchartsData(rawData);
      }, error => {
        console.log(error);
      }
    );

    this.dataService.getFundersResultsByTypeForCollaborationIndicatorForCountry('software', 'projects').subscribe(
      rawData => {
        this.softwareCoFundedProjectsByFunderData = this.dataHandlerService.convertRawDataToTreemapHighchartsData(rawData);
      }, error => {
        console.log(error);
      }
    );

    this.getSoftwareTableData(this.softwareTableContentSelection);
  }

  getSoftwareTableData(contentSelection: string) {

    this.loadingSoftwareAbsoluteTable = true;
    this.loadingSoftwarePercentageTable = true;
    this.dataService.getCollaborationIndicatorsTableData('software', contentSelection).subscribe(
      rawData => {
        this.softwareAbsoluteTableData = this.dataHandlerService.convertRawDataToIndicatorsTableData(rawData);
        this.softwarePercentageTableData = this.dataHandlerService.convertRawDataToIndicatorsTableData(rawData);
        this.loadingSoftwareAbsoluteTable = false;
        this.loadingSoftwarePercentageTable = false;
      }, error => {
        console.log(error);
        this.loadingSoftwareAbsoluteTable = false;
        this.loadingSoftwarePercentageTable = false;
      }
    );
  }

  getContent(type: string, contentSelection: string): void {
    this.softwareTableContentSelection = contentSelection;
    this.getSoftwareTableData(this.softwareTableContentSelection);
  }

  getSoftwareCoFundedFundersGroupBy(contentSelection: string): void {
    this.softwareCoFundedFundersGroupByView = contentSelection;
    if (contentSelection === 'country' && !this.softwareCoFundedFundersByCountryChartURL) {
      this.softwareCoFundedFundersByCountryChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"{0,1} funders","type":"column","query":{"name":"new.oso.results.funders_collab.affiliated.bycountry.all","parameters":[0,"software"],"profile":"${this.profileName}"}},{"name":"> 1 funders","type":"column","query":{"name":"new.oso.results.funders_collab.affiliated.bycountry.all","parameters":[1,"software"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Source software with multiple funders","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.softwareIndicatorsPaletteReversed.join('","')}\"]}}`));
    } else if (contentSelection === 'datasource' && !this.softwareCoFundedFundersByDatasourceChartURL) {
      this.softwareCoFundedFundersByDatasourceChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"{0,1} funders","type":"bar","query":{"name":"new.oso.results.funders_collab.affiliated.bydatasource","parameters":[0,"software"],"profile":"${this.profileName}"}},{"name":"> 1 funders","type":"bar","query":{"name":"new.oso.results.funders_collab.affiliated.bydatasource","parameters":[1,"software"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Source software with multiple funders","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.softwareIndicatorsPaletteReversed.join('","')}\"]}}`));
    } else if (contentSelection === 'organization' && !this.softwareCoFundedFundersByOrganizationChartURL) {
      this.softwareCoFundedFundersByOrganizationChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"{0,1} funders","type":"bar","query":{"name":"new.oso.results.funders_collab.affiliated.byorganization","parameters":[0,"software"],"profile":"${this.profileName}"}},{"name":"> 1 funders","type":"bar","query":{"name":"new.oso.results.funders_collab.affiliated.byorganization","parameters":[1,"software"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Source software with multiple funders","align":"left","margin":50},"subtitle":{"text":"by organization (top organizations)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.softwareIndicatorsPaletteReversed.join('","')}\"]}}`));
    }
  }

  getSoftwareCoFundedFundersGroupByMobile(contentSelection: string): void {
    this.softwareCoFundedFundersGroupByView = contentSelection;
    if (contentSelection === 'country' && !this.softwareCoFundedFundersByCountryChartURLMobile) {
      this.softwareCoFundedFundersByCountryChartURLMobile = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"{0,1} funders","type":"bar","query":{"name":"new.oso.results.funders_collab.affiliated.bycountry","parameters":[0,"software"],"profile":"${this.profileName}"}},{"name":"> 1 funders","type":"bar","query":{"name":"new.oso.results.funders_collab.affiliated.bycountry","parameters":[1,"software"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Source software with multiple funders","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.softwareIndicatorsPaletteReversed.join('","')}\"]}}`));
    } else if (contentSelection === 'datasource' && !this.softwareCoFundedFundersByDatasourceChartURL) {
      this.softwareCoFundedFundersByDatasourceChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"{0,1} funders","type":"bar","query":{"name":"new.oso.results.funders_collab.affiliated.bydatasource","parameters":[0,"software"],"profile":"${this.profileName}"}},{"name":"> 1 funders","type":"bar","query":{"name":"new.oso.results.funders_collab.affiliated.bydatasource","parameters":[1,"software"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Source software with multiple funders","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.softwareIndicatorsPaletteReversed.join('","')}\"]}}`));
    } else if (contentSelection === 'organization' && !this.softwareCoFundedFundersByOrganizationChartURL) {
      this.softwareCoFundedFundersByOrganizationChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"{0,1} funders","type":"bar","query":{"name":"new.oso.results.funders_collab.affiliated.byorganization","parameters":[0,"software"],"profile":"${this.profileName}"}},{"name":"> 1 funders","type":"bar","query":{"name":"new.oso.results.funders_collab.affiliated.byorganization","parameters":[1,"software"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Source software with multiple funders","align":"left","margin":50},"subtitle":{"text":"by organization (top organizations)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.softwareIndicatorsPaletteReversed.join('","')}\"]}}`));
    }
  }

  getSoftwareCoFundedProjectsGroupBy(contentSelection: string): void {
    this.softwareCoFundedProjectsGroupByView = contentSelection;
    if (contentSelection === 'country' && !this.softwareCoFundedProjectsByCountryChartURL) {
      this.softwareCoFundedProjectsByCountryChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"> 1 projects","type":"column","query":{"name":"new.oso.results.projects_collab.affiliated.bycountry.all","parameters":[1,"software"],"profile":"${this.profileName}"}},{"name":"{0,1} projects","type":"column","query":{"name":"new.oso.results.projects_collab.affiliated.bycountry.all","parameters":[0,"software"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Source software linked to multiple projects","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.softwareIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'datasource' && !this.softwareCoFundedProjectsByDatasourceChartURL) {
      this.softwareCoFundedProjectsByDatasourceChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"> 1 projects","type":"bar","query":{"name":"new.oso.results.projects_collab.affiliated.bydatasource","parameters":[1,"software"],"profile":"${this.profileName}"}},{"name":"{0,1} projects","type":"bar","query":{"name":"new.oso.results.projects_collab.affiliated.bydatasource","parameters":[0,"software"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Source software linked to multiple projects","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.softwareIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'organization' && !this.softwareCoFundedProjectsByOrganizationChartURL) {
      this.softwareCoFundedProjectsByOrganizationChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"> 1 projects","type":"bar","query":{"name":"new.oso.results.projects_collab.affiliated.byorganization","parameters":[1,"software"],"profile":"${this.profileName}"}},{"name":"{0,1} projects","type":"bar","query":{"name":"new.oso.results.projects_collab.affiliated.byorganization","parameters":[0,"software"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Source software linked to multiple projects","align":"left","margin":50},"subtitle":{"text":"by organization (top organizations)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.softwareIndicatorsPalette.join('","')}\"]}}`));
    }
  }

  getSoftwareCoFundedProjectsGroupByMobile(contentSelection: string): void {
    this.softwareCoFundedProjectsGroupByView = contentSelection;
    if (contentSelection === 'country' && !this.softwareCoFundedProjectsByCountryChartURLMobile) {
      this.softwareCoFundedProjectsByCountryChartURLMobile = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"> 1 projects","type":"bar","query":{"name":"new.oso.results.projects_collab.affiliated.bycountry","parameters":[1,"software"],"profile":"${this.profileName}"}},{"name":"{0,1} projects","type":"bar","query":{"name":"new.oso.results.projects_collab.affiliated.bycountry","parameters":[0,"software"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Source software linked to multiple projects","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.softwareIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'datasource' && !this.softwareCoFundedProjectsByDatasourceChartURL) {
      this.softwareCoFundedProjectsByDatasourceChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"> 1 projects","type":"bar","query":{"name":"new.oso.results.projects_collab.affiliated.bydatasource","parameters":[1,"software"],"profile":"${this.profileName}"}},{"name":"{0,1} projects","type":"bar","query":{"name":"new.oso.results.projects_collab.affiliated.bydatasource","parameters":[0,"software"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Source software linked to multiple projects","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.softwareIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'organization' && !this.softwareCoFundedProjectsByOrganizationChartURL) {
      this.softwareCoFundedProjectsByOrganizationChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"> 1 projects","type":"bar","query":{"name":"new.oso.results.projects_collab.affiliated.byorganization","parameters":[1,"software"],"profile":"${this.profileName}"}},{"name":"{0,1} projects","type":"bar","query":{"name":"new.oso.results.projects_collab.affiliated.byorganization","parameters":[0,"software"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Source software linked to multiple projects","align":"left","margin":50},"subtitle":{"text":"by organization (top organizations)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.softwareIndicatorsPalette.join('","')}\"]}}`));
    }
  }
}

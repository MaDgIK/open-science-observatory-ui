import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { ContinentCollaborationSoftwareComponent } from './continent-collaboration-software.component';
import { ContinentCollaborationSoftwareIndicatorsComponent } from './continent-collaboration-software-indicators.component';


const continentCollaborationSoftwareRoutes: Routes = [
  {
    path: '',
    component: ContinentCollaborationSoftwareComponent,
    children : [
      {
        path: '',
        redirectTo: 'co-funded',
        pathMatch: 'full',
      },
      {
        path: ':indicator',
        component: ContinentCollaborationSoftwareIndicatorsComponent,
      }
    ]
  },
];

@NgModule ({
  imports: [RouterModule.forChild(continentCollaborationSoftwareRoutes)],
  exports: [RouterModule]
})

export class ContinentCollaborationSoftwareRoutingModule {}

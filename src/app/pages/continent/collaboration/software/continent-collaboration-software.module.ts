import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReusableComponentsModule } from '../../../../shared/reusablecomponents/reusable-components.module';
import { DataService } from '../../../../services/data.service';
import { DataHandlerService } from '../../../../services/data-handler.service';
import { ContinentCollaborationSoftwareRoutingModule } from './continent-collaboration-software-routing.module';
import { ContinentCollaborationSoftwareComponent } from './continent-collaboration-software.component';
import { ContinentCollaborationSoftwareIndicatorsComponent } from './continent-collaboration-software-indicators.component';


@NgModule ({
  imports: [
    CommonModule,
    ContinentCollaborationSoftwareRoutingModule,
    ReusableComponentsModule,
  ],
  declarations: [
    ContinentCollaborationSoftwareComponent,
    ContinentCollaborationSoftwareIndicatorsComponent,
  ],
  providers: [
    DataService,
    DataHandlerService
  ],
})

export class ContinentCollaborationSoftwareModule {}

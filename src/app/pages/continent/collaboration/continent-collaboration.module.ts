import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReusableComponentsModule } from '../../../shared/reusablecomponents/reusable-components.module';
import { DataService } from '../../../services/data.service';
import { DataHandlerService } from '../../../services/data-handler.service';
import { ContinentCollaborationComponent } from './continent-collaboration.component';
import { ContinentCollaborationRoutingModule } from './continent-collaboration-routing.module';

@NgModule ({
  imports: [
    CommonModule,
    ContinentCollaborationRoutingModule,
    ReusableComponentsModule,
  ],
  declarations: [
    ContinentCollaborationComponent,
  ],
  providers: [
    DataService,
    DataHandlerService
  ],
})

export class ContinentCollaborationModule {}

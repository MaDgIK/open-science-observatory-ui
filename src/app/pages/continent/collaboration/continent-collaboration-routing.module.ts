import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { ContinentCollaborationComponent } from './continent-collaboration.component';


const continentCollaborationRoutes: Routes = [
  {
    path: '',
    component: ContinentCollaborationComponent,
    children : [
      {
        path: '',
        redirectTo: 'publications',
        pathMatch: 'full',
      },
      {
        path: 'publications',
        loadChildren: () => import('./publications/continent-collaboration-publications.module').then(m => m.ContinentCollaborationPublicationsModule),
      },
      {
        path: 'datasets',
        loadChildren: () => import('./datasets/continent-collaboration-datasets.module').then(m => m.ContinentCollaborationDatasetsModule),
      },
      {
        path: 'software',
        loadChildren: () => import('./software/continent-collaboration-software.module').then(m => m.ContinentCollaborationSoftwareModule),
      },
      {
        path: 'other-research-products',
        loadChildren: () => import('./other-research-products/continent-collaboration-orp.module').then(m => m.ContinentCollaborationORPModule),
      },
    ]
  },
];

@NgModule ({
  imports: [RouterModule.forChild(continentCollaborationRoutes)],
  exports: [RouterModule]
})

export class ContinentCollaborationRoutingModule {}

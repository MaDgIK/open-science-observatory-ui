import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { ContinentCollaborationORPComponent } from './continent-collaboration-orp.component';
import { ContinentCollaborationORPIndicatorsComponent } from './continent-collaboration-orp-indicators.component';


const continentCollaborationORPRoutes: Routes = [
  {
    path: '',
    component: ContinentCollaborationORPComponent,
    children : [
      {
        path: '',
        redirectTo: 'co-funded',
        pathMatch: 'full',
      },
      {
        path: ':indicator',
        component: ContinentCollaborationORPIndicatorsComponent,
      }
    ]
  },
];

@NgModule ({
  imports: [RouterModule.forChild(continentCollaborationORPRoutes)],
  exports: [RouterModule]
})

export class ContinentCollaborationORPRoutingModule {}

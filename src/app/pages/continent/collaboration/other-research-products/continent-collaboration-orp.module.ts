import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReusableComponentsModule } from '../../../../shared/reusablecomponents/reusable-components.module';
import { DataService } from '../../../../services/data.service';
import { DataHandlerService } from '../../../../services/data-handler.service';
import { ContinentCollaborationORPRoutingModule } from './continent-collaboration-orp-routing.module';
import { ContinentCollaborationORPComponent } from './continent-collaboration-orp.component';
import { ContinentCollaborationORPIndicatorsComponent } from './continent-collaboration-orp-indicators.component';


@NgModule ({
  imports: [
    CommonModule,
    ContinentCollaborationORPRoutingModule,
    ReusableComponentsModule,
  ],
  declarations: [
    ContinentCollaborationORPComponent,
    ContinentCollaborationORPIndicatorsComponent,
  ],
  providers: [
    DataService,
    DataHandlerService
  ],
})

export class ContinentCollaborationORPModule {}

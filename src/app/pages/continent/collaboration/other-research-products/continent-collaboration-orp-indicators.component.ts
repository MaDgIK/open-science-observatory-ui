import { Component, OnInit } from '@angular/core';
import {
  gradientStartColor,
  otherResearchProductsColor,
  otherResearchProductsPalette, otherIndicatorsPalette
} from '../../../../chart-palettes';
import {environment} from '../../../../../environments/environment';
import {CountryTableData} from '../../../../domain/overview-map-data';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';
import {TreemapHighchartsData} from '../../../../domain/treemap-highcharts-data';
import {DataHandlerService} from '../../../../services/data-handler.service';
import {DataService} from '../../../../services/data.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-continent-collaboration-orp-indicators',
  templateUrl: './continent-collaboration-orp-indicators.component.html',
})

export class ContinentCollaborationORPIndicatorsComponent implements OnInit {

  private chartsURL = environment.API_ENDPOINT + 'chart?json=';
  private profileName = environment.profileName;

  indicator: string;

  private otherIndicatorsPalette = otherIndicatorsPalette;
  otherColor = otherResearchProductsColor;

  otherCoFundedFundersTimeline: SafeResourceUrl;
  otherCoFundedProjectsTimeline: SafeResourceUrl;

  otherCoFundedFundersGroupByView = 'country';
  otherCoFundedFundersByCountryChartURL: SafeResourceUrl;
  otherCoFundedFundersByDatasourceChartURL: SafeResourceUrl;
  otherCoFundedFundersByOrganizationChartURL: SafeResourceUrl;
  otherCoFundedFundersByCountryChartURLMobile: SafeResourceUrl;

  otherCoFundedProjectsGroupByView = 'country';
  otherCoFundedProjectsByCountryChartURL: SafeResourceUrl;
  otherCoFundedProjectsByDatasourceChartURL: SafeResourceUrl;
  otherCoFundedProjectsByOrganizationChartURL: SafeResourceUrl;
  otherCoFundedProjectsByCountryChartURLMobile: SafeResourceUrl;

  otherCoFundedFundersByFunderData: TreemapHighchartsData[];
  otherCoFundedProjectsByFunderData: TreemapHighchartsData[];

  loadingOtherAbsoluteTable: boolean = true;
  loadingOtherPercentageTable: boolean = true;
  otherTableContentSelection: string = 'affiliated';
  otherAbsoluteTableData: CountryTableData[];
  otherPercentageTableData: CountryTableData[];

  constructor(private dataService: DataService,
              private dataHandlerService: DataHandlerService,
              private route: ActivatedRoute,
              private sanitizer: DomSanitizer) { }

  ngOnInit(): void {

    this.route.params.subscribe(params => {

      this.indicator = params['indicator'];
    });

    this.otherCoFundedFundersTimeline = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"> 1 funders","type":"column","query":{"name":"new.oso.results.funders_collab_timeline.affiliated","parameters":[1,"other"],"profile":"${this.profileName}"}},{"name":"{0,1} funders","type":"column","query":{"name":"new.oso.results.funders_collab_timeline.affiliated","parameters":[0,"other"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access ORPs with multiple funders","align":"left","margin":50},"subtitle":{"text":"over time","align":"left","margin":50},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherIndicatorsPalette.join('","')}\"]}}`));
    this.otherCoFundedProjectsTimeline = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"> 1 projects","type":"column","query":{"name":"new.oso.results.projects_collab_timeline.affiliated","parameters":[1,"other"],"profile":"${this.profileName}"}},{"name":"{0,1} projects","type":"column","query":{"name":"new.oso.results.projects_collab_timeline.affiliated","parameters":[0,"other"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access ORPs linked to multiple projects","align":"left","margin":50},"subtitle":{"text":"over time","align":"left","margin":50},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherIndicatorsPalette.join('","')}\"]}}`));

    this.otherCoFundedFundersByCountryChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"> 1 funders","type":"column","query":{"name":"new.oso.results.funders_collab.affiliated.bycountry.all","parameters":[1,"other"],"profile":"${this.profileName}"}},{"name":"{0,1} funders","type":"column","query":{"name":"new.oso.results.funders_collab.affiliated.bycountry.all","parameters":[0,"other"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access ORPs with multiple funders","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherIndicatorsPalette.join('","')}\"]}}`));
    this.otherCoFundedFundersByCountryChartURLMobile = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"> 1 funders","type":"bar","query":{"name":"new.oso.results.funders_collab.affiliated.bycountry","parameters":[1,"other"],"profile":"${this.profileName}"}},{"name":"{0,1} funders","type":"bar","query":{"name":"new.oso.results.funders_collab.affiliated.bycountry","parameters":[0,"other"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access ORPs with multiple funders","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherIndicatorsPalette.join('","')}\"]}}`));

    this.otherCoFundedProjectsByCountryChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"> 1 projects","type":"column","query":{"name":"new.oso.results.projects_collab.affiliated.bycountry.all","parameters":[1,"other"],"profile":"${this.profileName}"}},{"name":"{0,1} projects","type":"column","query":{"name":"new.oso.results.projects_collab.affiliated.bycountry.all","parameters":[0,"other"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access ORPs linked to multiple projects","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherIndicatorsPalette.join('","')}\"]}}`));
    this.otherCoFundedProjectsByCountryChartURLMobile = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"> 1 projects","type":"bar","query":{"name":"new.oso.results.projects_collab.affiliated.bycountry","parameters":[1,"other"],"profile":"${this.profileName}"}},{"name":"{0,1} projects","type":"bar","query":{"name":"new.oso.results.projects_collab.affiliated.bycountry","parameters":[0,"other"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access ORPs linked to multiple projects","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherIndicatorsPalette.join('","')}\"]}}`));


    this.dataService.getFundersResultsByTypeForCollaborationIndicatorForCountry('other', 'funders').subscribe(
      rawData => {
        this.otherCoFundedFundersByFunderData = this.dataHandlerService.convertRawDataToTreemapHighchartsData(rawData);
      }, error => {
        console.log(error);
      }
    );

    this.dataService.getFundersResultsByTypeForCollaborationIndicatorForCountry('other', 'projects').subscribe(
      rawData => {
        this.otherCoFundedProjectsByFunderData = this.dataHandlerService.convertRawDataToTreemapHighchartsData(rawData);
      }, error => {
        console.log(error);
      }
    );

    this.getOtherTableData(this.otherTableContentSelection);
  }

  getOtherTableData(contentSelection: string) {

    this.loadingOtherAbsoluteTable = true;
    this.loadingOtherPercentageTable = true;
    this.dataService.getCollaborationIndicatorsTableData('other', contentSelection).subscribe(
      rawData => {
        this.otherAbsoluteTableData = this.dataHandlerService.convertRawDataToIndicatorsTableData(rawData);
        this.otherPercentageTableData = this.dataHandlerService.convertRawDataToIndicatorsTableData(rawData);
        this.loadingOtherAbsoluteTable = false;
        this.loadingOtherPercentageTable = false;
      }, error => {
        console.log(error);
        this.loadingOtherAbsoluteTable = false;
        this.loadingOtherPercentageTable = false;
      }
    );
  }

  getContent(type: string, contentSelection: string): void {
    this.otherTableContentSelection = contentSelection;
    this.getOtherTableData(this.otherTableContentSelection);
  }

  getOtherCoFundedFundersGroupBy(contentSelection: string): void {
    this.otherCoFundedFundersGroupByView = contentSelection;
    if (contentSelection === 'country' && !this.otherCoFundedFundersByCountryChartURL) {
      this.otherCoFundedFundersByCountryChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"> 1 funders","type":"column","query":{"name":"new.oso.results.funders_collab.affiliated.bycountry.all","parameters":[1,"other"],"profile":"${this.profileName}"}},{"name":"{0,1} funders","type":"column","query":{"name":"new.oso.results.funders_collab.affiliated.bycountry.all","parameters":[0,"other"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access ORPs with multiple funders","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'datasource' && !this.otherCoFundedFundersByDatasourceChartURL) {
      this.otherCoFundedFundersByDatasourceChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"> 1 funders","type":"bar","query":{"name":"new.oso.results.funders_collab.affiliated.bydatasource","parameters":[1,"other"],"profile":"${this.profileName}"}},{"name":"{0,1} funders","type":"bar","query":{"name":"new.oso.results.funders_collab.affiliated.bydatasource","parameters":[0,"other"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access ORPs with multiple funders","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'organization' && !this.otherCoFundedFundersByOrganizationChartURL) {
      this.otherCoFundedFundersByOrganizationChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"> 1 funders","type":"bar","query":{"name":"new.oso.results.funders_collab.affiliated.byorganization","parameters":[1,"other"],"profile":"${this.profileName}"}},{"name":"{0,1} funders","type":"bar","query":{"name":"new.oso.results.funders_collab.affiliated.byorganization","parameters":[0,"other"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access ORPs with multiple funders","align":"left","margin":50},"subtitle":{"text":"by organization (top organizations)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherIndicatorsPalette.join('","')}\"]}}`));
    }
  }

  getOtherCoFundedFundersGroupByMobile(contentSelection: string): void {
    this.otherCoFundedFundersGroupByView = contentSelection;
    if (contentSelection === 'country' && !this.otherCoFundedFundersByCountryChartURLMobile) {
      this.otherCoFundedFundersByCountryChartURLMobile = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"> 1 funders","type":"bar","query":{"name":"new.oso.results.funders_collab.affiliated.bycountry","parameters":[1,"other"],"profile":"${this.profileName}"}},{"name":"{0,1} funders","type":"bar","query":{"name":"new.oso.results.funders_collab.affiliated.bycountry","parameters":[0,"other"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access ORPs with multiple funders","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'datasource' && !this.otherCoFundedFundersByDatasourceChartURL) {
      this.otherCoFundedFundersByDatasourceChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"> 1 funders","type":"bar","query":{"name":"new.oso.results.funders_collab.affiliated.bydatasource","parameters":[1,"other"],"profile":"${this.profileName}"}},{"name":"{0,1} funders","type":"bar","query":{"name":"new.oso.results.funders_collab.affiliated.bydatasource","parameters":[0,"other"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access ORPs with multiple funders","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'organization' && !this.otherCoFundedFundersByOrganizationChartURL) {
      this.otherCoFundedFundersByOrganizationChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"> 1 funders","type":"bar","query":{"name":"new.oso.results.funders_collab.affiliated.byorganization","parameters":[1,"other"],"profile":"${this.profileName}"}},{"name":"{0,1} funders","type":"bar","query":{"name":"new.oso.results.funders_collab.affiliated.byorganization","parameters":[0,"other"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access ORPs with multiple funders","align":"left","margin":50},"subtitle":{"text":"by organization (top organizations)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherIndicatorsPalette.join('","')}\"]}}`));
    }
  }

  getOtherCoFundedProjectsGroupBy(contentSelection: string): void {
    this.otherCoFundedProjectsGroupByView = contentSelection;
    if (contentSelection === 'country' && !this.otherCoFundedProjectsByCountryChartURL) {
      this.otherCoFundedProjectsByCountryChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"> 1 projects","type":"column","query":{"name":"new.oso.results.projects_collab.affiliated.bycountry.all","parameters":[1,"other"],"profile":"${this.profileName}"}},{"name":"{0,1} projects","type":"column","query":{"name":"new.oso.results.projects_collab.affiliated.bycountry.all","parameters":[0,"other"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access ORPs linked to multiple projects","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'datasource' && !this.otherCoFundedProjectsByDatasourceChartURL) {
      this.otherCoFundedProjectsByDatasourceChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"> 1 projects","type":"bar","query":{"name":"new.oso.results.projects_collab.affiliated.bydatasource","parameters":[1,"other"],"profile":"${this.profileName}"}},{"name":"{0,1} projects","type":"bar","query":{"name":"new.oso.results.projects_collab.affiliated.bydatasource","parameters":[0,"other"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access ORPs linked to multiple projects","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'organization' && !this.otherCoFundedProjectsByOrganizationChartURL) {
      this.otherCoFundedProjectsByOrganizationChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"> 1 projects","type":"bar","query":{"name":"new.oso.results.projects_collab.affiliated.byorganization","parameters":[1,"other"],"profile":"${this.profileName}"}},{"name":"{0,1} projects","type":"bar","query":{"name":"new.oso.results.projects_collab.affiliated.byorganization","parameters":[0,"other"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access ORPs linked to multiple projects","align":"left","margin":50},"subtitle":{"text":"by organization (top organizations)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherIndicatorsPalette.join('","')}\"]}}`));
    }
  }

  getOtherCoFundedProjectsGroupByMobile(contentSelection: string): void {
    this.otherCoFundedProjectsGroupByView = contentSelection;
    if (contentSelection === 'country' && !this.otherCoFundedProjectsByCountryChartURLMobile) {
      this.otherCoFundedProjectsByCountryChartURLMobile = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"> 1 projects","type":"bar","query":{"name":"new.oso.results.projects_collab.affiliated.bycountry","parameters":[1,"other"],"profile":"${this.profileName}"}},{"name":"{0,1} projects","type":"bar","query":{"name":"new.oso.results.projects_collab.affiliated.bycountry","parameters":[0,"other"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access ORPs linked to multiple projects","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'datasource' && !this.otherCoFundedProjectsByDatasourceChartURL) {
      this.otherCoFundedProjectsByDatasourceChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"> 1 projects","type":"bar","query":{"name":"new.oso.results.projects_collab.affiliated.bydatasource","parameters":[1,"other"],"profile":"${this.profileName}"}},{"name":"{0,1} projects","type":"bar","query":{"name":"new.oso.results.projects_collab.affiliated.bydatasource","parameters":[0,"other"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access ORPs linked to multiple projects","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'organization' && !this.otherCoFundedProjectsByOrganizationChartURL) {
      this.otherCoFundedProjectsByOrganizationChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"> 1 projects","type":"bar","query":{"name":"new.oso.results.projects_collab.affiliated.byorganization","parameters":[1,"other"],"profile":"${this.profileName}"}},{"name":"{0,1} projects","type":"bar","query":{"name":"new.oso.results.projects_collab.affiliated.byorganization","parameters":[0,"other"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access ORPs linked to multiple projects","align":"left","margin":50},"subtitle":{"text":"by organization (top organizations)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.otherIndicatorsPalette.join('","')}\"]}}`));
    }
  }
}

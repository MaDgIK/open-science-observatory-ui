import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReusableComponentsModule } from '../../../../shared/reusablecomponents/reusable-components.module';
import { DataService } from '../../../../services/data.service';
import { DataHandlerService } from '../../../../services/data-handler.service';
import { ContinentCollaborationPublicationsRoutingModule } from './continent-collaboration-publications-routing.module';
import { ContinentCollaborationPublicationsComponent } from './continent-collaboration-publications.component';
import { ContinentCollaborationPublicationsIndicatorsComponent } from './continent-collaboration-publications-indicators.component';


@NgModule ({
  imports: [
    CommonModule,
    ContinentCollaborationPublicationsRoutingModule,
    ReusableComponentsModule,
  ],
  declarations: [
    ContinentCollaborationPublicationsComponent,
    ContinentCollaborationPublicationsIndicatorsComponent,
  ],
  providers: [
    DataService,
    DataHandlerService
  ],
})

export class ContinentCollaborationPublicationsModule {}

import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { ContinentCollaborationPublicationsComponent } from './continent-collaboration-publications.component';
import { ContinentCollaborationPublicationsIndicatorsComponent } from './continent-collaboration-publications-indicators.component';

const continentCollaborationPublicationsRoutes: Routes = [
  {
    path: '',
    component: ContinentCollaborationPublicationsComponent,
    children : [
      {
        path: '',
        redirectTo: 'co-funded',
        pathMatch: 'full',
      },
      {
        path: ':indicator',
        component: ContinentCollaborationPublicationsIndicatorsComponent,
      }
    ]
  },
];

@NgModule ({
  imports: [RouterModule.forChild(continentCollaborationPublicationsRoutes)],
  exports: [RouterModule]
})

export class ContinentCollaborationPublicationsRoutingModule {}

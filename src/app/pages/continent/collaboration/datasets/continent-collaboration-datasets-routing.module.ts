import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { ContinentCollaborationDatasetsIndicatorsComponent } from './continent-collaboration-datasets-indicators.component';
import { ContinentCollaborationDatasetsComponent } from './continent-collaboration-datasets.component';

const continentCollaborationDatasetsRoutes: Routes = [
  {
    path: '',
    component: ContinentCollaborationDatasetsComponent,
    children : [
      {
        path: '',
        redirectTo: 'co-funded',
        pathMatch: 'full',
      },
      {
        path: ':indicator',
        component: ContinentCollaborationDatasetsIndicatorsComponent,
      }
    ]
  },
];

@NgModule ({
  imports: [RouterModule.forChild(continentCollaborationDatasetsRoutes)],
  exports: [RouterModule]
})

export class ContinentCollaborationDatasetsRoutingModule {}

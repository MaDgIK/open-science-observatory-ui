import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReusableComponentsModule } from '../../../../shared/reusablecomponents/reusable-components.module';
import { DataService } from '../../../../services/data.service';
import { DataHandlerService } from '../../../../services/data-handler.service';
import { ContinentCollaborationDatasetsRoutingModule } from './continent-collaboration-datasets-routing.module';
import { ContinentCollaborationDatasetsComponent } from './continent-collaboration-datasets.component';
import { ContinentCollaborationDatasetsIndicatorsComponent } from './continent-collaboration-datasets-indicators.component';


@NgModule ({
  imports: [
    CommonModule,
    ContinentCollaborationDatasetsRoutingModule,
    ReusableComponentsModule,
  ],
  declarations: [
    ContinentCollaborationDatasetsComponent,
    ContinentCollaborationDatasetsIndicatorsComponent,
  ],
  providers: [
    DataService,
    DataHandlerService
  ],
})

export class ContinentCollaborationDatasetsModule {}

import { Component, OnInit } from '@angular/core';
import {
  gradientStartColor,
  datasetColor,
  datasetPalette, datasetIndicatorsPalette
} from '../../../../chart-palettes';
import {environment} from '../../../../../environments/environment';
import {CountryTableData} from '../../../../domain/overview-map-data';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';
import {TreemapHighchartsData} from '../../../../domain/treemap-highcharts-data';
import {DataHandlerService} from '../../../../services/data-handler.service';
import {DataService} from '../../../../services/data.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-continent-collaboration-datasets-indicators',
  templateUrl: './continent-collaboration-datasets-indicators.component.html',
})

export class ContinentCollaborationDatasetsIndicatorsComponent implements OnInit {

  private chartsURL = environment.API_ENDPOINT + 'chart?json=';
  private profileName = environment.profileName;

  indicator: string;

  private datasetIndicatorsPalette = datasetIndicatorsPalette;
  datasetColor = datasetColor;

  datasetsCoFundedFundersTimeline: SafeResourceUrl;
  datasetsCoFundedProjectsTimeline: SafeResourceUrl;

  datasetsCoFundedFundersGroupByView = 'country';
  datasetsCoFundedFundersByCountryChartURL: SafeResourceUrl;
  datasetsCoFundedFundersByDatasourceChartURL: SafeResourceUrl;
  datasetsCoFundedFundersByOrganizationChartURL: SafeResourceUrl;
  datasetsCoFundedFundersByCountryChartURLMobile: SafeResourceUrl;

  datasetsCoFundedProjectsGroupByView = 'country';
  datasetsCoFundedProjectsByCountryChartURL: SafeResourceUrl;
  datasetsCoFundedProjectsByDatasourceChartURL: SafeResourceUrl;
  datasetsCoFundedProjectsByOrganizationChartURL: SafeResourceUrl;
  datasetsCoFundedProjectsByCountryChartURLMobile: SafeResourceUrl;

  datasetsCoFundedFundersByFunderData: TreemapHighchartsData[];
  datasetsCoFundedProjectsByFunderData: TreemapHighchartsData[];

  loadingDatasetsAbsoluteTable: boolean = true;
  loadingDatasetsPercentageTable: boolean = true;
  datasetsTableContentSelection: string = 'affiliated';
  datasetsAbsoluteTableData: CountryTableData[];
  datasetsPercentageTableData: CountryTableData[];

  constructor(private dataService: DataService,
              private dataHandlerService: DataHandlerService,
              private route: ActivatedRoute,
              private sanitizer: DomSanitizer) { }

  ngOnInit(): void {

    this.route.params.subscribe(params => {

      this.indicator = params['indicator'];
    });

    this.datasetsCoFundedFundersTimeline = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"> 1 funders","type":"column","query":{"name":"new.oso.results.funders_collab_timeline.affiliated","parameters":[1,"dataset"],"profile":"${this.profileName}"}},{"name":"{0,1} funders","type":"column","query":{"name":"new.oso.results.funders_collab_timeline.affiliated","parameters":[0,"dataset"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open access datasets with multiple funders","align":"left","margin":50},"subtitle":{"text":"over time","align":"left","margin":50},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetIndicatorsPalette.join('","')}\"]}}`));
    this.datasetsCoFundedProjectsTimeline = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"> 1 projects","type":"column","query":{"name":"new.oso.results.projects_collab_timeline.affiliated","parameters":[1,"dataset"],"profile":"${this.profileName}"}},{"name":"{0,1} projects","type":"column","query":{"name":"new.oso.results.projects_collab_timeline.affiliated","parameters":[0,"dataset"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets linked to multiple projects","align":"left","margin":50},"subtitle":{"text":"over time","align":"left","margin":50},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetIndicatorsPalette.join('","')}\"]}}`));

    this.datasetsCoFundedFundersByCountryChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"> 1 funders","type":"column","query":{"name":"new.oso.results.funders_collab.affiliated.bycountry.all","parameters":[1,"dataset"],"profile":"${this.profileName}"}},{"name":"{0,1} funders","type":"column","query":{"name":"new.oso.results.funders_collab.affiliated.bycountry.all","parameters":[0,"dataset"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open access datasets with multiple funders","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetIndicatorsPalette.join('","')}\"]}}`));
    this.datasetsCoFundedFundersByCountryChartURLMobile = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"> 1 funders","type":"bar","query":{"name":"new.oso.results.funders_collab.affiliated.bycountry","parameters":[1,"dataset"],"profile":"${this.profileName}"}},{"name":"{0,1} funders","type":"bar","query":{"name":"new.oso.results.funders_collab.affiliated.bycountry","parameters":[0,"dataset"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open access datasets with multiple funders","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetIndicatorsPalette.join('","')}\"]}}`));

    this.datasetsCoFundedProjectsByCountryChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"> 1 projects","type":"column","query":{"name":"new.oso.results.projects_collab.affiliated.bycountry.all","parameters":[1,"dataset"],"profile":"${this.profileName}"}},{"name":"{0,1} projects","type":"column","query":{"name":"new.oso.results.projects_collab.affiliated.bycountry.all","parameters":[0,"dataset"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets linked to multiple projects","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetIndicatorsPalette.join('","')}\"]}}`));
    this.datasetsCoFundedProjectsByCountryChartURLMobile = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"> 1 projects","type":"bar","query":{"name":"new.oso.results.projects_collab.affiliated.bycountry","parameters":[1,"dataset"],"profile":"${this.profileName}"}},{"name":"{0,1} projects","type":"bar","query":{"name":"new.oso.results.projects_collab.affiliated.bycountry","parameters":[0,"dataset"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets linked to multiple projects","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetIndicatorsPalette.join('","')}\"]}}`));


    this.dataService.getFundersResultsByTypeForCollaborationIndicatorForCountry('dataset', 'funders').subscribe(
      rawData => {
        this.datasetsCoFundedFundersByFunderData = this.dataHandlerService.convertRawDataToTreemapHighchartsData(rawData);
      }, error => {
        console.log(error);
      }
    );

    this.dataService.getFundersResultsByTypeForCollaborationIndicatorForCountry('dataset', 'projects').subscribe(
      rawData => {
        this.datasetsCoFundedProjectsByFunderData = this.dataHandlerService.convertRawDataToTreemapHighchartsData(rawData);
      }, error => {
        console.log(error);
      }
    );

    this.getDatasetsTableData(this.datasetsTableContentSelection);
  }

  getDatasetsTableData(contentSelection: string) {

    this.loadingDatasetsAbsoluteTable = true;
    this.loadingDatasetsPercentageTable = true;
    this.dataService.getCollaborationIndicatorsTableData('dataset', contentSelection).subscribe(
      rawData => {
        this.datasetsAbsoluteTableData = this.dataHandlerService.convertRawDataToIndicatorsTableData(rawData);
        this.datasetsPercentageTableData = this.dataHandlerService.convertRawDataToIndicatorsTableData(rawData);
        this.loadingDatasetsAbsoluteTable = false;
        this.loadingDatasetsPercentageTable = false;
      }, error => {
        console.log(error);
        this.loadingDatasetsAbsoluteTable = false;
        this.loadingDatasetsPercentageTable = false;
      }
    );
  }

  getContent(type: string, contentSelection: string): void {
    this.datasetsTableContentSelection = contentSelection;
    this.getDatasetsTableData(this.datasetsTableContentSelection);
  }

  getDatasetsCoFundedFundersGroupBy(contentSelection: string): void {
    this.datasetsCoFundedFundersGroupByView = contentSelection;
    if (contentSelection === 'country' && !this.datasetsCoFundedFundersByCountryChartURL) {
      this.datasetsCoFundedFundersByCountryChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"> 1 funders","type":"column","query":{"name":"new.oso.results.funders_collab.affiliated.bycountry.all","parameters":[1,"dataset"],"profile":"${this.profileName}"}},{"name":"{0,1} funders","type":"column","query":{"name":"new.oso.results.funders_collab.affiliated.bycountry.all","parameters":[0,"dataset"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open access datasets with multiple funders","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'datasource' && !this.datasetsCoFundedFundersByDatasourceChartURL) {
      this.datasetsCoFundedFundersByDatasourceChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"> 1 funders","type":"bar","query":{"name":"new.oso.results.funders_collab.affiliated.bydatasource","parameters":[1,"dataset"],"profile":"${this.profileName}"}},{"name":"{0,1} funders","type":"bar","query":{"name":"new.oso.results.funders_collab.affiliated.bydatasource","parameters":[0,"dataset"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open access datasets with multiple funders","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'organization' && !this.datasetsCoFundedFundersByOrganizationChartURL) {
      this.datasetsCoFundedFundersByOrganizationChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"> 1 funders","type":"bar","query":{"name":"new.oso.results.funders_collab.affiliated.byorganization","parameters":[1,"dataset"],"profile":"${this.profileName}"}},{"name":"{0,1} funders","type":"bar","query":{"name":"new.oso.results.funders_collab.affiliated.byorganization","parameters":[0,"dataset"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open access datasets with multiple funders","align":"left","margin":50},"subtitle":{"text":"by organization (top organizations)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetIndicatorsPalette.join('","')}\"]}}`));
    }
  }

  getDatasetsCoFundedFundersGroupByMobile(contentSelection: string): void {
    this.datasetsCoFundedFundersGroupByView = contentSelection;
    if (contentSelection === 'country' && !this.datasetsCoFundedFundersByCountryChartURLMobile) {
      this.datasetsCoFundedFundersByCountryChartURLMobile = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"> 1 funders","type":"bar","query":{"name":"new.oso.results.funders_collab.affiliated.bycountry","parameters":[1,"dataset"],"profile":"${this.profileName}"}},{"name":"{0,1} funders","type":"bar","query":{"name":"new.oso.results.funders_collab.affiliated.bycountry","parameters":[0,"dataset"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open access datasets with multiple funders","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'datasource' && !this.datasetsCoFundedFundersByDatasourceChartURL) {
      this.datasetsCoFundedFundersByDatasourceChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"> 1 funders","type":"bar","query":{"name":"new.oso.results.funders_collab.affiliated.bydatasource","parameters":[1,"dataset"],"profile":"${this.profileName}"}},{"name":"{0,1} funders","type":"bar","query":{"name":"new.oso.results.funders_collab.affiliated.bydatasource","parameters":[0,"dataset"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open access datasets with multiple funders","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'organization' && !this.datasetsCoFundedFundersByOrganizationChartURL) {
      this.datasetsCoFundedFundersByOrganizationChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"> 1 funders","type":"bar","query":{"name":"new.oso.results.funders_collab.affiliated.byorganization","parameters":[1,"dataset"],"profile":"${this.profileName}"}},{"name":"{0,1} funders","type":"bar","query":{"name":"new.oso.results.funders_collab.affiliated.byorganization","parameters":[0,"dataset"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open access datasets with multiple funders","align":"left","margin":50},"subtitle":{"text":"by organization (top organizations)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetIndicatorsPalette.join('","')}\"]}}`));
    }
  }

  getDatasetsCoFundedProjectsGroupBy(contentSelection: string): void {
    this.datasetsCoFundedProjectsGroupByView = contentSelection;
    if (contentSelection === 'country' && !this.datasetsCoFundedProjectsByCountryChartURL) {
      this.datasetsCoFundedProjectsByCountryChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"> 1 projects","type":"column","query":{"name":"new.oso.results.projects_collab.affiliated.bycountry.all","parameters":[1,"dataset"],"profile":"${this.profileName}"}},{"name":"{0,1} projects","type":"column","query":{"name":"new.oso.results.projects_collab.affiliated.bycountry.all","parameters":[0,"dataset"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets linked to multiple projects","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'datasource' && !this.datasetsCoFundedProjectsByDatasourceChartURL) {
      this.datasetsCoFundedProjectsByDatasourceChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"> 1 projects","type":"bar","query":{"name":"new.oso.results.projects_collab.affiliated.bydatasource","parameters":[1,"dataset"],"profile":"${this.profileName}"}},{"name":"{0,1} projects","type":"bar","query":{"name":"new.oso.results.projects_collab.affiliated.bydatasource","parameters":[0,"dataset"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets linked to multiple projects","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'organization' && !this.datasetsCoFundedProjectsByOrganizationChartURL) {
      this.datasetsCoFundedProjectsByOrganizationChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"> 1 projects","type":"bar","query":{"name":"new.oso.results.projects_collab.affiliated.byorganization","parameters":[1,"dataset"],"profile":"${this.profileName}"}},{"name":"{0,1} projects","type":"bar","query":{"name":"new.oso.results.projects_collab.affiliated.byorganization","parameters":[0,"dataset"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets linked to multiple projects","align":"left","margin":50},"subtitle":{"text":"by organization (top organizations)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetIndicatorsPalette.join('","')}\"]}}`));
    }
  }

  getDatasetsCoFundedProjectsGroupByMobile(contentSelection: string): void {
    this.datasetsCoFundedProjectsGroupByView = contentSelection;
    if (contentSelection === 'country' && !this.datasetsCoFundedProjectsByCountryChartURLMobile) {
      this.datasetsCoFundedProjectsByCountryChartURLMobile = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"> 1 projects","type":"bar","query":{"name":"new.oso.results.projects_collab.affiliated.bycountry","parameters":[1,"dataset"],"profile":"${this.profileName}"}},{"name":"{0,1} projects","type":"bar","query":{"name":"new.oso.results.projects_collab.affiliated.bycountry","parameters":[0,"dataset"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets linked to multiple projects","align":"left","margin":50},"subtitle":{"text":"by country","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'datasource' && !this.datasetsCoFundedProjectsByDatasourceChartURL) {
      this.datasetsCoFundedProjectsByDatasourceChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"> 1 projects","type":"bar","query":{"name":"new.oso.results.projects_collab.affiliated.bydatasource","parameters":[1,"dataset"],"profile":"${this.profileName}"}},{"name":"{0,1} projects","type":"bar","query":{"name":"new.oso.results.projects_collab.affiliated.bydatasource","parameters":[0,"dataset"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets linked to multiple projects","align":"left","margin":50},"subtitle":{"text":"by datasource (top datasources)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetIndicatorsPalette.join('","')}\"]}}`));
    } else if (contentSelection === 'organization' && !this.datasetsCoFundedProjectsByOrganizationChartURL) {
      this.datasetsCoFundedProjectsByOrganizationChartURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.chartsURL + encodeURIComponent(`{"library":"HighCharts","chartDescription":{"queries":[{"name":"> 1 projects","type":"bar","query":{"name":"new.oso.results.projects_collab.affiliated.byorganization","parameters":[1,"dataset"],"profile":"${this.profileName}"}},{"name":"{0,1} projects","type":"bar","query":{"name":"new.oso.results.projects_collab.affiliated.byorganization","parameters":[0,"dataset"],"profile":"${this.profileName}"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Open Access datasets linked to multiple projects","align":"left","margin":50},"subtitle":{"text":"by organization (top organizations)","align":"left"},"yAxis":{"title":{"text":""},"reversedStacks":false},"xAxis":{"title":{"text":""}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false},"stacking":"normal"}},"legend":{"enabled":true},"credits":{"href":null,"enabled":false},"colors":[\"${this.datasetIndicatorsPalette.join('","')}\"]}}`));
    }
  }
}

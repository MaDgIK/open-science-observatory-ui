import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReusableComponentsModule } from '../../shared/reusablecomponents/reusable-components.module';
import { ContinentRoutingModule } from './continent-routing.module';
import { ContinentComponent } from './continent.component';
import { ContinentOverviewComponent } from './overview/continent-overview.compopnent';
import { DataHandlerService } from '../../services/data-handler.service';
import { DataService } from '../../services/data.service';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';

@NgModule ({
  imports: [
    CommonModule,
    ContinentRoutingModule,
    ReusableComponentsModule,
    MatSlideToggleModule
  ],
  declarations: [
    ContinentComponent,
    ContinentOverviewComponent,
  ],
  providers: [
    DataService,
    DataHandlerService
  ],
})

export class ContinentModule {}

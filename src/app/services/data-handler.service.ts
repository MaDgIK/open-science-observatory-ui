import { Injectable } from '@angular/core';
import { RawData, Row } from '../domain/raw-data';
import {
  CountryOverviewData,
  CountryMapData,
  EuropeData,
  OverviewMapData,
  CountryTableData,
  CountryPageOverviewData, RnDExpenditure
} from '../domain/overview-map-data';
import {Indicator} from '../domain/overview-data';
import {TreemapHighchartsData} from '../domain/treemap-highcharts-data';

@Injectable ()
export class DataHandlerService {

  public convertRawDataToLastUpdateDate(rawData: RawData) {

    let lastUpdateDate: string;

    for (const series of rawData.datasets) {

      if (series.series.query.name === 'creation_date') {
        lastUpdateDate = series.series.result[0].row[0];
      }
    }

    return lastUpdateDate;
  }

  public convertRawMapDataToMapData(rawData: RawData) {

    const overviewMapData: OverviewMapData = new OverviewMapData();

    for (const series of rawData.datasets) {

      if (series.series.query.name === 'new.oso.publications.peer_reviewed.affiliated') {
        overviewMapData.publications = this.rawResultToCountryMapData(series.series.result);
      } else if (series.series.query.name === 'oso.results.affiliated' && series.series.query.parameters.includes('dataset')) {
        overviewMapData.datasets = this.rawResultToCountryMapData(series.series.result);
      } else if (series.series.query.name === 'oso.repositories.doar_re3data') {
        overviewMapData.repositories = this.rawResultToCountryMapData(series.series.result);
      } else if (series.series.query.name === 'oso.journals.doaj') {
        overviewMapData.journals = this.rawResultToCountryMapData(series.series.result);
      } else if (series.series.query.name === 'oso.oa_policies') {
        overviewMapData.policies = this.rawResultToCountryMapData(series.series.result);
      }
    }

    // console.log('Overview map data (data-handler)', overviewMapData);

    return overviewMapData;
  }

  public createJoinedPublicationsCountryMap(rawData: RawData) {

    const joinedPublicationsMap = new Map();

    for (const series of rawData.datasets) {
      if (series.series.query.name === 'new.oso.publications.peer_reviewed.affiliated') {

        for (const rowResult of series.series.result) {
          if (joinedPublicationsMap.has(rowResult.row[1])) {
            const deposited = joinedPublicationsMap.get(rowResult.row[1]).deposited;
            joinedPublicationsMap.delete(rowResult.row[1]);
            joinedPublicationsMap.set(rowResult.row[1], {countryName: rowResult.row[1], countryCode: rowResult.row[2],
              affiliated: Number(rowResult.row[0]), deposited: deposited});
          } else {
            joinedPublicationsMap.set(rowResult.row[1], {countryName: rowResult.row[1], countryCode: rowResult.row[2],
              affiliated: Number(rowResult.row[0]), deposited: 0});
          }
        }

      } else if (series.series.query.name === 'new.oso.publications.peer_reviewed.deposited') {

        for (const rowResult of series.series.result) {
          if (joinedPublicationsMap.has(rowResult.row[1])) {
            const affiliated = joinedPublicationsMap.get(rowResult.row[1]).affiliated;
            joinedPublicationsMap.delete(rowResult.row[1]);
            joinedPublicationsMap.set(rowResult.row[1], {countryName: rowResult.row[1], countryCode: rowResult.row[2],
              affiliated: affiliated, deposited: Number(rowResult.row[0])});
          }
          joinedPublicationsMap.set(rowResult.row[1], {countryName: rowResult.row[1], countryCode: rowResult.row[2],
            affiliated: 0, deposited: Number(rowResult.row[0])});
        }
      }
    }

    return joinedPublicationsMap;
  }

  public createJoinedDatasetsCountryMap(rawData: RawData) {

    const joinedDatasetsMap = new Map();

    for (const series of rawData.datasets) {
      if (series.series.query.name === 'oso.results.affiliated' && series.series.query.parameters.includes('dataset')) {

        for (const rowResult of series.series.result) {
          if (joinedDatasetsMap.has(rowResult.row[1])) {
            const deposited = joinedDatasetsMap.get(rowResult.row[1]).deposited;
            joinedDatasetsMap.delete(rowResult.row[1]);
            joinedDatasetsMap.set(rowResult.row[1], {countryName: rowResult.row[1], countryCode: rowResult.row[2],
              affiliated: Number(rowResult.row[0]), deposited: deposited});
          } else {
            joinedDatasetsMap.set(rowResult.row[1], {countryName: rowResult.row[1], countryCode: rowResult.row[2],
              affiliated: Number(rowResult.row[0]), deposited: 0});
          }
        }

      } else if (series.series.query.name === 'oso.results.deposited' && series.series.query.parameters.includes('dataset')) {

        for (const rowResult of series.series.result) {
          if (joinedDatasetsMap.has(rowResult.row[1])) {
            const affiliated = joinedDatasetsMap.get(rowResult.row[1]).affiliated;
            joinedDatasetsMap.delete(rowResult.row[1]);
            joinedDatasetsMap.set(rowResult.row[1], {countryName: rowResult.row[1], countryCode: rowResult.row[2],
              affiliated: affiliated, deposited: Number(rowResult.row[0])});
          }
          joinedDatasetsMap.set(rowResult.row[1], { countryName: rowResult.row[1], countryCode: rowResult.row[2],
            affiliated: 0, deposited: Number(rowResult.row[0])});
        }
      }
    }

    return joinedDatasetsMap;

  }

  public createJoinedRepositoriesCountryMap(rawData: RawData) {

    const joinedRepositoriesMap = new Map();

    for (const series of rawData.datasets) {
      if (series.series.query.name === 'oso.repositories.doar_re3data') {

        for (const rowResult of series.series.result) {
          if (joinedRepositoriesMap.has(rowResult.row[1])) {
            const simple = joinedRepositoriesMap.get(rowResult.row[1]).simple;
            joinedRepositoriesMap.delete(rowResult.row[1]);
            joinedRepositoriesMap.set(rowResult.row[1], {countryName: rowResult.row[1], countryCode: rowResult.row[2],
              doar_re3data: Number(rowResult.row[0]), simple: simple});
          } else {
            joinedRepositoriesMap.set(rowResult.row[1], {countryName: rowResult.row[1], countryCode: rowResult.row[2],
              doar_re3data: Number(rowResult.row[0]), simple: 0});
          }
        }

      } else if (series.series.query.name === 'oso.repositories') {

        for (const rowResult of series.series.result) {
          if (joinedRepositoriesMap.has(rowResult.row[1])) {
            const doar_re3data = joinedRepositoriesMap.get(rowResult.row[1]).doar_re3data;
            joinedRepositoriesMap.delete(rowResult.row[1]);
            joinedRepositoriesMap.set(rowResult.row[1], {countryName: rowResult.row[1], countryCode: rowResult.row[2],
              doar_re3data: doar_re3data, simple: Number(rowResult.row[0])});
          }
          joinedRepositoriesMap.set(rowResult.row[1], { countryName: rowResult.row[1], countryCode: rowResult.row[2],
            doar_re3data: 0, simple: Number(rowResult.row[0])});
        }
      }
    }

    return joinedRepositoriesMap;

  }

  public createJoinedJournalsCountryMap(rawData: RawData) {

    const joinedJournalsMap = new Map();

    for (const series of rawData.datasets) {
      if (series.series.query.name === 'oso.journals.doaj') {

        for (const rowResult of series.series.result) {
          if (joinedJournalsMap.has(rowResult.row[1])) {
            const simple = joinedJournalsMap.get(rowResult.row[1]).simple;
            joinedJournalsMap.delete(rowResult.row[1]);
            joinedJournalsMap.set(rowResult.row[1], {countryName: rowResult.row[1], countryCode: rowResult.row[2],
              doaj: Number(rowResult.row[0]), simple: simple});
          } else {
            joinedJournalsMap.set(rowResult.row[1], {countryName: rowResult.row[1], countryCode: rowResult.row[2],
              doaj: Number(rowResult.row[0]), simple: 0});
          }
        }

      } else if (series.series.query.name === 'oso.journals') {

        for (const rowResult of series.series.result) {
          if (joinedJournalsMap.has(rowResult.row[1])) {
            const doaj = joinedJournalsMap.get(rowResult.row[1]).doaj;
            joinedJournalsMap.delete(rowResult.row[1]);
            joinedJournalsMap.set(rowResult.row[1], {countryName: rowResult.row[1], countryCode: rowResult.row[2],
              doaj: doaj, simple: Number(rowResult.row[0])});
          }
          joinedJournalsMap.set(rowResult.row[1], { countryName: rowResult.row[1], countryCode: rowResult.row[2],
            doaj: 0, simple: Number(rowResult.row[0])});
        }
      }
    }

    return joinedJournalsMap;

  }

  public convertRawDataToCountryOverviewData(rawData: RawData) {

    const countryData: CountryOverviewData = new CountryOverviewData();

    for (const series of rawData.datasets) {

      if (series.series.query.name === 'new.oso.publications.peer_reviewed.affiliated.country') {
        countryData.publicationsAffiliated = Number(series.series.result[0].row[0]);
      } else if (series.series.query.name === 'new.oso.publications.peer_reviewed.deposited.country') {
        countryData.publicationsDeposited = Number(series.series.result[0].row[0]);
      } else if (series.series.query.name === 'oso.results.affiliated.country' && series.series.query.parameters.includes('dataset')) {
        countryData.datasetsAffiliated = Number(series.series.result[0].row[0]);
      } else if (series.series.query.name === 'oso.results.deposited.country' && series.series.query.parameters.includes('dataset')) {
        countryData.datasetsDeposited = Number(series.series.result[0].row[0]);
      } else if (series.series.query.name === 'oso.repositories.doar_re3data.country') {
        countryData.repositories = Number(series.series.result[0].row[0]);
      } else if (series.series.query.name === 'oso.journals.doaj.country') {
        countryData.journals = Number(series.series.result[0].row[0]);
      } else if (series.series.query.name === 'oso.oa_policies.country') {
        countryData.policies = Number(series.series.result[0].row[0]);
      }
    }

    return countryData;
  }

  public convertRawDataToEuropeOverviewData(rawData: RawData) {

    const europeData: EuropeData = new EuropeData();

    for (const series of rawData.datasets) {

      if (series.series.query.name === 'new.oso.publications.peer_reviewed.oa_percentage') {

        const publicationsIndicator: Indicator = new Indicator();
        publicationsIndicator.percentage = Number(series.series.result[0].row[0]);
        publicationsIndicator.oa = Number(series.series.result[0].row[1]);
        publicationsIndicator.total = Number(series.series.result[0].row[2]);
        europeData.publications = publicationsIndicator;

      } else if (series.series.query.name === 'oso.results.oa_percentage' && series.series.query.parameters.includes('dataset')) {

        const datasetsIndicator: Indicator = new Indicator();
        datasetsIndicator.percentage = Number(series.series.result[0].row[0]);
        datasetsIndicator.oa = Number(series.series.result[0].row[1]);
        datasetsIndicator.total = Number(series.series.result[0].row[2]);
        europeData.datasets = datasetsIndicator;

      } else if (series.series.query.name === 'oso.results.oa_percentage' && series.series.query.parameters.includes('software')) {

        const softwareIndicator: Indicator = new Indicator();
        softwareIndicator.percentage = Number(series.series.result[0].row[0]);
        softwareIndicator.oa = Number(series.series.result[0].row[1]);
        softwareIndicator.total = Number(series.series.result[0].row[2]);
        europeData.software = softwareIndicator;

      } else if (series.series.query.name === 'oso.results.oa_percentage' && series.series.query.parameters.includes('other')) {

        const otherIndicator: Indicator = new Indicator();
        otherIndicator.percentage = Number(series.series.result[0].row[0]);
        otherIndicator.oa = Number(series.series.result[0].row[1]);
        otherIndicator.total = Number(series.series.result[0].row[2]);
        europeData.other = otherIndicator;

      } else if (series.series.query.name === 'oso.repositories.doar_re3data.validated') {

        const repositoriesIndicator: Indicator = new Indicator();
        repositoriesIndicator.percentage = Number(series.series.result[0].row[0]);
        repositoriesIndicator.oa = Number(series.series.result[0].row[1]);
        repositoriesIndicator.total = Number(series.series.result[0].row[2]);
        europeData.repositories = repositoriesIndicator;

      } else if (series.series.query.name === 'oso.journals.doaj.validated') {

        const journalsIndicator: Indicator = new Indicator();
        journalsIndicator.percentage = Number(series.series.result[0].row[0]);
        journalsIndicator.oa = Number(series.series.result[0].row[1]);
        journalsIndicator.total = Number(series.series.result[0].row[2]);
        europeData.journals = journalsIndicator;

      } else if (series.series.query.name === 'oso.oa_policies.europe') {

        const policiesIndicator: Indicator = new Indicator();
        policiesIndicator.percentage = Number(series.series.result[0].row[0]);
        policiesIndicator.oa = Number(series.series.result[0].row[1]);
        policiesIndicator.total = Number(series.series.result[0].row[2]);
        europeData.policies = policiesIndicator;

      }
    }

    return europeData;
  }

  public convertRawDataToAbsoluteTableData(rawData: RawData) {

    const mapTableData: Map<string, CountryTableData> = new Map();

    for (const series of rawData.datasets) {

      if (series.series.query.name.includes('oso.results') && series.series.query.parameters.includes('publication')) {
        for (const rowResult of series.series.result) {

          // remove unwanted countries
          if (this.isCountryToBeRemoved(rowResult.row[2])) {
            continue;
          }

          if (mapTableData.has(rowResult.row[2])) {
            const countryTableData = mapTableData.get(rowResult.row[2]);
            countryTableData.publications = Number(rowResult.row[0]);
          } else {
            const countryTableData: CountryTableData = new CountryTableData();
            countryTableData.publications = Number(rowResult.row[0]);
            countryTableData.name = rowResult.row[1];
            countryTableData.code = rowResult.row[2];
            mapTableData.set(rowResult.row[2], countryTableData);
          }
        }

      } else if (series.series.query.name.includes('oso.results') && series.series.query.parameters.includes('dataset')) {

        for (const rowResult of series.series.result) {

          // remove unwanted countries
          if (this.isCountryToBeRemoved(rowResult.row[2])) {
            continue;
          }

          if (mapTableData.has(rowResult.row[2])) {
            const countryTableData = mapTableData.get(rowResult.row[2]);
            countryTableData.datasets = Number(rowResult.row[0]);
          } else {
            const countryTableData: CountryTableData = new CountryTableData();
            countryTableData.datasets = Number(rowResult.row[0]);
            countryTableData.name = rowResult.row[1];
            countryTableData.code = rowResult.row[2];
            mapTableData.set(rowResult.row[2], countryTableData);
          }
        }
      } else if (series.series.query.name.includes('oso.results') && series.series.query.parameters.includes('software')) {

        for (const rowResult of series.series.result) {

          // remove unwanted countries
          if (this.isCountryToBeRemoved(rowResult.row[2])) {
            continue;
          }

          if (mapTableData.has(rowResult.row[2])) {
            const countryTableData = mapTableData.get(rowResult.row[2]);
            countryTableData.software = Number(rowResult.row[0]);
          } else {
            const countryTableData: CountryTableData = new CountryTableData();
            countryTableData.software = Number(rowResult.row[0]);
            countryTableData.name = rowResult.row[1];
            countryTableData.code = rowResult.row[2];
            mapTableData.set(rowResult.row[2], countryTableData);
          }
        }
      } else if (series.series.query.name.includes('oso.results') && series.series.query.parameters.includes('other')) {

        for (const rowResult of series.series.result) {

          // remove unwanted countries
          if (this.isCountryToBeRemoved(rowResult.row[2])) {
            continue;
          }

          if (mapTableData.has(rowResult.row[2])) {
            const countryTableData = mapTableData.get(rowResult.row[2]);
            countryTableData.other = Number(rowResult.row[0]);
          } else {
            const countryTableData: CountryTableData = new CountryTableData();
            countryTableData.other = Number(rowResult.row[0]);
            countryTableData.name = rowResult.row[1];
            countryTableData.code = rowResult.row[2];
            mapTableData.set(rowResult.row[2], countryTableData);
          }
        }
      } else if (series.series.query.name.includes('oso.repositories')) {

        for (const rowResult of series.series.result) {

          // remove unwanted countries
          if (this.isCountryToBeRemoved(rowResult.row[2])) {
            continue;
          }

          if (mapTableData.has(rowResult.row[2])) {
            const countryTableData = mapTableData.get(rowResult.row[2]);
            countryTableData.repositories = Number(rowResult.row[0]);
          } else {
            const countryTableData: CountryTableData = new CountryTableData();
            countryTableData.repositories = Number(rowResult.row[0]);
            countryTableData.name = rowResult.row[1];
            countryTableData.code = rowResult.row[2];
            mapTableData.set(rowResult.row[2], countryTableData);
          }
        }
      } else if (series.series.query.name.includes('oso.journals')) {

        for (const rowResult of series.series.result) {

          // remove unwanted countries
          if (this.isCountryToBeRemoved(rowResult.row[2])) {
            continue;
          }

          if (mapTableData.has(rowResult.row[2])) {
            const countryTableData = mapTableData.get(rowResult.row[2]);
            countryTableData.journals = Number(rowResult.row[0]);
          } else {
            const countryTableData: CountryTableData = new CountryTableData();
            countryTableData.journals = Number(rowResult.row[0]);
            countryTableData.name = rowResult.row[1];
            countryTableData.code = rowResult.row[2];
            mapTableData.set(rowResult.row[2], countryTableData);
          }
        }
      }
    }

    const tableData: CountryTableData[] = [];

    mapTableData.forEach((value: CountryTableData, key: string) => {
      // console.log(key, value);
      tableData.push(value);
    });

    return tableData;
  }

  public convertRawDataToPercentageTableData(rawData: RawData) {

    const mapTableData: Map<string, CountryTableData> = new Map();

    for (const series of rawData.datasets) {

      if (series.series.query.name.includes('oso.results')
        && series.series.query.parameters && series.series.query.parameters[0] === 'publication') {
        for (const rowResult of series.series.result) {

          // remove unwanted countries
          if (this.isCountryToBeRemoved(rowResult.row[4])) {
            continue;
          }

          if (mapTableData.has(rowResult.row[4])) {
            const countryTableData = mapTableData.get(rowResult.row[4]);
            if (rowResult.row[0] !== 'NaN') {
              countryTableData.publications = Number(rowResult.row[0]);
            }
          } else {
            const countryTableData: CountryTableData = new CountryTableData();
            if (rowResult.row[0] !== 'NaN') {
              countryTableData.publications = Number(rowResult.row[0]);
            }
            countryTableData.name = rowResult.row[3];
            countryTableData.code = rowResult.row[4];
            mapTableData.set(rowResult.row[4], countryTableData);
          }
        }

      } else if (series.series.query.name.includes('oso.results')
        && series.series.query.parameters && series.series.query.parameters[0] === 'dataset') {

        for (const rowResult of series.series.result) {

          // remove unwanted countries
          if (this.isCountryToBeRemoved(rowResult.row[4])) {
            continue;
          }

          if (mapTableData.has(rowResult.row[4])) {
            const countryTableData = mapTableData.get(rowResult.row[4]);
            if (rowResult.row[0] !== 'NaN') {
              countryTableData.datasets = Number(rowResult.row[0]);
            }
          } else {
            const countryTableData: CountryTableData = new CountryTableData();
            if (rowResult.row[0] !== 'NaN') {
              countryTableData.datasets = Number(rowResult.row[0]);
            }
            countryTableData.name = rowResult.row[3];
            countryTableData.code = rowResult.row[4];
            mapTableData.set(rowResult.row[4], countryTableData);
          }
        }
      } else if (series.series.query.name.includes('oso.results')
        && series.series.query.parameters && series.series.query.parameters[0] === 'software') {

        for (const rowResult of series.series.result) {

          // remove unwanted countries
          if (this.isCountryToBeRemoved(rowResult.row[4])) {
            continue;
          }

          if (mapTableData.has(rowResult.row[4])) {
            const countryTableData = mapTableData.get(rowResult.row[4]);
            if (rowResult.row[0] !== 'NaN') {
              countryTableData.software = Number(rowResult.row[0]);
            }
          } else {
            const countryTableData: CountryTableData = new CountryTableData();
            if (rowResult.row[0] !== 'NaN') {
              countryTableData.software = Number(rowResult.row[0]);
            }
            countryTableData.name = rowResult.row[3];
            countryTableData.code = rowResult.row[4];
            mapTableData.set(rowResult.row[4], countryTableData);
          }
        }
      } else if (series.series.query.name.includes('oso.results')
        && series.series.query.parameters && series.series.query.parameters[0] === 'other') {

        for (const rowResult of series.series.result) {

          // remove unwanted countries
          if (this.isCountryToBeRemoved(rowResult.row[4])) {
            continue;
          }

          if (mapTableData.has(rowResult.row[4])) {
            const countryTableData = mapTableData.get(rowResult.row[4]);
            if (rowResult.row[0] !== 'NaN') {
              countryTableData.other = Number(rowResult.row[0]);
            }
          } else {
            const countryTableData: CountryTableData = new CountryTableData();
            if (rowResult.row[0] !== 'NaN') {
              countryTableData.other = Number(rowResult.row[0]);
            }
            countryTableData.name = rowResult.row[3];
            countryTableData.code = rowResult.row[4];
            mapTableData.set(rowResult.row[4], countryTableData);
          }
        }
      } else if (series.series.query.name.includes('oso.repositories')) {

        for (const rowResult of series.series.result) {

          // remove unwanted countries
          if (this.isCountryToBeRemoved(rowResult.row[4])) {
            continue;
          }

          if (mapTableData.has(rowResult.row[4])) {
            const countryTableData = mapTableData.get(rowResult.row[4]);
            countryTableData.repositories = Number(rowResult.row[0]);
          } else {
            const countryTableData: CountryTableData = new CountryTableData();
            countryTableData.repositories = Number(rowResult.row[0]);
            countryTableData.name = rowResult.row[3];
            countryTableData.code = rowResult.row[4];
            mapTableData.set(rowResult.row[4], countryTableData);
          }
        }
      } else if (series.series.query.name.includes('oso.journals')) {

        for (const rowResult of series.series.result) {

          // remove unwanted countries
          if (this.isCountryToBeRemoved(rowResult.row[4])) {
            continue;
          }

          if (mapTableData.has(rowResult.row[4])) {
            const countryTableData = mapTableData.get(rowResult.row[4]);
            countryTableData.journals = Number(rowResult.row[0]);
          } else {
            const countryTableData: CountryTableData = new CountryTableData();
            countryTableData.journals = Number(rowResult.row[0]);
            countryTableData.name = rowResult.row[3];
            countryTableData.code = rowResult.row[4];
            mapTableData.set(rowResult.row[4], countryTableData);
          }
        }
      }
    }

    const tableData: CountryTableData[] = [];

    mapTableData.forEach((value: CountryTableData, key: string) => {
      // console.log(key, value);
      tableData.push(value);
    });

    return tableData;
  }

  public convertRawDataToIndicatorsTableData(rawData: RawData) {

    const mapTableData: Map<string, CountryTableData> = new Map();

    for (const series of rawData.datasets) {

      if (series.series.query.name.includes('oso.results.pid_percentage')) {
        for (const rowResult of series.series.result) {

          const pidIndicator = new Indicator();

          // remove unwanted countries
          if (this.isCountryToBeRemoved(rowResult.row[3])) {
            continue;
          }

          if (mapTableData.has(rowResult.row[3])) {
            const countryTableData = mapTableData.get(rowResult.row[3]);
            pidIndicator.total = Number(rowResult.row[2]);
            pidIndicator.oa = Number(rowResult.row[1]);
            if (rowResult.row[0] !== 'NaN') {
              pidIndicator.percentage = Number(rowResult.row[0]);
            }
            countryTableData.pid = pidIndicator;
          } else {
            const countryTableData: CountryTableData = new CountryTableData();
            pidIndicator.total = Number(rowResult.row[2]);
            pidIndicator.oa = Number(rowResult.row[1]);
            if (rowResult.row[0] !== 'NaN') {
              pidIndicator.percentage = Number(rowResult.row[0]);
            }
            countryTableData.pid = pidIndicator;
            countryTableData.name = rowResult.row[4];
            countryTableData.code = rowResult.row[3];
            mapTableData.set(rowResult.row[3], countryTableData);
          }
        }

      } else if (series.series.query.name.includes('oso.results.licence_percentage')) {

        for (const rowResult of series.series.result) {

          const licenceIndicator = new Indicator();

          // remove unwanted countries
          if (this.isCountryToBeRemoved(rowResult.row[3])) {
            continue;
          }

          if (mapTableData.has(rowResult.row[3])) {
            const countryTableData = mapTableData.get(rowResult.row[3]);
            licenceIndicator.total = Number(rowResult.row[2]);
            licenceIndicator.oa = Number(rowResult.row[1]);
            if (rowResult.row[0] !== 'NaN') {
              licenceIndicator.percentage = Number(rowResult.row[0]);
            }
            countryTableData.licence = licenceIndicator;
          } else {
            const countryTableData: CountryTableData = new CountryTableData();
            licenceIndicator.total = Number(rowResult.row[2]);
            licenceIndicator.oa = Number(rowResult.row[1]);
            if (rowResult.row[0] !== 'NaN') {
              licenceIndicator.percentage = Number(rowResult.row[0]);
            }
            countryTableData.licence = licenceIndicator;
            countryTableData.name = rowResult.row[4];
            countryTableData.code = rowResult.row[3];
            mapTableData.set(rowResult.row[3], countryTableData);
          }
        }
      } else if (series.series.query.name.includes('oso.results.green_percentage')) {

        for (const rowResult of series.series.result) {

          const greenIndicator = new Indicator();

          // remove unwanted countries
          if (this.isCountryToBeRemoved(rowResult.row[3])) {
            continue;
          }

          if (mapTableData.has(rowResult.row[3])) {
            const countryTableData = mapTableData.get(rowResult.row[3]);
            greenIndicator.total = Number(rowResult.row[2]);
            greenIndicator.oa = Number(rowResult.row[1]);
            if (rowResult.row[0] !== 'NaN') {
              greenIndicator.percentage = Number(rowResult.row[0]);
            }
            countryTableData.green = greenIndicator;
          } else {
            const countryTableData: CountryTableData = new CountryTableData();
            greenIndicator.total = Number(rowResult.row[2]);
            greenIndicator.oa = Number(rowResult.row[1]);
            if (rowResult.row[0] !== 'NaN') {
              greenIndicator.percentage = Number(rowResult.row[0]);
            }
            countryTableData.green = greenIndicator;
            countryTableData.name = rowResult.row[4];
            countryTableData.code = rowResult.row[3];
            mapTableData.set(rowResult.row[3], countryTableData);
          }
        }
      } else if (series.series.query.name.includes('oso.results.gold_percentage')) {

        for (const rowResult of series.series.result) {

          const goldIndicator = new Indicator();

          // remove unwanted countries
          if (this.isCountryToBeRemoved(rowResult.row[3])) {
            continue;
          }

          if (mapTableData.has(rowResult.row[3])) {
            const countryTableData = mapTableData.get(rowResult.row[3]);
            goldIndicator.total = Number(rowResult.row[2]);
            goldIndicator.oa = Number(rowResult.row[1]);
            if (rowResult.row[0] !== 'NaN') {
              goldIndicator.percentage = Number(rowResult.row[0]);
            }
            countryTableData.gold = goldIndicator;
          } else {
            const countryTableData: CountryTableData = new CountryTableData();
            goldIndicator.total = Number(rowResult.row[2]);
            goldIndicator.oa = Number(rowResult.row[1]);
            if (rowResult.row[0] !== 'NaN') {
              goldIndicator.percentage = Number(rowResult.row[0]);
            }
            countryTableData.gold = goldIndicator;
            countryTableData.name = rowResult.row[4];
            countryTableData.code = rowResult.row[3];
            mapTableData.set(rowResult.row[3], countryTableData);
          }
        }
      } else if (series.series.query.name.includes('oso.results.cc_licence_percentage')) {

        for (const rowResult of series.series.result) {

          const ccLicenceIndicator = new Indicator();

          // remove unwanted countries
          if (this.isCountryToBeRemoved(rowResult.row[3])) {
            continue;
          }

          if (mapTableData.has(rowResult.row[3])) {
            const countryTableData = mapTableData.get(rowResult.row[3]);
            ccLicenceIndicator.total = Number(rowResult.row[2]);
            ccLicenceIndicator.oa = Number(rowResult.row[1]);
            if (rowResult.row[0] !== 'NaN') {
              ccLicenceIndicator.percentage = Number(rowResult.row[0]);
            }
            countryTableData.cc_licence = ccLicenceIndicator;
          } else {
            const countryTableData: CountryTableData = new CountryTableData();
            ccLicenceIndicator.total = Number(rowResult.row[2]);
            ccLicenceIndicator.oa = Number(rowResult.row[1]);
            if (rowResult.row[0] !== 'NaN') {
              ccLicenceIndicator.percentage = Number(rowResult.row[0]);
            }
            countryTableData.cc_licence = ccLicenceIndicator;
            countryTableData.name = rowResult.row[4];
            countryTableData.code = rowResult.row[3];
            mapTableData.set(rowResult.row[3], countryTableData);
          }
        }
      } else if (series.series.query.name.includes('oso.results.abstract_percentage')) {

        for (const rowResult of series.series.result) {

          const abstractIndicator = new Indicator();

          // remove unwanted countries
          if (this.isCountryToBeRemoved(rowResult.row[3])) {
            continue;
          }

          if (mapTableData.has(rowResult.row[3])) {
            const countryTableData = mapTableData.get(rowResult.row[3]);
            abstractIndicator.total = Number(rowResult.row[2]);
            abstractIndicator.oa = Number(rowResult.row[1]);
            if (rowResult.row[0] !== 'NaN') {
              abstractIndicator.percentage = Number(rowResult.row[0]);
            }
            countryTableData.abstract = abstractIndicator;
          } else {
            const countryTableData: CountryTableData = new CountryTableData();
            abstractIndicator.total = Number(rowResult.row[2]);
            abstractIndicator.oa = Number(rowResult.row[1]);
            if (rowResult.row[0] !== 'NaN') {
              abstractIndicator.percentage = Number(rowResult.row[0]);
            }
            countryTableData.abstract = abstractIndicator;
            countryTableData.name = rowResult.row[4];
            countryTableData.code = rowResult.row[3];
            mapTableData.set(rowResult.row[3], countryTableData);
          }
        }
      } else if (series.series.query.name.includes('new.oso.results.funders_collab_percentage')) {

        for (const rowResult of series.series.result) {

          const fundersCollabIndicator = new Indicator();

          // remove unwanted countries
          if (this.isCountryToBeRemoved(rowResult.row[3])) {
            continue;
          }

          if (mapTableData.has(rowResult.row[3])) {
            const countryTableData = mapTableData.get(rowResult.row[3]);
            fundersCollabIndicator.total = Number(rowResult.row[2]);
            fundersCollabIndicator.oa = Number(rowResult.row[1]);
            if (rowResult.row[0] !== 'NaN') {
              fundersCollabIndicator.percentage = Number(rowResult.row[0]);
            }
            countryTableData.funders_collab = fundersCollabIndicator;
          } else {
            const countryTableData: CountryTableData = new CountryTableData();
            fundersCollabIndicator.total = Number(rowResult.row[2]);
            fundersCollabIndicator.oa = Number(rowResult.row[1]);
            if (rowResult.row[0] !== 'NaN') {
              fundersCollabIndicator.percentage = Number(rowResult.row[0]);
            }
            countryTableData.funders_collab = fundersCollabIndicator;
            countryTableData.name = rowResult.row[4];
            countryTableData.code = rowResult.row[3];
            mapTableData.set(rowResult.row[3], countryTableData);
          }
        }
      } else if (series.series.query.name.includes('new.oso.results.projects_collab_percentage')) {

        for (const rowResult of series.series.result) {

          const projectsCollabIndicator = new Indicator();

          // remove unwanted countries
          if (this.isCountryToBeRemoved(rowResult.row[3])) {
            continue;
          }

          if (mapTableData.has(rowResult.row[3])) {
            const countryTableData = mapTableData.get(rowResult.row[3]);
            projectsCollabIndicator.total = Number(rowResult.row[2]);
            projectsCollabIndicator.oa = Number(rowResult.row[1]);
            if (rowResult.row[0] !== 'NaN') {
              projectsCollabIndicator.percentage = Number(rowResult.row[0]);
            }
            countryTableData.projects_collab = projectsCollabIndicator;
          } else {
            const countryTableData: CountryTableData = new CountryTableData();
            projectsCollabIndicator.total = Number(rowResult.row[2]);
            projectsCollabIndicator.oa = Number(rowResult.row[1]);
            if (rowResult.row[0] !== 'NaN') {
              projectsCollabIndicator.percentage = Number(rowResult.row[0]);
            }
            countryTableData.projects_collab = projectsCollabIndicator;
            countryTableData.name = rowResult.row[4];
            countryTableData.code = rowResult.row[3];
            mapTableData.set(rowResult.row[3], countryTableData);
          }
        }
      } else if (series.series.query.name.includes('new.oso.results.authors_collab_percentage')) {

        for (const rowResult of series.series.result) {

          const authorsCollabIndicator = new Indicator();

          // remove unwanted countries
          if (this.isCountryToBeRemoved(rowResult.row[3])) {
            continue;
          }

          if (mapTableData.has(rowResult.row[3])) {
            const countryTableData = mapTableData.get(rowResult.row[3]);
            authorsCollabIndicator.total = Number(rowResult.row[2]);
            authorsCollabIndicator.oa = Number(rowResult.row[1]);
            if (rowResult.row[0] !== 'NaN') {
              authorsCollabIndicator.percentage = Number(rowResult.row[0]);
            }
            countryTableData.authors_collab = authorsCollabIndicator;
          } else {
            const countryTableData: CountryTableData = new CountryTableData();
            authorsCollabIndicator.total = Number(rowResult.row[2]);
            authorsCollabIndicator.oa = Number(rowResult.row[1]);
            if (rowResult.row[0] !== 'NaN') {
              authorsCollabIndicator.percentage = Number(rowResult.row[0]);
            }
            countryTableData.authors_collab = authorsCollabIndicator;
            countryTableData.name = rowResult.row[4];
            countryTableData.code = rowResult.row[3];
            mapTableData.set(rowResult.row[3], countryTableData);
          }
        }
      }
    }

    const tableData: CountryTableData[] = [];

    mapTableData.forEach((value: CountryTableData, key: string) => {
      // console.log(key, value);
      tableData.push(value);
    });

    return tableData;
  }

  public convertRawDataToLeadingOpenScienceData(rawData: RawData) {

    const leadingOpenScienceData: CountryOverviewData[] = [];

    for (const series of rawData.datasets) {
      if (series.series.query.name === 'new.oso.mobile.overview') {
        for (const rowResult of series.series.result) {

          const countryOverviewData: CountryOverviewData = new CountryOverviewData();
          countryOverviewData.name = rowResult.row[1];
          countryOverviewData.code = rowResult.row[0];
          countryOverviewData.publicationsAffiliated = Number(rowResult.row[2]);
          countryOverviewData.publicationsDeposited = Number(rowResult.row[3]);

          leadingOpenScienceData.push(countryOverviewData);
        }
      }
    }

    return leadingOpenScienceData;
  }

  public convertRawDataToCountryPageOverviewData(rawData: RawData) {

    const countryPageOverviewData: CountryPageOverviewData = new CountryPageOverviewData();

    for (const series of rawData.datasets) {

      if ((series.series.query.name === 'new.oso.publications.peer_reviewed.oa_percentage.country')
        && series.series.result && series.series.result.length > 0 && series.series.result[0].row) {

        countryPageOverviewData.name = series.series.result[0].row[3];
        countryPageOverviewData.code = series.series.result[0].row[4];

        const publicationsAffiliatedIndicator: Indicator = new Indicator();
        publicationsAffiliatedIndicator.percentage = Number(series.series.result[0].row[0]);
        publicationsAffiliatedIndicator.oa = Number(series.series.result[0].row[1]);
        publicationsAffiliatedIndicator.total = Number(series.series.result[0].row[2]);
        countryPageOverviewData.publicationsAffiliated = publicationsAffiliatedIndicator;

      } else if ((series.series.query.name === 'new.oso.publications.peer_reviewed.oa_percentage.deposited.country')
        && series.series.result && series.series.result.length > 0 && series.series.result[0].row) {

        countryPageOverviewData.name = series.series.result[0].row[3];
        countryPageOverviewData.code = series.series.result[0].row[4];

        const publicationsDepositedIndicator: Indicator = new Indicator();
        publicationsDepositedIndicator.percentage = Number(series.series.result[0].row[0]);
        publicationsDepositedIndicator.oa = Number(series.series.result[0].row[1]);
        publicationsDepositedIndicator.total = Number(series.series.result[0].row[2]);
        countryPageOverviewData.publicationsDeposited = publicationsDepositedIndicator;

      } else if ((series.series.query.name === 'oso.results.oa_percentage.country')
        && series.series.query.parameters && series.series.query.parameters[1] === 'dataset'
        && series.series.result && series.series.result.length > 0 && series.series.result[0].row) {

        countryPageOverviewData.name = series.series.result[0].row[3];
        countryPageOverviewData.code = series.series.result[0].row[4];

        const datasetsAffiliatedIndicator: Indicator = new Indicator();
        datasetsAffiliatedIndicator.percentage = Number(series.series.result[0].row[0]);
        datasetsAffiliatedIndicator.oa = Number(series.series.result[0].row[1]);
        datasetsAffiliatedIndicator.total = Number(series.series.result[0].row[2]);
        countryPageOverviewData.datasetsAffiliated = datasetsAffiliatedIndicator;

      } else if ((series.series.query.name === 'oso.results.oa_percentage.deposited.country')
        && series.series.query.parameters && series.series.query.parameters[0] === 'dataset'
        && series.series.result && series.series.result.length > 0 && series.series.result[0].row) {

        countryPageOverviewData.name = series.series.result[0].row[3];
        countryPageOverviewData.code = series.series.result[0].row[4];

        const datasetsDepositedIndicator: Indicator = new Indicator();
        datasetsDepositedIndicator.percentage = Number(series.series.result[0].row[0]);
        datasetsDepositedIndicator.oa = Number(series.series.result[0].row[1]);
        datasetsDepositedIndicator.total = Number(series.series.result[0].row[2]);
        countryPageOverviewData.datasetsDeposited = datasetsDepositedIndicator;

      } else if (series.series.query.name === 'oso.repositories.doar_re3data.validated.country'
        && series.series.result && series.series.result.length > 0 && series.series.result[0].row) {

        countryPageOverviewData.name = series.series.result[0].row[3];
        countryPageOverviewData.code = series.series.result[0].row[4];

        const repositoriesIndicator: Indicator = new Indicator();
        repositoriesIndicator.percentage = Number(series.series.result[0].row[0]);
        repositoriesIndicator.oa = Number(series.series.result[0].row[1]);
        repositoriesIndicator.total = Number(series.series.result[0].row[2]);
        countryPageOverviewData.repositories = repositoriesIndicator;

      } else if (series.series.query.name === 'oso.journals.doaj.validated.country'
        && series.series.result && series.series.result.length > 0 && series.series.result[0].row) {

        countryPageOverviewData.name = series.series.result[0].row[3];
        countryPageOverviewData.code = series.series.result[0].row[4];

        const journalsIndicator: Indicator = new Indicator();
        journalsIndicator.percentage = Number(series.series.result[0].row[0]);
        journalsIndicator.oa = Number(series.series.result[0].row[1]);
        journalsIndicator.total = Number(series.series.result[0].row[2]);
        countryPageOverviewData.journals = journalsIndicator;

      } else if (series.series.query.name === 'oso.oa_policies.country'
        && series.series.result && series.series.result.length > 0 && series.series.result[0].row) {

        countryPageOverviewData.policies = Number(series.series.result[0].row[0]);

      } else if (series.series.query.name === 'oso.rnd.country'
        && series.series.result && series.series.result.length > 0 && series.series.result[0].row) {

        const rndExpenditure: RnDExpenditure = new RnDExpenditure();
        rndExpenditure.expenditure = Number(series.series.result[0].row[0]);
        rndExpenditure.year = Number(series.series.result[0].row[1]);
        countryPageOverviewData.rndExpenditure = rndExpenditure;

      } else if (series.series.query.name === 'oso.funder.country'
        && series.series.result && series.series.result.length > 0 && series.series.result[0].row) {

        countryPageOverviewData.funders = Number(series.series.result[0].row[0]);

      } else if (series.series.query.name === 'oso.funding_organizations.country'
        && series.series.result && series.series.result.length > 0 && series.series.result[0].row) {

        countryPageOverviewData.fundingOrganizations = Number(series.series.result[0].row[0]);

      } else if (series.series.query.name === 'oso.ec_funded_organizations.country'
        && series.series.result && series.series.result.length > 0 && series.series.result[0].row) {

        countryPageOverviewData.ec_fundedOrganizations = Number(series.series.result[0].row[0]);

      } else if (series.series.query.name === 'new.oso.ec_funded_projects.country'
        && series.series.result && series.series.result.length > 0 && series.series.result[0].row) {

        countryPageOverviewData.ec_fundedProjects = Number(series.series.result[0].row[0]);
      }
    }

    return countryPageOverviewData;
  }

  public convertRawDataToCountryPageOAData(rawData: RawData) {

    const countryPageOverviewData: CountryPageOverviewData = new CountryPageOverviewData();

    for (const series of rawData.datasets) {

      if ((series.series.query.name === 'new.oso.publications.peer_reviewed.oa_percentage.country')
        && series.series.result && series.series.result.length > 0 && series.series.result[0].row) {

        countryPageOverviewData.name = series.series.result[0].row[3];
        countryPageOverviewData.code = series.series.result[0].row[4];

        const publicationsAffiliatedIndicator: Indicator = new Indicator();
        publicationsAffiliatedIndicator.percentage = Number(series.series.result[0].row[0]);
        publicationsAffiliatedIndicator.oa = Number(series.series.result[0].row[1]);
        publicationsAffiliatedIndicator.total = Number(series.series.result[0].row[2]);
        countryPageOverviewData.publicationsAffiliated = publicationsAffiliatedIndicator;

      } else  if ((series.series.query.name === 'oso.results.oa_percentage.country')
        && series.series.query.parameters && series.series.query.parameters[1] === 'dataset'
        && series.series.result && series.series.result.length > 0 && series.series.result[0].row) {

        countryPageOverviewData.name = series.series.result[0].row[3];
        countryPageOverviewData.code = series.series.result[0].row[4];

        const datasetsAffiliatedIndicator: Indicator = new Indicator();
        datasetsAffiliatedIndicator.percentage = Number(series.series.result[0].row[0]);
        datasetsAffiliatedIndicator.oa = Number(series.series.result[0].row[1]);
        datasetsAffiliatedIndicator.total = Number(series.series.result[0].row[2]);
        countryPageOverviewData.datasetsAffiliated = datasetsAffiliatedIndicator;

      } else if ((series.series.query.name === 'oso.results.oa_percentage.country')
        && series.series.query.parameters && series.series.query.parameters[1] === 'software'
        && series.series.result && series.series.result.length > 0 && series.series.result[0].row) {

        countryPageOverviewData.name = series.series.result[0].row[3];
        countryPageOverviewData.code = series.series.result[0].row[4];

        const softwareIndicator: Indicator = new Indicator();
        softwareIndicator.percentage = Number(series.series.result[0].row[0]);
        softwareIndicator.oa = Number(series.series.result[0].row[1]);
        softwareIndicator.total = Number(series.series.result[0].row[2]);
        countryPageOverviewData.software = softwareIndicator;

      } else if ((series.series.query.name === 'oso.results.oa_percentage.country')
        && series.series.query.parameters && series.series.query.parameters[1] === 'other'
        && series.series.result && series.series.result.length > 0 && series.series.result[0].row) {

        countryPageOverviewData.name = series.series.result[0].row[3];
        countryPageOverviewData.code = series.series.result[0].row[4];

        const otherIndicator: Indicator = new Indicator();
        otherIndicator.percentage = Number(series.series.result[0].row[0]);
        otherIndicator.oa = Number(series.series.result[0].row[1]);
        otherIndicator.total = Number(series.series.result[0].row[2]);
        countryPageOverviewData.other = otherIndicator;

      }
    }

    return countryPageOverviewData;
  }

  public convertRawDataToTreemapHighchartsData(rawData: RawData) {

    const treeMapData: TreemapHighchartsData[] = [];

    for (const series of rawData.datasets) {
      // let index = 1;
      for (const rowResult of series.series.result) {

        const treeMapItemData: TreemapHighchartsData = new TreemapHighchartsData();
        treeMapItemData.name = rowResult.row[1];
        treeMapItemData.value = Number(rowResult.row[0]);
        treeMapItemData.colorValue = Number(rowResult.row[0]);
        // treeMapItemData.colorValue = index;

        treeMapData.push(treeMapItemData);

        // index = index + 1;
      }
    }

    return treeMapData;
  }

  private rawResultToCountryMapData(result: Row[]) {

    const entityMapData: CountryMapData[] = [];
    for (const rowResult of result) {

      // remove unwanted countries
      if (this.isCountryToBeRemoved(rowResult.row[2])) {
        continue;
      }

      const countryMapData: CountryMapData = new CountryMapData();
      countryMapData.value = Number(rowResult.row[0]);
      countryMapData.name = rowResult.row[1];
      countryMapData.code = rowResult.row[2];

      entityMapData.push(countryMapData);
    }

    return entityMapData;
  }

  private isCountryToBeRemoved(countryCode: string) {
    return (countryCode === 'GG' || countryCode === 'FO' || countryCode === 'GI' || countryCode === 'VA' || countryCode === 'IM'
      || countryCode === 'JE' || countryCode === 'LI' || countryCode === 'MC' || countryCode === 'SM' || countryCode === 'SJ'
      || countryCode === 'AX');
  }
}

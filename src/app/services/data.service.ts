import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { OverviewData } from '../domain/overview-data';
import { environment } from '../../environments/environment';
import { RawData } from '../domain/raw-data';

const headerOptions = {
  headers : new HttpHeaders().set('Content-Type', 'application/json')
    .set('Accept', 'application/json'),
};

@Injectable ()
export class DataService {

  private _jsonURl = 'http://esperos.di.uoa.gr/oss.json';

  private apiURL = environment.API_ENDPOINT + 'raw?json=';
  private profileName = environment.profileName;

  constructor(private httpClient: HttpClient) {
  }

  public getLastUpdateDate(): Observable<RawData> {
    const lastUpdateDateQuery = `{"series":[{"query":{"name":"creation_date","profile":"${this.profileName}"}}],"verbose":true}`;
    return this.httpClient.get<RawData>(this.apiURL + encodeURIComponent(lastUpdateDateQuery), headerOptions);
  }

  public getOverviewMapData(): Observable<RawData> {
    const mapDataQuery = `{"series":[{"query":{"name":"new.oso.publications.peer_reviewed.affiliated","profile":"${this.profileName}"}},{"query":{"name":"new.oso.publications.peer_reviewed.deposited","profile":"${this.profileName}"}},{"query":{"name":"oso.results.affiliated","parameters":["dataset"],"profile":"${this.profileName}"}},{"query":{"name":"oso.results.deposited","parameters":["dataset"],"profile":"${this.profileName}"}},{"query":{"name":"oso.repositories","profile":"${this.profileName}"}},{"query":{"name":"oso.repositories.doar_re3data","profile":"${this.profileName}"}},{"query":{"name":"oso.journals","profile":"${this.profileName}"}},{"query":{"name":"oso.journals.doaj","profile":"${this.profileName}"}},{"query":{"name":"oso.oa_policies","profile":"${this.profileName}"}}],"verbose":true}`;
    return this.httpClient.get<RawData>(this.apiURL + encodeURIComponent(mapDataQuery), headerOptions);
  }

  public getCountryOverviewData(countryCode: string): Observable<RawData> {
    const countryOverviewDataQuery = `{"series":[{"query":{"name":"new.oso.publications.peer_reviewed.affiliated.country", "parameters":["${countryCode}"],"profile":"${this.profileName}"}},{"query":{"name":"new.oso.publications.peer_reviewed.deposited.country", "parameters":["${countryCode}"],"profile":"${this.profileName}"}},{"query":{"name":"oso.results.affiliated.country", "parameters":["${countryCode}","dataset"],"profile":"${this.profileName}"}},{"query":{"name":"oso.results.deposited.country", "parameters":["${countryCode}","dataset"],"profile":"${this.profileName}"}},{"query":{"name":"oso.repositories.doar_re3data.country", "parameters":["${countryCode}"],"profile":"${this.profileName}"}},{"query":{"name":"oso.journals.doaj.country", "parameters":["${countryCode}"],"profile":"${this.profileName}"}},{"query":{"name":"oso.oa_policies.country", "parameters":["${countryCode}"],"profile":"${this.profileName}"}}],"verbose":true}`;
    return this.httpClient.get<RawData>(this.apiURL + encodeURIComponent(countryOverviewDataQuery), headerOptions);
  }

  public getCountryOAData(countryCode: string): Observable<RawData> {
    const countryOADataQuery = `{"series":[{"query":{"name":"new.oso.publications.peer_reviewed.oa_percentage.country", "parameters":["${countryCode}"],"profile":"${this.profileName}"}},{"query":{"name":"oso.results.oa_percentage.country", "parameters":["${countryCode}","dataset"],"profile":"${this.profileName}"}},{"query":{"name":"oso.results.oa_percentage.country", "parameters":["${countryCode}","software"],"profile":"${this.profileName}"}},{"query":{"name":"oso.results.oa_percentage.country", "parameters":["${countryCode}","other"],"profile":"${this.profileName}"}}],"verbose":true}`;
    return this.httpClient.get<RawData>(this.apiURL + encodeURIComponent(countryOADataQuery), headerOptions);
  }

  public getEuropeOverviewData(): Observable<RawData> {
    const overviewRawDataQuery = `{"series":[{"query":{"name":"new.oso.publications.peer_reviewed.oa_percentage","profile":"${this.profileName}"}},{"query":{"name":"oso.results.oa_percentage","parameters":["dataset"],"profile":"${this.profileName}"}},{"query":{"name":"oso.repositories.doar_re3data.validated","profile":"${this.profileName}"}},{"query":{"name":"oso.journals.doaj.validated","profile":"${this.profileName}"}},{"query":{"name":"oso.oa_policies.europe","profile":"${this.profileName}"}}],"verbose":true}`;
    return this.httpClient.get<RawData>(this.apiURL + encodeURIComponent(overviewRawDataQuery), headerOptions);
  }

  public getOverviewTableAbsoluteData(content: string): Observable<RawData> {
    let overviewTableAbsoluteDataQuery = '';
    if (content === 'affiliated') {
      overviewTableAbsoluteDataQuery = `{"series":[{"query":{"name":"oso.results.affiliated","parameters":["publication"],"profile":"${this.profileName}"}},{"query":{"name":"oso.results.affiliated","parameters":["dataset"],"profile":"${this.profileName}"}},{"query":{"name":"oso.results.affiliated","parameters":["software"],"profile":"${this.profileName}"}},{"query":{"name":"oso.results.affiliated","parameters":["other"],"profile":"${this.profileName}"}},{"query":{"name":"oso.repositories","profile":"${this.profileName}"}},{"query":{"name":"oso.journals","profile":"${this.profileName}"}},{"query":{"name":"oso.oa_policies","profile":"${this.profileName}"}}],"verbose":true}`;
    } else if (content === 'affiliated_peer_reviewed') {
      overviewTableAbsoluteDataQuery = `{"series":[{"query":{"name":"oso.results.affiliated.peer_reviewed","parameters":["publication"],"profile":"${this.profileName}"}},{"query":{"name":"oso.results.affiliated","parameters":["dataset"],"profile":"${this.profileName}"}},{"query":{"name":"oso.results.affiliated","parameters":["software"],"profile":"${this.profileName}"}},{"query":{"name":"oso.results.affiliated","parameters":["other"],"profile":"${this.profileName}"}},{"query":{"name":"oso.repositories","profile":"${this.profileName}"}},{"query":{"name":"oso.journals","profile":"${this.profileName}"}},{"query":{"name":"oso.oa_policies","profile":"${this.profileName}"}}],"verbose":true}`;
    } else if (content === 'deposited') {
      overviewTableAbsoluteDataQuery = `{"series":[{"query":{"name":"oso.results.deposited","parameters":["publication"],"profile":"${this.profileName}"}},{"query":{"name":"oso.results.deposited","parameters":["dataset"],"profile":"${this.profileName}"}},{"query":{"name":"oso.results.deposited","parameters":["software"],"profile":"${this.profileName}"}},{"query":{"name":"oso.results.deposited","parameters":["other"],"profile":"${this.profileName}"}},{"query":{"name":"oso.repositories","profile":"${this.profileName}"}},{"query":{"name":"oso.journals","profile":"${this.profileName}"}},{"query":{"name":"oso.oa_policies","profile":"${this.profileName}"}}],"verbose":true}`;
    } else if (content === 'deposited_peer_reviewed') {
      overviewTableAbsoluteDataQuery = `{"series":[{"query":{"name":"oso.results.deposited.peer_reviewed","parameters":["publication"],"profile":"${this.profileName}"}},{"query":{"name":"oso.results.deposited","parameters":["dataset"],"profile":"${this.profileName}"}},{"query":{"name":"oso.results.deposited","parameters":["software"],"profile":"${this.profileName}"}},{"query":{"name":"oso.results.deposited","parameters":["other"],"profile":"${this.profileName}"}},{"query":{"name":"oso.repositories","profile":"${this.profileName}"}},{"query":{"name":"oso.journals","profile":"${this.profileName}"}},{"query":{"name":"oso.oa_policies","profile":"${this.profileName}"}}],"verbose":true}`;
    }
    return this.httpClient.get<RawData>(this.apiURL + encodeURIComponent(overviewTableAbsoluteDataQuery), headerOptions);
  }

  public getOverviewTablePercentageData(content: string): Observable<RawData> {
    let overviewTablePercentageDataQuery = '';
    if (content === 'affiliated') {
      overviewTablePercentageDataQuery = `{"series":[{"query":{"name":"oso.results.oa_percentage.bycountry","parameters":["publication"],"profile":"${this.profileName}"}},{"query":{"name":"oso.results.oa_percentage.bycountry","parameters":["dataset"],"profile":"${this.profileName}"}},{"query":{"name":"oso.results.oa_percentage.bycountry","parameters":["software"],"profile":"${this.profileName}"}},{"query":{"name":"oso.results.oa_percentage.bycountry","parameters":["other"],"profile":"${this.profileName}"}},{"query":{"name":"oso.repositories.doar_re3data.validated.bycountry","profile":"${this.profileName}"}},{"query":{"name":"oso.journals.doaj.validated.bycountry","profile":"${this.profileName}"}}],"verbose":true}`;
    } else if (content === 'affiliated_peer_reviewed') {
      overviewTablePercentageDataQuery = `{"series":[{"query":{"name":"oso.results.oa_percentage.affiliated.peer_reviewed.bycountry","parameters":["publication"],"profile":"${this.profileName}"}},{"query":{"name":"oso.results.oa_percentage.bycountry","parameters":["dataset"],"profile":"${this.profileName}"}},{"query":{"name":"oso.results.oa_percentage.bycountry","parameters":["software"],"profile":"${this.profileName}"}},{"query":{"name":"oso.results.oa_percentage.bycountry","parameters":["other"],"profile":"${this.profileName}"}},{"query":{"name":"oso.repositories.doar_re3data.validated.bycountry","profile":"${this.profileName}"}},{"query":{"name":"oso.journals.doaj.validated.bycountry","profile":"${this.profileName}"}}],"verbose":true}`;
    } else if (content === 'deposited') {
      overviewTablePercentageDataQuery = `{"series":[{"query":{"name":"oso.results.oa_percentage.deposited.bycountry","parameters":["publication"],"profile":"${this.profileName}"}},{"query":{"name":"oso.results.oa_percentage.deposited.bycountry","parameters":["dataset"],"profile":"${this.profileName}"}},{"query":{"name":"oso.results.oa_percentage.deposited.bycountry","parameters":["software"],"profile":"${this.profileName}"}},{"query":{"name":"oso.results.oa_percentage.deposited.bycountry","parameters":["other"],"profile":"${this.profileName}"}},{"query":{"name":"oso.repositories.doar_re3data.validated.bycountry","profile":"${this.profileName}"}},{"query":{"name":"oso.journals.doaj.validated.bycountry","profile":"${this.profileName}"}}],"verbose":true}`;
    } else if (content === 'deposited_peer_reviewed') {
      overviewTablePercentageDataQuery = `{"series":[{"query":{"name":"oso.results.oa_percentage.deposited.peer_reviewed.bycountry","parameters":["publication"],"profile":"${this.profileName}"}},{"query":{"name":"oso.results.oa_percentage.deposited.bycountry","parameters":["dataset"],"profile":"${this.profileName}"}},{"query":{"name":"oso.results.oa_percentage.deposited.bycountry","parameters":["software"],"profile":"${this.profileName}"}},{"query":{"name":"oso.results.oa_percentage.deposited.bycountry","parameters":["other"],"profile":"${this.profileName}"}},{"query":{"name":"oso.repositories.doar_re3data.validated.bycountry","profile":"${this.profileName}"}},{"query":{"name":"oso.journals.doaj.validated.bycountry","profile":"${this.profileName}"}}],"verbose":true}`;
    }
    return this.httpClient.get<RawData>(this.apiURL + encodeURIComponent(overviewTablePercentageDataQuery), headerOptions);
  }

  public getLeadingOpenScienceMobileData(): Observable<RawData> {
    const leadingOpenScienceMobileDataQuery = `{"series":[{"query":{"name":"new.oso.mobile.overview","profile":"${this.profileName}"}}],"verbose":true}`;
    return this.httpClient.get<RawData>(this.apiURL + encodeURIComponent(leadingOpenScienceMobileDataQuery), headerOptions);
  }

  public getEuropeOAPercentages(): Observable<RawData> {
    const europeOAPercentagesQuery = `{"series":[{"query":{"name":"new.oso.publications.peer_reviewed.oa_percentage","profile":"${this.profileName}"}},{"query":{"name":"oso.results.oa_percentage","parameters":["dataset"],"profile":"${this.profileName}"}},{"query":{"name":"oso.results.oa_percentage","parameters":["software"],"profile":"${this.profileName}"}},{"query":{"name":"oso.results.oa_percentage","parameters":["other"],"profile":"${this.profileName}"}}],"verbose":true}`;
    return this.httpClient.get<RawData>(this.apiURL + encodeURIComponent(europeOAPercentagesQuery), headerOptions);
  }

  public getCountryPageOverviewData(countryCode: string): Observable<RawData> {
    const countryPageOverviewDataQuery = `{"series":[{"query":{"name":"new.oso.publications.peer_reviewed.oa_percentage.country", "parameters":["${countryCode}"],"profile":"${this.profileName}"}},{"query":{"name":"new.oso.publications.peer_reviewed.oa_percentage.deposited.country", "parameters":["${countryCode}"],"profile":"${this.profileName}"}},{"query":{"name":"oso.results.oa_percentage.country", "parameters":["${countryCode}","dataset"],"profile":"${this.profileName}"}},{"query":{"name":"oso.results.oa_percentage.deposited.country", "parameters":["dataset","${countryCode}"],"profile":"${this.profileName}"}},{"query":{"name":"oso.repositories.doar_re3data.validated.country", "parameters":["${countryCode}"],"profile":"${this.profileName}"}},{"query":{"name":"oso.journals.doaj.validated.country", "parameters":["${countryCode}"],"profile":"${this.profileName}"}},{"query":{"name":"oso.oa_policies.country", "parameters":["${countryCode}"],"profile":"${this.profileName}"}},{"query":{"name":"oso.rnd.country","parameters":["${countryCode}"],"profile":"${this.profileName}"}},{"query":{"name":"oso.funder.country", "parameters":["${countryCode}"],"profile":"${this.profileName}"}},{"query":{"name":"oso.funding_organizations.country", "parameters":["${countryCode}"],"profile":"${this.profileName}"}},{"query":{"name":"oso.ec_funded_organizations.country", "parameters":["${countryCode}"],"profile":"${this.profileName}"}},{"query":{"name":"new.oso.ec_funded_projects.country", "parameters":["${countryCode}"],"profile":"${this.profileName}"}}],"verbose":true}`;
    return this.httpClient.get<RawData>(this.apiURL + encodeURIComponent(countryPageOverviewDataQuery), headerOptions);
  }

  public getFundersResults(): Observable<RawData> {
    const fundersResultsQuery = `{"series":[{"query":{"name":"oso.funders.results","profile":"${this.profileName}"}}],"verbose":true}`;
    return this.httpClient.get<RawData>(this.apiURL + encodeURIComponent(fundersResultsQuery), headerOptions);
  }

  public getFundersResultsByType(type: string): Observable<RawData> {
    const fundersResultsByTypeQuery = `{"series":[{"query":{"name":"new.oso.results.byfunder", "parameters":["${type}"],"profile":"${this.profileName}"}}],"verbose":true}`;
    return this.httpClient.get<RawData>(this.apiURL + encodeURIComponent(fundersResultsByTypeQuery), headerOptions);
  }

  public getFundersResultsByPublications(): Observable<RawData> {
    const fundersResultsByPublicationsQuery = `{"series":[{"query":{"name":"new.oso.publications.peer_reviewed.byfunder","profile":"${this.profileName}"}}],"verbose":true}`;
    return this.httpClient.get<RawData>(this.apiURL + encodeURIComponent(fundersResultsByPublicationsQuery), headerOptions);
  }

  public getFundersResultsByTypeForPID(type: string): Observable<RawData> {
    const fundersResultsByTypeForPIDQuery = `{"series":[{"query":{"name":"oso.results.pid.affiliated.byfunder", "parameters":["${type}"],"profile":"${this.profileName}"}}],"verbose":true}`;
    return this.httpClient.get<RawData>(this.apiURL + encodeURIComponent(fundersResultsByTypeForPIDQuery), headerOptions);
  }

  public getFundersResultsByTypeForLicence(type: string): Observable<RawData> {
    const fundersResultsByTypeForLicenceQuery = `{"series":[{"query":{"name":"oso.results.licence.affiliated.byfunder", "parameters":["${type}"],"profile":"${this.profileName}"}}],"verbose":true}`;
    return this.httpClient.get<RawData>(this.apiURL + encodeURIComponent(fundersResultsByTypeForLicenceQuery), headerOptions);
  }

  public getFundersResultsByTypeForCCLicence(type: string): Observable<RawData> {
    const fundersResultsByTypeForCCLicenceQuery = `{"series":[{"query":{"name":"new.oso.results.cc_licence.affiliated.byfunder", "parameters":["${type}"],"profile":"${this.profileName}"}}],"verbose":true}`;
    return this.httpClient.get<RawData>(this.apiURL + encodeURIComponent(fundersResultsByTypeForCCLicenceQuery), headerOptions);
  }

  public getFundersResultsByTypeForAbstract(): Observable<RawData> {
    const fundersResultsByTypeForAbstractQuery = `{"series":[{"query":{"name":"new.oso.publications.abstract.peer_reviewed.affiliated.byfunder","profile":"${this.profileName}"}}],"verbose":true}`;
    return this.httpClient.get<RawData>(this.apiURL + encodeURIComponent(fundersResultsByTypeForAbstractQuery), headerOptions);
  }

  // public getFundersResultsByTypeForGold(type: string): Observable<RawData> {
  //   const fundersResultsByTypeForGoldQuery = `{"series":[{"query":{"name":"oso.results.gold.affiliated.byfunder", "parameters":["${type}"],"profile":"${this.profileName}"}}],"verbose":true}`;
  //   return this.httpClient.get<RawData>(this.apiURL + encodeURIComponent(fundersResultsByTypeForGoldQuery), headerOptions);
  // }
  //
  // public getFundersResultsByTypeForGreen(type: string): Observable<RawData> {
  //   const fundersResultsByTypeForGreenQuery = `{"series":[{"query":{"name":"oso.results.green.affiliated.byfunder", "parameters":["${type}"],"profile":"${this.profileName}"}}],"verbose":true}`;
  //   return this.httpClient.get<RawData>(this.apiURL + encodeURIComponent(fundersResultsByTypeForGreenQuery), headerOptions);
  // }

  public getFundersResultsByPublicationForCollaborationIndicatorForCountry(indicator: string): Observable<RawData> {
    const fundersResultsByPublicationForIndicatorQuery = `{"series":[{"query":{"name":"new.oso.publications.peer_reviewed.${indicator}_collab.affiliated.byfunder", "parameters":[1],"profile":"${this.profileName}"}}],"verbose":true}`;
    return this.httpClient.get<RawData>(this.apiURL + encodeURIComponent(fundersResultsByPublicationForIndicatorQuery), headerOptions);
  }

  public getFundersResultsByTypeForCollaborationIndicatorForCountry(type: string, indicator: string): Observable<RawData> {
    const fundersResultsByPublicationForIndicatorQuery = `{"series":[{"query":{"name":"new.oso.results.${indicator}_collab.affiliated.byfunder", "parameters":[1,"${type}"],"profile":"${this.profileName}"}}],"verbose":true}`;
    return this.httpClient.get<RawData>(this.apiURL + encodeURIComponent(fundersResultsByPublicationForIndicatorQuery), headerOptions);
  }

  public getFundersResultsByTypeForCountry(type: string, countryCode: string): Observable<RawData> {
    const fundersResultsByTypeForCountryQuery = `{"series":[{"query":{"name":"oso.results.affiliated.byfunder.country", "parameters":["${countryCode}","${type}"],"profile":"${this.profileName}"}}],"verbose":true}`;
    return this.httpClient.get<RawData>(this.apiURL + encodeURIComponent(fundersResultsByTypeForCountryQuery), headerOptions);
  }

  public getFundersResultsByPublicationsForCountry(countryCode: string): Observable<RawData> {
    const fundersResultsByPublicationsForCountryQuery = `{"series":[{"query":{"name":"new.oso.publications.peer_reviewed.affiliated.byfunder.country", "parameters":["${countryCode}"],"profile":"${this.profileName}"}}],"verbose":true}`;
    return this.httpClient.get<RawData>(this.apiURL + encodeURIComponent(fundersResultsByPublicationsForCountryQuery), headerOptions);
  }

  public getFundersResultsByTypeForIndicatorForCountry(type: string, indicator: string, countryCode: string): Observable<RawData> {
    const fundersResultsByTypeForPIDForCountryQuery = `{"series":[{"query":{"name":"new.oso.results.${indicator}.affiliated.byfunder.country", "parameters":[1,"${type}","${countryCode}"],"profile":"${this.profileName}"}}],"verbose":true}`;
    return this.httpClient.get<RawData>(this.apiURL + encodeURIComponent(fundersResultsByTypeForPIDForCountryQuery), headerOptions);
  }

  public getFundersResultsByPublicationsForIndicatorForCountry(indicator: string, countryCode: string): Observable<RawData> {
    const fundersResultsByPublicationsForPIDForCountryQuery = `{"series":[{"query":{"name":"new.oso.publications.${indicator}.peer_reviewed.affiliated.byfunder.country", "parameters":[1,"${countryCode}"],"profile":"${this.profileName}"}}],"verbose":true}`;
    return this.httpClient.get<RawData>(this.apiURL + encodeURIComponent(fundersResultsByPublicationsForPIDForCountryQuery), headerOptions);
  }

  public getOpenScienceIndicatorsTableData(type: string, content: string): Observable<RawData> {

    let indicatorsTableDataQuery = '';
    if (content === 'affiliated') {
      indicatorsTableDataQuery = `{"series":[{"query":{"name":"oso.results.pid_percentage.affiliated.bycountry","parameters":["${type}"],"profile":"${this.profileName}"}},{"query":{"name":"oso.results.licence_percentage.affiliated.bycountry","parameters":["${type}"],"profile":"${this.profileName}"}},{"query":{"name":"oso.results.green_percentage.affiliated.bycountry","parameters":["${type}"],"profile":"${this.profileName}"}},{"query":{"name":"oso.results.gold_percentage.affiliated.bycountry","parameters":["${type}"],"profile":"${this.profileName}"}},{"query":{"name":"new.oso.results.cc_licence_percentage.affiliated.bycountry","parameters":["${type}"],"profile":"${this.profileName}"}},{"query":{"name":"new.oso.results.abstract_percentage.affiliated.bycountry","parameters":["${type}"],"profile":"${this.profileName}"}}],"verbose":true}`;
    } else if (content === 'affiliated_peer_reviewed') {
      indicatorsTableDataQuery = `{"series":[{"query":{"name":"oso.results.pid_percentage.affiliated.peer_reviewed.bycountry","parameters":["${type}"],"profile":"${this.profileName}"}},{"query":{"name":"oso.results.licence_percentage.affiliated.peer_reviewed.bycountry","parameters":["${type}"],"profile":"${this.profileName}"}},{"query":{"name":"oso.results.green_percentage.affiliated.peer_reviewed.bycountry","parameters":["${type}"],"profile":"${this.profileName}"}},{"query":{"name":"oso.results.gold_percentage.affiliated.peer_reviewed.bycountry","parameters":["${type}"],"profile":"${this.profileName}"}},{"query":{"name":"new.oso.results.cc_licence_percentage.affiliated.peer_reviewed.bycountry","parameters":["${type}"],"profile":"${this.profileName}"}},{"query":{"name":"new.oso.results.abstract_percentage.affiliated.peer_reviewed.bycountry","parameters":["${type}"],"profile":"${this.profileName}"}}],"verbose":true}`;
    } else if (content === 'deposited') {
      indicatorsTableDataQuery = `{"series":[{"query":{"name":"oso.results.pid_percentage.deposited.bycountry","parameters":["${type}"],"profile":"${this.profileName}"}},{"query":{"name":"oso.results.licence_percentage.deposited.bycountry","parameters":["${type}"],"profile":"${this.profileName}"}},{"query":{"name":"oso.results.green_percentage.deposited.bycountry","parameters":["${type}"],"profile":"${this.profileName}"}},{"query":{"name":"oso.results.gold_percentage.deposited.bycountry","parameters":["${type}"],"profile":"${this.profileName}"}},{"query":{"name":"new.oso.results.cc_licence_percentage.deposited.bycountry","parameters":["${type}"],"profile":"${this.profileName}"}},{"query":{"name":"new.oso.results.abstract_percentage.deposited.bycountry","parameters":["${type}"],"profile":"${this.profileName}"}}],"verbose":true}`;
    } else if (content === 'deposited_peer_reviewed') {
      indicatorsTableDataQuery = `{"series":[{"query":{"name":"oso.results.pid_percentage.deposited.peer_reviewed.bycountry","parameters":["${type}"],"profile":"${this.profileName}"}},{"query":{"name":"oso.results.licence_percentage.deposited.peer_reviewed.bycountry","parameters":["${type}"],"profile":"${this.profileName}"}},{"query":{"name":"oso.results.green_percentage.deposited.peer_reviewed.bycountry","parameters":["${type}"],"profile":"${this.profileName}"}},{"query":{"name":"oso.results.gold_percentage.deposited.peer_reviewed.bycountry","parameters":["${type}"],"profile":"${this.profileName}"}},{"query":{"name":"new.oso.results.cc_licence_percentage.deposited.peer_reviewed.bycountry","parameters":["${type}"],"profile":"${this.profileName}"}},{"query":{"name":"new.oso.results.abstract_percentage.deposited.peer_reviewed.bycountry","parameters":["${type}"],"profile":"${this.profileName}"}}],"verbose":true}`;
    }
    return this.httpClient.get<RawData>(this.apiURL + encodeURIComponent(indicatorsTableDataQuery), headerOptions);
  }

  public getCollaborationIndicatorsTableData(type: string, content: string): Observable<RawData> {

    let indicatorsTableDataQuery = '';
    if (content === 'affiliated') {
      indicatorsTableDataQuery = `{"series":[{"query":{"name":"new.oso.results.funders_collab_percentage.affiliated.bycountry","parameters":["${type}"],"profile":"${this.profileName}"}},{"query":{"name":"new.oso.results.projects_collab_percentage.affiliated.bycountry","parameters":["${type}"],"profile":"${this.profileName}"}},{"query":{"name":"new.oso.results.authors_collab_percentage.affiliated.bycountry","parameters":["${type}"],"profile":"${this.profileName}"}}],"verbose":true}`;
    } else if (content === 'affiliated_peer_reviewed') {
      indicatorsTableDataQuery = `{"series":[{"query":{"name":"new.oso.results.funders_collab_percentage.affiliated.peer_reviewed.bycountry","parameters":["${type}"],"profile":"${this.profileName}"}},{"query":{"name":"new.oso.results.projects_collab_percentage.affiliated.peer_reviewed.bycountry","parameters":["${type}"],"profile":"${this.profileName}"}},{"query":{"name":"new.oso.results.authors_collab_percentage.affiliated.peer_reviewed.bycountry","parameters":["${type}"],"profile":"${this.profileName}"}}],"verbose":true}`;
    } else if (content === 'deposited') {
      indicatorsTableDataQuery = `{"series":[{"query":{"name":"new.oso.results.funders_collab_percentage.deposited.bycountry","parameters":["${type}"],"profile":"${this.profileName}"}},{"query":{"name":"new.oso.results.projects_collab_percentage.deposited.bycountry","parameters":["${type}"],"profile":"${this.profileName}"}},{"query":{"name":"new.oso.results.authors_collab_percentage.deposited.bycountry","parameters":["${type}"],"profile":"${this.profileName}"}}],"verbose":true}`;
    } else if (content === 'deposited_peer_reviewed') {
      indicatorsTableDataQuery = `{"series":[{"query":{"name":"new.oso.results.funders_collab_percentage.deposited.peer_reviewed.bycountry","parameters":["${type}"],"profile":"${this.profileName}"}},{"query":{"name":"new.oso.results.projects_collab_percentage.deposited.peer_reviewed.bycountry","parameters":["${type}"],"profile":"${this.profileName}"}},{"query":{"name":"new.oso.results.authors_collab_percentage.deposited.peer_reviewed.bycountry","parameters":["${type}"],"profile":"${this.profileName}"}}],"verbose":true}`;
    }
    return this.httpClient.get<RawData>(this.apiURL + encodeURIComponent(indicatorsTableDataQuery), headerOptions);
  }


  public getOverviewData(): Observable<OverviewData> {
    return this.httpClient.get<OverviewData>(this._jsonURl, headerOptions);
  }

  public getCountryData(countryName: string) {
    return this.httpClient.get<OverviewData>(this._jsonURl, headerOptions);
  }
}

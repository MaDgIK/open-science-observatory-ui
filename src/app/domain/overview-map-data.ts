import {Indicator} from './overview-data';

export class OverviewMapData {
  publications: CountryMapData[];
  datasets: CountryMapData[];
  repositories: CountryMapData[];
  journals: CountryMapData[];
  policies: CountryMapData[];
}

export class CountryMapData {
  code: string;
  name: string;
  value: number;
}

export class JoinedMapData {
  countryName: string;
  countryCode: string;
  affiliated: number;
  deposited: number;
  doar_re3data: number;
  doaj: number;
  simple: number;
}

export class SelectedCountry {
  name: string;
  code: string;
}

export class CountryOverviewData {
  name: string = null;
  code: string = null;
  publicationsAffiliated: number = null;
  publicationsDeposited: number = null;
  datasetsAffiliated: number = null;
  datasetsDeposited: number = null;
  repositories: number = null;
  journals: number = null;
  policies: number = null;
}

export class EuropeData {
  publications: Indicator;
  datasets: Indicator;
  software: Indicator;
  other: Indicator;
  repositories: Indicator;
  journals: Indicator;
  policies: Indicator;
}

export class CountryTableData {
  name: string = null;
  code: string = null;
  publications: number = null;
  datasets: number = null;
  software: number = null;
  other: number = null;
  repositories: number = null;
  journals: number = null;
  policies: number = null;
  pid: Indicator;
  licence: Indicator;
  gold: Indicator;
  green: Indicator;
  cc_licence: Indicator;
  abstract: Indicator;
  funders_collab: Indicator;
  projects_collab: Indicator;
  authors_collab: Indicator;
}

export class CountryPageOverviewData {
  name: string = null;
  code: string = null;
  publicationsAffiliated: Indicator;
  publicationsDeposited: Indicator ;
  datasetsAffiliated: Indicator;
  datasetsDeposited: Indicator;
  software: Indicator;
  other: Indicator;
  repositories: Indicator;
  journals: Indicator;
  policies: number = null;
  rndExpenditure: RnDExpenditure;
  funders: number = null;
  fundingOrganizations: number = null;
  ec_fundedOrganizations: number = null;
  ec_fundedProjects: number = null;
}

export class RnDExpenditure {
  expenditure: number;
  year: number;
}

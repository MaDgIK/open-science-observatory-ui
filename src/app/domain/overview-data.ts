export class OverviewData {
  overview: OverviewIndicators;
  countries: CountryOverview[];
}

export class OverviewIndicators {
  publications: Indicator;
  datasets: Indicator;
  repositories: Indicator;
  journals: Indicator;
  policies: Indicator;
  software: Indicator;
  otherProducts: Indicator;
  funders: Indicator;
  ecFundedOrganizations: Indicator;
}

export class CountryOverview {
  country: string;
  repositories: Indicator;
  journals: Indicator;
  policies: Indicator;
  publications: Indicator;
  datasets: Indicator;
  software: Indicator;
  otherProducts: Indicator;
  funders: Indicator;
  ecFundedOrganizations: Indicator;
}

export class Indicator {
  oa: number;
  total: number;
  percentage: number;
}

// export const publicationColor = '#F181AE';
// export const publicationTooltipColor = '#a52e5d';
// export const publicationBackgroundColor = '#f8d5e3';
// export const publicationColor = '#381565';
export const publicationColor = '#6f38b1';
// export const publicationTooltipColor = '#250e44';
export const publicationTooltipColor = '#4e277c';
export const publicationBackgroundColor = '#e9ddf8';

// export const datasetColor = '#A98BD4';
// export const datasetTooltipColor = '#7658a1';
// export const datasetBackgroundColor = '#e2daf0';
export const datasetColor = '#0044a2';
export const datasetTooltipColor = '#00337a';
export const datasetBackgroundColor = '#d6e7ff';

// export const repositoriesColor = '#708AA5';
// export const repositoriesTooltipColor = '#3d5772';
// export const repositoriesBackgroundColor = '#ADD3E0';
export const repositoriesColor = '#007997';
export const repositoriesTooltipColor = '#00627a';
export const repositoriesBackgroundColor = '#d6f7ff';

// export const journalsColor = '#FFCE4E';
// export const journalsTooltipColor = '#cc9b1b';
// export const journalsBackgroundColor = '#FBE8B1';
export const journalsColor = '#00554E';
export const journalsTooltipColor = '#003d38';
export const journalsBackgroundColor = '#d6fffc';

export const policiesColor = '#639C66';
export const policiesTooltipColor = '#306933';

// export const softwareColor = '#C2155A';
// export const softwareColor = '#542097';
export const softwareColor = '#009DE0';

// export const otherResearchProductsColor = '#00CCCC';
// export const otherResearchProductsColor = '#00b8d0';
export const otherResearchProductsColor = '#00B086';

// export const nonOAColor = '#787878';
export const nonOAColor = '#c3cad2';
export const noIndicatorColor = '#c3cad2';

// export const resultColor = '#072AE6';
export const resultColor = '#EC4386';

// export const gradientStartColor = '#A1AAB8';
export const gradientStartColor = '#c3cad2';

export const goldColor = '#ffc000';
export const greenColor = '#70ad47';
export const goldGreenColor = '#BFD200';

export const publicationPalette = [publicationColor, nonOAColor,
  '#2f7ed8', '#0d233a', '#8bbc21', '#910000', '#1aadce', '#492970', '#f28f43', '#77a1e5', '#c42525', '#a6c96a'];
export const publicationIndicatorsPalette = [publicationColor, noIndicatorColor,
  '#2f7ed8', '#0d233a', '#8bbc21', '#910000', '#1aadce', '#492970', '#f28f43', '#77a1e5', '#c42525', '#a6c96a'];

export const datasetPalette = [datasetColor, nonOAColor,
  '#2f7ed8', '#0d233a', '#8bbc21', '#910000', '#1aadce', '#492970', '#f28f43', '#77a1e5', '#c42525', '#a6c96a'];
export const datasetIndicatorsPalette = [datasetColor, noIndicatorColor,
  '#2f7ed8', '#0d233a', '#8bbc21', '#910000', '#1aadce', '#492970', '#f28f43', '#77a1e5', '#c42525', '#a6c96a'];

export const softwarePalette = [softwareColor, nonOAColor,
  '#2f7ed8', '#0d233a', '#8bbc21', '#910000', '#1aadce', '#492970', '#f28f43', '#77a1e5', '#c42525', '#a6c96a'];
export const softwareIndicatorsPalette = [softwareColor, noIndicatorColor,
  '#2f7ed8', '#0d233a', '#8bbc21', '#910000', '#1aadce', '#492970', '#f28f43', '#77a1e5', '#c42525', '#a6c96a'];
export const softwareIndicatorsPaletteReversed = [noIndicatorColor, softwareColor,
  '#2f7ed8', '#0d233a', '#8bbc21', '#910000', '#1aadce', '#492970', '#f28f43', '#77a1e5', '#c42525', '#a6c96a'];

export const otherResearchProductsPalette = [otherResearchProductsColor, nonOAColor,
  '#2f7ed8', '#0d233a', '#8bbc21', '#910000', '#1aadce', '#492970', '#f28f43', '#77a1e5', '#c42525', '#a6c96a'];
export const otherIndicatorsPalette = [otherResearchProductsColor, noIndicatorColor,
  '#2f7ed8', '#0d233a', '#8bbc21', '#910000', '#1aadce', '#492970', '#f28f43', '#77a1e5', '#c42525', '#a6c96a'];

export const resultsPalette = [resultColor, nonOAColor,
  '#2f7ed8', '#0d233a', '#8bbc21', '#910000', '#1aadce', '#492970', '#f28f43', '#77a1e5', '#c42525', '#a6c96a'];

export const goldGreenIndicatorsPalette = [goldColor, goldGreenColor, greenColor, noIndicatorColor,
  '#2f7ed8', '#0d233a', '#8bbc21', '#910000', '#1aadce', '#492970', '#f28f43', '#77a1e5', '#c42525', '#a6c96a'];



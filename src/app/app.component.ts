import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { SmoothScroll } from './services/smooth-scroll';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'open-science-observatory-ui';

  constructor(private router: Router, private smoothScroll: SmoothScroll) {
  }

  isEmbedRoute() {
    // console.log('Is embed route? Route is: ' + this.router.url);
    return (this.router.url === '/overview-map-embed');
  }

}

import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { countries } from '../../domain/countries';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-top-menu',
  templateUrl: './top-menu.component.html',
})

export class TopmenuComponent {

  countriesCollection = countries;
  keyword = 'name';

  environmentName = environment.environmentName;

  constructor(private router: Router) {
  }

  isHomeRoute() {
    return (this.router.url === '/home');
  }

  selectCountryFromAutocompleteEvent(item) {
    // do something with selected item
    // console.log('country selected: ', item);
    this.router.navigate([`/country/${item.id}`]);
  }

  onChangeSearch(search: string) {
    // fetch remote data from here
    // And reassign the 'data' which is binded to 'data' property.
  }

  onFocused(e) {
    // do something
  }
}

import {jsPDF} from 'jspdf';
import domtoimage from 'dom-to-image';


export function printPage(sectionID: string) {
  const node = document.getElementById(sectionID);

  let img;
  let filename;
  let newImage;


  domtoimage.toPng(node, {bgcolor: '#fff'}).then(function (dataUrl) {

    img = new Image();
    img.src = dataUrl;
    newImage = img.src;

    img.onload = function () {

      const pdfWidth = img.width;
      const pdfHeight = img.height;

      // FileSaver.saveAs(dataUrl, 'my-pdfimage.png'); // Save as Image

      let doc;

      if (pdfWidth > pdfHeight) {
        doc = new jsPDF('l', 'px', [pdfWidth, pdfHeight]);
      } else {
        doc = new jsPDF('p', 'px', [pdfWidth, pdfHeight]);
      }

      const width = doc.internal.pageSize.getWidth();
      const height = doc.internal.pageSize.getHeight();

      doc.addImage(newImage, 'PNG', 10, 10, width, height);
      filename = 'mypdf_' + '.pdf';
      doc.save(filename);

    };

  }).catch(function(error) {
    // Error Handling
  });
}

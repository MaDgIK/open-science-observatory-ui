/**
 * Created by stefania on 4/6/17.
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AsideHelpContentComponent, HelpContentComponent } from './help-content.component';
import { HelpContentService } from '../../services/help-content.service';
import { FooterComponent } from '../footer/footer.component';
import { ReadMoreComponent, ReadMoreTextComponent } from './read-more.component';
import { TopmenuComponent } from '../topmenu/top-menu.component';
import {TreemapHighchartsComponent} from '../../chart-components/treemap-highcharts/treemap-highcharts.component';
import {HighchartsChartModule} from 'highcharts-angular';
import {CountriesTableComponent} from '../../pages/home/countries-table.component';
import {EuropeMapOverviewComponent} from "../../pages/home/europe-map-overview.component";
import {NgxEchartsModule} from "ngx-echarts";

const myGroups = [
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    // TabsModule.forRoot(),
    // ModalModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    HighchartsChartModule,
    NgxEchartsModule
  ],
  declarations: [
    HelpContentComponent,
    AsideHelpContentComponent,
    // TopmenuComponent,
    FooterComponent,
    TreemapHighchartsComponent,
    CountriesTableComponent,
    ReadMoreComponent,
    ReadMoreTextComponent,
    ...myGroups,
    EuropeMapOverviewComponent
  ],
  exports: [
    HelpContentComponent,
    AsideHelpContentComponent,
    // TopmenuComponent,
    FooterComponent,
    TreemapHighchartsComponent,
    CountriesTableComponent,
    ...myGroups,
    EuropeMapOverviewComponent,
    ReadMoreComponent,
    ReadMoreTextComponent
  ],
  providers: [
    HelpContentService
  ],
})

export class ReusableComponentsModule {
}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ReusableComponentsModule } from './shared/reusablecomponents/reusable-components.module';
import { HomeComponent } from './pages/home/home.component';
import { DataService } from './services/data.service';
import { CountriesTableComponent } from './pages/home/countries-table.component';
import { DataViewComponent } from './pages/home/data-view.component';
import { HighchartsChartModule } from 'highcharts-angular';
import { CountriesMapOverviewComponent } from './pages/home/countries-map-overview.component';
import { CountryComponent } from './pages/country/country.component';
import { CountryMapComponent } from './pages/country/country-map.component';
import { GaugeChartComponent } from './pages/home/gauge-chart.component';
import { MethodologyPageComponent } from './pages/methodology/methodology.component';
import { ContinentComponent } from './pages/continent/continent.component';
import { EuropeMapOverviewComponent } from './pages/home/europe-map-overview.component';
import { DataHandlerService } from './services/data-handler.service';

import { NgxEchartsModule } from 'ngx-echarts';
import * as echarts from 'echarts';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
import { TopmenuComponent } from './shared/topmenu/top-menu.component';
import { TreemapHighchartsComponent } from './chart-components/treemap-highcharts/treemap-highcharts.component';
import { MatomoModule } from 'ngx-matomo';
import {environment} from '../environments/environment';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    // ContinentComponent,
    // CountriesTableComponent,
    DataViewComponent,
    CountriesMapOverviewComponent,
    // EuropeMapOverviewComponent,
    // CountryComponent,
    CountryMapComponent,
    GaugeChartComponent,
    MethodologyPageComponent,
    TopmenuComponent,
    // TreemapHighchartsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReusableComponentsModule,
    // NgxEchartsModule.forRoot({
    //   echarts
    // }),
    // NgxEchartsModule,
    HighchartsChartModule,
    AutocompleteLibModule,
    MatomoModule.forRoot({
      scriptUrl: environment.MATOMO_URL + 'matomo.js',
      trackers: [
        {
          trackerUrl: environment.MATOMO_URL + 'matomo.php',
          siteId: environment.MATOMO_SITE_ID
        }
      ],
      routeTracking: {
        enable: true
      }
    }),
    BrowserAnimationsModule,
  ],
  providers: [
    DataService,
    DataHandlerService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Component, Input, OnInit } from '@angular/core';

import * as  Highcharts from 'highcharts';
import More from 'highcharts/highcharts-more';
import Tree from 'highcharts/modules/treemap';
import Heatmap from 'highcharts/modules/heatmap';
import Exporting from 'highcharts/modules/exporting';
import ExportData from 'highcharts/modules/export-data';
import { TreemapHighchartsData } from '../../domain/treemap-highcharts-data';

More(Highcharts);
Tree(Highcharts);
Heatmap(Highcharts);
Exporting(Highcharts);
ExportData(Highcharts);

@Component({
  selector: 'app-treemap-highchart',
  templateUrl: './treemap-highcharts.component.html',
})

export class TreemapHighchartsComponent implements OnInit {

  @Input() chartTitle: string;
  @Input() chartSubtitle: string = '';
  @Input() chartData: TreemapHighchartsData[];
  @Input() color: string;

  @Input() height: number;

  Highcharts: typeof Highcharts = Highcharts;

  treeMapChartOptions = {};

  constructor() {}

  ngOnInit(): void {

    this.treeMapChartOptions = {
      chart: {
        type: 'treemap',
        height: this.height
      },
      title: {
        text: this.chartTitle,
        align: 'left',
        margin: 25,
        // style: {
        //   color: '#7A7A7A',
        //   fontSize: '18px'
        // }
      },
      subtitle: {
        text: this.chartSubtitle,
        align: 'left',
        margin: 25,
        // style: {
        //   color: '#7A7A7A',
        //   fontSize: '18px'
        // }
      },
      tooltip: {
        enabled: true
      },
      credits: {
        enabled: false
      },
      exporting: {
        enabled : true
      },
      colorAxis: {
        minColor: '#FFFFFF',
        maxColor: this.color,
        // maxColor: Highcharts.getOptions().colors[0]
      },
      series: [
        {
          type: 'treemap',
          layoutAlgorithm: 'squarified',
          data: this.chartData
        }
      ]
    };
  }

}

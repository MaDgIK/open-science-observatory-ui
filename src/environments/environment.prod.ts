export const environment = {
  production: true,
  // API_ENDPOINT: '/api',
  environmentName: 'prod',
  API_ENDPOINT: 'https://services.openaire.eu/stats-tool/',
  // API_ENDPOINT: 'https://stats.madgik.di.uoa.gr/stats-api/',
  FAQ_ENDPOINT: '/uoa-admin-tools/api',
  FAQ_HOMEPAGE: '/uoa-admin-tools/dashboard',
  // profileName: 'OpenAIRE All-inclusive'
  profileName: 'observatory',
  MATOMO_URL: 'https://analytics.openaire.eu/',
  MATOMO_SITE_ID: 509,
  cc_icon_path: '//osobservatory.openaire.eu/assets/img/icons/cc.svg'
};

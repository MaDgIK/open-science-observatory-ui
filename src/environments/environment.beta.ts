export const environment = {
  production: true,
  // API_ENDPOINT: '/api',
  environmentName: 'beta',
  API_ENDPOINT: 'https://beta.services.openaire.eu/stats-tool/',
  FAQ_ENDPOINT: '/uoa-admin-tools/api',
  FAQ_HOMEPAGE: '/uoa-admin-tools/dashboard',
  profileName: 'observatory',
  MATOMO_URL: 'https://analytics.openaire.eu/',
  MATOMO_SITE_ID: 508,
  cc_icon_path: '//beta.osobservatory.openaire.eu/assets/img/icons/cc.svg'
};
